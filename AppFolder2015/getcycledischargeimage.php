<?php

header('Access-Control-Allow-Origin: *');

include 'connect.php';
$q = $_GET['q'];
// Protect against form submission variables.
if (get_magic_quotes_gpc())
{
 $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
 while (list($key, $val) = each($process))
 {
 foreach ($val as $k => $v)
 {
 unset($process[$key][$k]);
 if (is_array($v))
 {
 $process[$key][stripslashes($k)] = $v;
 $process[] = &$process[$key][stripslashes($k)];
 }
 else
 {
 $process[$key][stripslashes($k)] = stripslashes($v);
 }
 }
 }
 unset($process);
}
try
{

switch ($q) {
        case '4000':
                $query = "cycle_discharge_4000_image";
                break;
        case '4500':
                $query = "cycle_discharge_4500_image";
                break;
        case '5000':
                $query = "cycle_discharge_5000_image";
                break;
        case 'AGM-S':
                $query = "cycle_discharge_AGM_S_image";
                break;
        case 'AGM-R':
                $query = "cycle_discharge_AGM_R_image";
                break;
}




$sql = "SELECT $query FROM rolls_batteries_mobile_settings WHERE setting_id = 1";


 $result = $pdo->query($sql);
}
catch (PDOException $e)
{
 echo 'Error fetching data: ' . $e->getMessage();
 exit();
} 

$arr = array();

while ($row = $result->fetch())
{
 $arr[] = $row;
}

echo '{"cycle_discharge_image":'.json_encode($arr).'}';
