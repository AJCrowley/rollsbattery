<?php

header('Access-Control-Allow-Origin: *');

include 'connect.php';
$q = $_GET['q'];
$s = $_GET['s'];
$volt = $_GET['v'];

// Protect against form submission variables.
if (get_magic_quotes_gpc())
{
 $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
 while (list($key, $val) = each($process))
 {
 foreach ($val as $k => $v)
 {
 unset($process[$key][$k]);
 if (is_array($v))
 {
 $process[$key][stripslashes($k)] = $v;
 $process[] = &$process[$key][stripslashes($k)];
 }
 else
 {
 $process[$key][stripslashes($k)] = stripslashes($v);
 }
 }
 }
 unset($process);
}
try
{

$series = '';
if ($s != '') {
if ($s != 'all') {
	$series = "series = '" . $s . "' AND "; 
}
}

if ($volt != '') {
	$series = "voltage = '" . $volt . "' AND "; 
}



switch ($q) {
	case 'renewable':
		$query = "WHERE " . $series . "( renewable_energy = 'y' OR renewable_energy = 'Y')";
		break;
	case 'agm':
		$query = "WHERE " . $series . " (agm = 'y' OR agm = 'Y')";
		break;
	case 'marine':
		$query = "WHERE " . $series . " (marine = 'y' OR marine = 'Y')";
		break;
	case 'railroad':
		$query = "WHERE " . $series . " (railroad = 'y' OR railroad = 'Y')";
		break;
	case 'motive_power':
		$query = "WHERE " . $series . " (motive_power = 'y' OR motive_power = 'Y')";
		break;
}


$sql = "SELECT model, voltage, image_filename FROM rolls_batteries $query ORDER BY voltage ASC, model ASC";

//echo $sql;
 $result = $pdo->query($sql);
}
catch (PDOException $e)
{
 echo 'Error fetching data: ' . $e->getMessage();
 exit();
} 

$arr = array();

while ($row = $result->fetch())
{
 $arr[] = $row;
}

echo '{"models":'.json_encode($arr).'}';
