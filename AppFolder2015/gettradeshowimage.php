<?php

header('Access-Control-Allow-Origin: *');

include 'connect.php';
$q = $_GET['q'];
// Protect against form submission variables.
if (get_magic_quotes_gpc())
{
 $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
 while (list($key, $val) = each($process))
 {
 foreach ($val as $k => $v)
 {
 unset($process[$key][$k]);
 if (is_array($v))
 {
 $process[$key][stripslashes($k)] = $v;
 $process[] = &$process[$key][stripslashes($k)];
 }
 else
 {
 $process[$key][stripslashes($k)] = stripslashes($v);
 }
 }
 }
 unset($process);
}
try
{


$sql = "SELECT trade_show_image FROM rolls_batteries_mobile_settings WHERE setting_id = 1";


 $result = $pdo->query($sql);
}
catch (PDOException $e)
{
 echo 'Error fetching data: ' . $e->getMessage();
 exit();
} 

$arr = array();

while ($row = $result->fetch())
{
 $arr[] = $row;
}

echo '{"tradeshowimage":'.json_encode($arr).'}';
