<?php

header('Access-Control-Allow-Origin: *');

include 'connect.php';
$q = $_GET['q'];
// Protect against form submission variables.
if (get_magic_quotes_gpc())
{
 $process = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
 while (list($key, $val) = each($process))
 {
 foreach ($val as $k => $v)
 {
 unset($process[$key][$k]);
 if (is_array($v))
 {
 $process[$key][stripslashes($k)] = $v;
 $process[] = &$process[$key][stripslashes($k)];
 }
 else
 {
 $process[$key][stripslashes($k)] = stripslashes($v);
 }
 }
 }
 unset($process);
}
try
{

switch ($q) {
        case '2volt':
                $query = "image_2_volt_system";
                break;
        case '4volt':
                $query = "image_4_volt_system";
                break;
        case '6volt':
                $query = "image_6_volt_system";
                break;
        case '8volt':
                $query = "image_8_volt_system";
                break;
        case '12volt':
                $query = "image_12_volt_system";
                break;
}




$sql = "SELECT $query FROM rolls_batteries_mobile_settings WHERE setting_id = 1";


 $result = $pdo->query($sql);
}
catch (PDOException $e)
{
 echo 'Error fetching data: ' . $e->getMessage();
 exit();
} 

$arr = array();

while ($row = $result->fetch())
{
 $arr[] = $row;
}

echo '{"volt_system_image":'.json_encode($arr).'}';
