
function dzstaa_mo_ready(){

    jQuery('.saveconfirmer').fadeOut('slow');
    jQuery(document).delegate(".picker-con .the-icon", "click", function(){
        var _t = jQuery(this);
        var _c = _t.parent().children('.picker');
        if(_c.css('display')=='none'){
            _c.fadeIn('fast');
        }else{
            _c.fadeOut('fast');
        };
    });







    function mo_saveall(){
        jQuery('#save-ajax-loading').css('visibility', 'visible');
        var mainarray = jQuery('.mainsettings').serialize();
        var data = {
            action: 'dzstaa_ajax_mo',
            postdata: mainarray
        };
        jQuery('.saveconfirmer').html('Options saved.');
        jQuery('.saveconfirmer').fadeIn('fast').delay(2000).fadeOut('fast');
        jQuery.post(ajaxurl, data, function(response) {
            if(window.console !=undefined ){
                console.log('Got this from the server: ' + response);
            }
            jQuery('#save-ajax-loading').css('visibility', 'hidden');
        });

        return false;
    }




	setTimeout(reskin_select, 10);

    jQuery('#main-ajax-loading').css('visibility', 'hidden');


    jQuery('.dzstaa-save-mainoptions').unbind('click');
    jQuery('.dzstaa-save-mainoptions').bind('click', mo_saveall);


}
jQuery(document).ready(function($){
    reskin_select();


})


function reskin_select(){
    for(i=0;i<jQuery('select').length;i++){
        var _cache = jQuery('select').eq(i);
        //console.log(_cache.parent().attr('class'));
		
        if(_cache.hasClass('styleme')==false || _cache.parent().hasClass('select_wrapper') || _cache.parent().hasClass('select-wrapper')){
            continue;
        }
        var sel = (_cache.find(':selected'));
        _cache.wrap('<div class="select-wrapper"></div>')
        _cache.parent().prepend('<span>' + sel.text() + '</span>')
    }
    jQuery(document).undelegate(".select-wrapper select", "change");
    jQuery(document).delegate(".select-wrapper select", "change",  change_select);
        

    function change_select(){
        var selval = (jQuery(this).find(':selected').text());
        jQuery(this).parent().children('span').text(selval);
    }

}