<?php

class DZSZoomTabsandAccordinos {

    public $main_options = '';
    public $theurl;
    public $slidersindex = 0;
    public $shortcode = 'zoomtabsandaccordions';
    public $pluginmode = "plugin";
    public $dboptionsname = 'dzstaa_options';
    public $alwaysembed = 'on';
    public $mode = 'plugin';
    public $adminpagename = 'dzstaa_menu';
    public $admin_capability = 'manage_options';

    function __construct() {
        if ($this->mode == 'plugin') {
            //echo plugins_url('', __FILE__);
            $this->theurl = plugins_url('', __FILE__) . '/';
        } else {
            $this->theurl = THEME_URL . 'plugins/dzs-zoomtabsandaccordions/';
        }


        //===default options
        $defaultOpts = array(
            'enable_main_script_always' => 'off',
            'extra_css' => '',
        );
        $this->mainoptions = get_option($this->dboptionsname);
        if ($this->mainoptions == '') {
            $this->mainoptions = $defaultOpts;
            update_option($this->dboptionsname, $this->mainoptions);
        };

        $this->mainoptions = array_merge($defaultOpts, $this->mainoptions);


        add_action('init', array($this, 'handle_init'));
        add_action('admin_head', array($this, 'handle_admin_head'));
        add_action('admin_menu', array($this, 'handle_admin_menu'));
        add_action('wp_head', array($this, 'handle_wp_head'));


        add_action('wp_ajax_dzstaa_ajax_mo', array($this, 'post_save_mo'));

        
//          add_action('wp_print_styles', array($this, 'fe_embedscripts'));
          add_shortcode($this->shortcode, array($this, 'show_shortcode'));
          add_shortcode('dzs_' . $this->shortcode, array($this, 'show_shortcode'));
         
        
        add_shortcode('dzs_pricingtable', array($this, 'sc_pricingtable'));
        add_shortcode('dzstaa_tab', array($this, 'sc_tab'));
        add_shortcode('dzstaa_tab_title', array($this, 'sc_tab_title'));
        add_shortcode('dzstaa_tab_content', array($this, 'sc_tab_content'));
        
        
        add_filter( "the_content", array($this, 'parse_content_for_nested_shortcodes') ); 
        
    }

    function handle_init() {
        wp_enqueue_script('jquery');
        if (is_admin()) {
            if (current_user_can('edit_posts') || current_user_can('edit_pages')) {
                // echo 'ceva';
                wp_enqueue_script('dzstaa.admin', $this->theurl . 'admin/admin.js');
                wp_enqueue_style('dzstaa.admin', $this->theurl . 'admin/admin.css');
                wp_enqueue_script('thickbox');
                wp_enqueue_style('thickbox');
                wp_enqueue_script('jquery-ui-core');
                wp_enqueue_script('jquery-ui-sortable');
                wp_enqueue_script('jquery-ui-droppable');
                wp_enqueue_style('dzs.zoombox', $this->theurl . 'zoombox/zoombox.css');
                wp_enqueue_script('dzs.zoombox', $this->theurl . 'zoombox/zoombox.js');
                wp_enqueue_script('dzstaa_htmleditor_plugin', $this->theurl . 'tinymce/plugin-htmleditor.js');
                //add_filter("mce_external_plugins" , array($this, 'tinymce_plugin'));
                //add_filter("mce_buttons" , array($this, 'tinymce_register_button'));
            }
        }else{
            if($this->mainoptions['enable_main_script_always']=='on'){
                $this->fe_embedscripts();
                
            }
        }
    }

    function post_save_mo() {
        $auxarray = array();
        //parsing post data
        parse_str($_POST['postdata'], $auxarray);

        if (isset($auxarray['use_external_uploaddir']) && $auxarray['use_external_uploaddir'] == 'on') {

            $path_uploadfile = dirname(dirname(dirname(__FILE__))) . '/upload.php';
            if (file_exists($path_uploadfile) === false) {
                copy(dirname(__FILE__) . '/admin/upload.php', $path_uploadfile);
            }
            $path_uploaddir = dirname(dirname(dirname(__FILE__))) . '/upload';
            if (is_dir($path_uploaddir) === false) {
                mkdir($path_uploaddir, 0777);
            }
        }

        update_option($this->dboptionsname, $auxarray);
        die();
    }

    function handle_footer() {
    }

    function handle_add_meta_boxes() {
        
    }
    
    
    function handle_wp_head() {

        if ($this->mainoptions['extra_css']) {
            echo '<style>';
            echo $this->mainoptions['extra_css'];
            echo '</style>';
        }
    }

    function ajax_preparePreview() {
        echo stripslashes(do_shortcode($_POST['postdata']));
        die();
    }
    function sc_tab($atts=array(), $content = ''){
        //[dzs_pricingtable][/dzs_pricingtable]
        
        $fout = '';
        $margs = array(
        );
        
        if(!$atts){
            $atts = array();
        }
        $margs = array_merge($margs, $atts);
        
        
        $fout.='<div class="dzs-tab-tobe';
        $fout.='">';
        $fout.=stripslashes(do_shortcode($content));
        $fout.='</div>';
        
        
        
        
        return $fout;
        
    }
    function sc_tab_title($atts=array(), $content = ''){
        //[dzs_pricingtable][/dzs_pricingtable]
        
        $fout = '';
        $margs = array(
        );
        
        if(!$atts){
            $atts = array();
        }
        $margs = array_merge($margs, $atts);
        
        
        $fout.='<div class="tab-menu';
        $fout.='">';
        $fout.=stripslashes(do_shortcode($content));
        $fout.='</div>';
        
        
        
        
        return $fout;
        
    }
    function sc_tab_content($atts=array(), $content = ''){
        //[dzs_pricingtable][/dzs_pricingtable]
        
        $fout = '';
        $margs = array(
        );
        
        if(!$atts){
            $atts = array();
        }
        $margs = array_merge($margs, $atts);
        
        
        $fout.='<div class="tab-content';
        $fout.='">';
        $fout.=stripslashes(do_shortcode($content));
        $fout.='</div>';
        
        
        
        
        return $fout;
        
    }
    function handle_admin_menu() {

        if ($this->pluginmode == 'theme') {
            
        } else {
            $dzstaa_page = add_menu_page(__('ZoomTabs', 'dzstaa'), __('ZoomTabs', 'dzstaa'), $this->admin_capability, $this->adminpagename, array($this, 'admin_page_mainoptions'), 'div');
        }
    }

    function admin_page_mainoptions() {



        if (isset($_POST['dzstaa_delete_alloptions']) && $_POST['dzstaa_delete_alloptions'] == 'on') {
            //delete_option('dzstaa_items');
            delete_option($this->dboptionsname);
            //delete_option('dzstaa_biggalleries');
        }
        ?>

        <div class="wrap">
            <h2><?php echo __('DZS ZoomTabs and Accordions Main Settings', 'dzstaa'); ?></h2>
            <br/>
            <form class="mainsettings dzstaa-mainoptions">

                <h3><?php echo __('Admin Options', 'dzstaa'); ?></h3>
                <div class="setting">
                    <div class="label"><?php echo __('Enable Always Load Main Script', 'dzstaa'); ?></div>
                    <?php echo DZSHelpers::generate_select('enable_main_script_always', array('options' => array('on', 'off'), 'class' => 'styleme', 'seekval' => $this->mainoptions['enable_main_script_always'])); ?>
                    <div class="sidenote"><?php echo __('if disabled, the script will load only where the shortcode is applied', 'dzstaa'); ?></div>
                </div>
                <div class="setting">
                    <div class="label"><?php echo __('Extra CSS', 'dzsp'); ?></div>
                <?php echo DZSHelpers::generate_input_textarea('extra_css', array( 'seekval' => $this->mainoptions['extra_css']));   ?>
                    <div class="sidenote"><?php echo __('', 'dzsp'); ?></div>
                </div>
                <br/>
                <a href='#' class="button-primary save-btn dzstaa-save-mainoptions"><?php echo __('Save Options', 'dzstaa'); ?></a>
            </form>
            <br/><br/>
            <form class="mainsettings" method="POST">
                <button name="dzstaa_delete_alloptions" value="on" class="button-secondary"><?php echo __('Delete All DZS ZoomTabs and Accordions Plugin Data', 'dzstaa'); ?></button>
            </form>
            <div class="saveconfirmer" style=""><img alt="" style="" id="save-ajax-loading2" src="<?php echo site_url(); ?>/wp-admin/images/wpspin_light.gif"/></div>
            <script>
                jQuery(document).ready(function($){
                    dzstaa_mo_ready();
                })
            </script>
        </div>
        <div class="clear"></div><br/>
        <?php
    }

    function handle_admin_head() {
        if (is_admin()) {
            echo '<script>
        var dzstaa_settings = {
        thepath : "' . $this->theurl . '"
        ,wpurl : "' . site_url() . '"
        };  </script>';
            wp_enqueue_script('dzstaa_configreceiver', $this->theurl . 'tinymce/receiver.js');
        }
    }

    function fe_embedscripts() {
        
          wp_enqueue_script('dzs.tabsandaccordions', $this->theurl . 'dzstabsandaccordions/dzstabsandaccordions.js');
          wp_enqueue_style('dzs.tabsandaccordions', $this->theurl . 'dzstabsandaccordions/dzstabsandaccordions.css');
          wp_enqueue_style('fontawesome', $this->theurl . 'fontawesome/font-awesome.min.css');
    }
    
    
    function parse_content_for_nested_shortcodes($content){
        
//        echo $content;
        $matches=array();
          $subject = $content;
$pattern = '/\[zoomtabsandaccordions.*?(\[zoomtabsandaccordions.*?\].*?\[\/zoomtabsandaccordions\])/';
preg_match_all($pattern, $subject, $matches);
if(count($matches)>0 && isset($matches[1])){
    foreach($matches[1] as $regmatch){
        $content = str_replace($regmatch, stripslashes(do_shortcode($regmatch)), $content);
    }
}


        return $content;
    }

    function show_shortcode($atts, $content = null) {
        //[zoomtabsandaccordions]test teste test[/zoomtabsandaccordions]
       
          $fout='';
          
          
          $margs = array(
                'settings_starttab' => '',
                'skin' => 'skin-default',
                'design_tabsposition' => 'top',
                'toggle_breakpoint' => '',
                'settings_appendwholecontent' => 'off',
                'design_highlightcolor' => '',
              'settings_enable_linking' => 'off',
              'settings_is_always_accordion' => 'off',
              'toggle_type' => 'accordion',
              'settings_scroll_to_start' => 'off',
              'design_tabswidth' => '',
              'design_transition' => 'default',
              'refresh_tab_height' => '0',
              
          );
          if(is_array($atts)==false){
              $atts = array();
          }
          $margs = array_merge($margs, $atts);
          
          if($margs['settings_is_always_accordion']=='on'){
              $margs['toggle_breakpoint'] = 40000;
          }
          
//          print_r($margs);
          
          $fout.='<style>';
          if($margs['design_highlightcolor']!=''){
              $fout.='body #dzs-tabs'.$this->slidersindex.'.skin-blue .tabs-menu .tab-menu-con.active .tab-menu{ background-color: '.$margs['design_highlightcolor'].'; }';
              $fout.='body #dzs-tabs'.$this->slidersindex.'.skin-melbourne .tabs-menu .tab-menu-con.active{ border-color: '.$margs['design_highlightcolor'].'; color: '.$margs['design_highlightcolor'].'; }';
              $fout.='body #dzs-tabs'.$this->slidersindex.'.skin-default .tabs-menu .tab-menu-con.active .tab-menu{ border-color: '.$margs['design_highlightcolor'].'; }';
              
              
          }
          $fout.='</style>';
          
          $fout.='<div id="dzs-tabs'.$this->slidersindex.'" class="dzs-tabs dzs-tabs'.$this->slidersindex.' '.$margs['skin'].'">';
          
          $fout.=stripslashes(do_shortcode($content));
          
          
          $fout.='</div>';
          $fout.='<script>';
          $fout.='jQuery(document).ready(function($){ dzstaa_init(".dzs-tabs'.$this->slidersindex.'", {'
                  . 'init_each: true';
          
            $fout.=',toggle_type : "'.$margs['toggle_type'].'"';
            $fout.=',settings_enable_linking : "'.$margs['settings_enable_linking'].'"';
            $fout.=',design_transition : "'.$margs['design_transition'].'"';
              
              
          if($margs['design_tabsposition']!=''){
              $fout.=', design_tabsposition : "'.$margs['design_tabsposition'].'"';
          }
          if($margs['design_tabswidth']!=''){
              $fout.=', design_tabswidth : "'.$margs['design_tabswidth'].'"';
          }
          if($margs['settings_starttab']!=''){
              $fout.=', settings_startTab : "'.$margs['settings_starttab'].'"';
          }
          if($margs['toggle_breakpoint']!=''){
              $fout.=', toggle_breakpoint : "'.$margs['toggle_breakpoint'].'"';
          }
          if($margs['refresh_tab_height']!='' && $margs['refresh_tab_height']!='0'){
              $fout.=', refresh_tab_height : "'.$margs['refresh_tab_height'].'"';
          }


          if($margs['settings_appendwholecontent']=='on'){
              $fout.=', settings_appendWholeContent : true';
          }
          if($margs['settings_scroll_to_start']=='on'){
              $fout.=', settings_scroll_to_start : '.$margs['settings_scroll_to_start'].'"';
          }
          
          
          
          $fout.='});';
          $fout.='});';
          $fout.='</script>';
          
          
          $this->fe_embedscripts();
          
          $this->slidersindex++;
          
          return $fout;
          
    }
    
    function show_generator(){
        
        
// some total cache vars that needs to be like this
define('DONOTCACHEPAGE', true);
define('DONOTMINIFY', true);
        

$url_admin = get_admin_url();
$structure_test = '<div class="item-con">
    <div class="item-header">
        <h4>Tab</h4>
        <a class="button-header button-delete">
        <span class="icon delete"></span>
        </a>
        <a class="button-header button-edit active">
        <span class="icon pencil"></span>
        </a>
        <div class="clear"></div>
    </div>
    <div class="item-settings">
        <div class="setting">
            <div class="setting-label">Title</div>
            <div class="setting-input type-textarea">

            <textarea class="testtitle tinymce_enabled" name="testauthor[]" rows="3" >title
            </textarea>
        </div>
        </div>
        <div class="setting">
            <div class="setting-label">Tooltip</div>
            <div class="setting-input type-textarea">

            <textarea class="testtooltip tinymce_enabled" name="testauthor[]" rows="3" >tooltip
            </textarea>
        </div>
        </div>
        <div class="setting">
            <div class="setting-label">Content</div>
            <div class="setting-input type-textarea">

            <textarea class="testcont tinymce_enabled" name="testauthor[]" rows="3" >This is some example text that you can edit inside the &lt;strong&gt;TinyMCE editor&lt;/strong&gt;.
            </textarea>
        </div>
        </div>
    </div>
    </div>';
        ?>
<!doctype html>
<html lang="en" style="">
    <head>
        <meta charset="utf-8" />
        <title>DZS ZoomTabs and Accordions Generator</title>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" type="text/javascript"></script>
        <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<![endif]-->
        
        <script>
            /*
             * 
             */
            //console.log(window.tinyMCE)
            var dzstaa_settings = {
                thepath : "<?php echo $this->theurl; ?>"
                ,startSetup : '<?php if(isset($_GET['sel'])){
                    $aux = $_GET['sel'];
                    
                    //==sanitizing...
                    $aux = stripslashes($aux);
                    $aux = str_replace("'", '"', $aux);
                    $lb = array("\r\n", "\n", "\r");
                    $aux = str_replace($lb, ' ', $aux);
                    echo $aux;
                    } ?>'
            }
	if(window.tinyMCE && window.wptinyMCE==undefined){
            window.wptinyMCE = window.tinyMCE;
        }
        var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
            window.theme_url = "<?php echo $this->theurl; ?>";
            
            var pagenow = 'dzs_portfolio',
	typenow = 'dzs_portfolio',
	adminpage = 'post-php',
	thousandsSeparator = ',',
	decimalPoint = '.',
	isRtl = 0;
var wordCountL10n = {"type":"w"};
var thickboxL10n = {"next":"Next >","prev":"< Prev","image":"Image","of":"of","close":"Close","noiframes":"This feature requires inline frames. You have iframes disabled or your browser does not support them.","loadingAnimation":"<?php echo site_url(); ?>\/wp-includes\/js\/thickbox\/loadingAnimation.gif","closeImage":"<?php echo site_url(); ?>\/wp-includes\/js\/thickbox\/tb-close.png"};
var commonL10n = {"warnDelete":"You are about to permanently delete the selected items.\n  'Cancel' to stop, 'OK' to delete."};
var wpAjax = {"noPerm":"You do not have permission to do that.","broken":"An unidentified error has occurred."};
var autosaveL10n = {"autosaveInterval":"60","savingText":"Saving Draft\u2026","saveAlert":"The changes you made will be lost if you navigate away from this page.","blog_id":"1"};
var quicktagsL10n = {"closeAllOpenTags":"Close all open tags","closeTags":"close tags","enterURL":"Enter the URL","enterImageURL":"Enter the URL of the image","enterImageDescription":"Enter a description of the image","fullscreen":"fullscreen","toggleFullscreen":"Toggle fullscreen mode","textdirection":"text direction","toggleTextdirection":"Toggle Editor Text Direction"};var adminCommentsL10n = {"hotkeys_highlight_first":"","hotkeys_highlight_last":"","replyApprove":"Approve and Reply","reply":"Reply"};var heartbeatSettings = {"nonce":"ecc15b5e95"};var postL10n = {"ok":"OK","cancel":"Cancel","publishOn":"Publish on:","publishOnFuture":"Schedule for:","publishOnPast":"Published on:","dateFormat":"%1$s %2$s, %3$s @ %4$s : %5$s","showcomm":"Show more comments","endcomm":"No more comments found.","publish":"Publish","schedule":"Schedule","update":"Update","savePending":"Save as Pending","saveDraft":"Save Draft","private":"Private","public":"Public","publicSticky":"Public, Sticky","password":"Password Protected","privatelyPublished":"Privately Published","published":"Published","comma":","};var _wpUtilSettings = {"ajax":{"url":"<?php echo $url_admin; ?>admin-ajax.php"}};var _wpMediaModelsL10n = {"settings":{"ajaxurl":"<?php echo $url_admin; ?>admin-ajax.php","post":{"id":0}}};var pluploadL10n = {"queue_limit_exceeded":"You have attempted to queue too many files.","file_exceeds_size_limit":"%s exceeds the maximum upload size for this site.","zero_byte_file":"This file is empty. Please try another.","invalid_filetype":"This file type is not allowed. Please try another.","not_an_image":"This file is not an image. Please try another.","image_memory_exceeded":"Memory exceeded. Please try another smaller file.","image_dimensions_exceeded":"This is larger than the maximum size. Please try another.","default_error":"An error occurred in the upload. Please try again later.","missing_upload_url":"There was a configuration error. Please contact the server administrator.","upload_limit_exceeded":"You may only upload 1 file.","http_error":"HTTP error.","upload_failed":"Upload failed.","big_upload_failed":"Please try uploading this file with the %1$sbrowser uploader%2$s.","big_upload_queued":"%s exceeds the maximum upload size for the multi-file uploader when used in your browser.","io_error":"IO error.","security_error":"Security error.","file_cancelled":"File canceled.","upload_stopped":"Upload stopped.","dismiss":"Dismiss","crunching":"Crunching\u2026","deleted":"moved to the trash.","error_uploading":"\u201c%s\u201d has failed to upload."};
var _wpPluploadSettings = {"defaults":{"runtimes":"html5,silverlight,flash,html4","file_data_name":"async-upload","multiple_queues":true,"max_file_size":"33554432b","url":"<?php echo $url_admin; ?>async-upload.php","flash_swf_url":"<?php echo site_url(); ?>\/wp-includes\/js\/plupload\/plupload.flash.swf","silverlight_xap_url":"<?php echo site_url(); ?>\/wp-includes\/js\/plupload\/plupload.silverlight.xap","filters":[{"title":"Allowed Files","extensions":"*"}],"multipart":true,"urlstream_upload":true,"multipart_params":{"action":"upload-attachment","_wpnonce":"773ef53e9b"}},"browser":{"mobile":false,"supported":true},"limitExceeded":false};
var _wpMediaViewsL10n = {"url":"URL","addMedia":"Add Media","search":"Search","select":"Select","cancel":"Cancel","selected":"%d selected","dragInfo":"Drag and drop to reorder images.","uploadFilesTitle":"Upload Files","uploadImagesTitle":"Upload Images","mediaLibraryTitle":"Media Library","insertMediaTitle":"Insert Media","createNewGallery":"Create a new gallery","returnToLibrary":"\u2190 Return to library","allMediaItems":"All media items","noItemsFound":"No items found.","insertIntoPost":"Insert into post","uploadedToThisPost":"Uploaded to this post","warnDelete":"You are about to permanently delete this item.\n  'Cancel' to stop, 'OK' to delete.","insertFromUrlTitle":"Insert from URL","setFeaturedImageTitle":"Set Featured Image","setFeaturedImage":"Set featured image","createGalleryTitle":"Create Gallery","editGalleryTitle":"Edit Gallery","cancelGalleryTitle":"\u2190 Cancel Gallery","insertGallery":"Insert gallery","updateGallery":"Update gallery","addToGallery":"Add to gallery","addToGalleryTitle":"Add to Gallery","reverseOrder":"Reverse order","settings":{"tabs":[],"tabUrl":"<?php echo $url_admin; ?>media-upload.php?chromeless=1","mimeTypes":{"image":"Images","audio":"Audio","video":"Video"},"captions":true,"nonce":{"sendToEditor":"0aef7a9d93"},"post":{"id":3905,"nonce":"0ba07d0c8c","featuredImageId":"3577"},"defaultProps":{"link":"","align":"","size":""},"embedExts":["mp3","ogg","wma","m4a","wav","mp4","m4v","webm","ogv","wmv","flv"]}};var authcheckL10n = {"beforeunload":"Your session has expired. You can log in again from this page or go to the login page.","interval":"180"};var wordCountL10n = {"type":"w"};var wpLinkL10n = {"title":"Insert\/edit link","update":"Update","save":"Add Link","noTitle":"(no title)","noMatchesFound":"No matches found."};/* ]]> */
</script>
        <link rel="stylesheet" type="text/css" href="<?php echo $this->theurl; ?>tinymce/popup.css"/>
        <script src="<?php echo $this->theurl; ?>tinymce/popup.js"></script>
        <script type='text/javascript' src='<?php echo $url_admin; ?>load-scripts.php?c=1&amp;load%5B%5D=utils,plupload,plupload-html5,plupload-flash,plupload-silverlight,plupload-html4,json2&amp;ver=3.6.1'></script>
        <script type='text/javascript' src='<?php echo $url_admin; ?>load-scripts.php?c=1&amp;load%5B%5D=thickbox,jquery-ui-core,jquery-ui-widget,jquery-ui-mouse,jquery-ui-sortable,jquery-ui-draggable,jquery-ui-droppable,&amp;load%5B%5D=common,admin-bar,schedule,wp-ajax-response,autosave,wp-lists,admin-comments,suggest,postbox,&amp;load%5B%5D=heartbeat,underscore,shortcode,backbone,wp-util,wp-backbone,media-models,wp-plupload,media-views,media-editor,wp-auth-check&amp;load%5B%5D=,word-count,editor,jquery-ui-resizable,jquery-ui-button,jquery-ui-position,jquery-ui-dialog,wpdialogs,wplink,wpdialogs-popup,wp-&amp;load%5B%5D=fullscreen,media-upload&amp;ver=3.6.1'></script>
        
        <script src="<?php echo $this->theurl; ?>tinymce/tinymce/tinymce.min.js"></script>
        <script src="<?php echo $this->theurl; ?>tinymce/tinymce/jquery.tinymce.min.js"></script>
        <!--
        <script type="text/javascript" src="http://localhost/tinymce_jquery/jscripts/tiny_mce/jquery.tinymce.js"></script>
        <script src="<?php echo $this->theurl; ?>tinymce/js/tiny_mce.js"></script>
        -->
        <?php //wp_head(); ?>
    
    </head>
    <body class="popup-admin-wrapper">
        <div class="maincon">
            
            
            <div class="main-generator-con">
                <div class="generator-controls" style="text-align: right;"><div style="" class="btn-add-tab"><i class="fa fa-plus white-circle-bg">&nbsp;</i>Add Tab</div> <div style="" class="btn-delete-tab delete-trigger"><i class="fa fa-plus white-circle-bg">&nbsp;</i>Remove Tab</div></div>
                <br/>
                <div class="main-generator">
                    
                    <style id="generator-dzstaa-style"></style>
                    <div id="generator-dzstaa" class="">
                        
                    </div>
                </div>
                <div class="item-editor"></div>
            </div>
            <hr>
            
            <div class="setting type_any">
                <div class="setting-label">Skin</div>
                <select class="textinput styleme changer-skin" name="skin">
                    <option>skin-default</option>
                    <option>skin-blue</option>
                    <option>skin-melbourne</option>
                    <option>skin-box</option>
                </select>
            </div>
            
            
            <div class="setting type_any">
                <div class="setting-label">Toggle Breakpoint</div>
                <input class="textinput " name="toggle_breakpoint" value=""/>
                <div class="sidenote">The number of pixels at which tabs minimize into toggles.</div>
            </div>
            
            
            <div class="setting type_any">
                <div class="setting-label">Highlight Color</div>
                <input class="textinput with-colorpicker" name="design_highlightcolor" value=""/><span class="picker-con"><span class="the-icon"></span><span class="picker"></span></span>
                <div class="sidenote">Set a highlight color - for skin-melbourne and for skin-blue and for skin-default.</div>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Tabs Position</div>
                <select class="textinput styleme" name="design_tabsposition">
                    <option>top</option>
                    <option>bottom</option>
                    <option>left</option>
                    <option>right</option>
                </select>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Start Tab</div>
                <input class="textinput " name="settings_starttab" value=""/>
                <div class="sidenote">Input here a start tab where to start the tabs or leave blank for default.</div>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Tabs Width</div>
                <input class="textinput " name="design_tabswidth" value="default"/>
                <div class="sidenote">Possible values - <strong>default</strong> - <strong>auto</strong> - <strong>fullwidth</strong>, will  - a numberic value in px.</div>
            </div>
            <div class="setting type_any">
                <div class="setting-label">Refresh Tab Height</div>
                <input class="textinput " name="refresh_tab_height" value="0"/>
                <div class="sidenote"><?php echo __('Refresh tab height every <strong>x</strong> ms. This is best left to 0 in most cases when the tab content does not change height')?>.</div>
            </div>
            
            
            
            <div class="setting type_any">
                <div class="setting-label">Append Whole Content</div>
                <select class="textinput styleme" name="settings_appendwholecontent">
                    <option>off</option>
                    <option>on</option>
                </select>
                <div class="sidenote">In case you want to load forms / scripts inside DZS tabs, you need to turn this to <strong>on</strong> so that the content is moved in the tabs not cloned..</div>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Enable Linking</div>
                <select class="textinput styleme" name="settings_enable_linking">
                    <option>off</option>
                    <option>on</option>
                </select>
                <div class="sidenote">Enable HTML5 History API on tabs so you can link to them.</div>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Is Always Accordion ?</div>
                <select class="textinput styleme" name="settings_is_always_accordion">
                    <option>off</option>
                    <option>on</option>
                </select>
                <div class="sidenote">Make this tabs always be accordions.</div>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Accordion Type?</div>
                <select class="textinput styleme" name="toggle_type">
                    <option>accordion</option>
                    <option>toggle</option>
                </select>
            </div>
            
            <div class="setting type_any">
                <div class="setting-label">Transition Type?</div>
                <select class="textinput styleme" name="design_transition">
                    <option>default</option>
                    <option>fade</option>
                    <option>slide</option>
                </select>
            </div>
            
            
            
            
            <p><span style="display:inline-block; text-align: center;"class="button-style-noir btn-primary btn-master-generate">Generate</span></p>
            <div class="clear"></div>
        </div>
        
            <div class="output-div"></div>
<script type="text/javascript">if(typeof wpOnload=='function'){
    wpOnload();;
}</script>


<script type="text/html" id="tmpl-media-frame">
		<div class="media-frame-menu"></div>
		<div class="media-frame-title"></div>
		<div class="media-frame-router"></div>
		<div class="media-frame-content"></div>
		<div class="media-frame-toolbar"></div>
		<div class="media-frame-uploader"></div>
	</script>

	<script type="text/html" id="tmpl-media-modal">
		<div class="media-modal wp-core-ui">
			<a class="media-modal-close" href="#" title="Close"><span class="media-modal-icon"></span></a>
			<div class="media-modal-content"></div>
		</div>
		<div class="media-modal-backdrop"></div>
	</script>

	<script type="text/html" id="tmpl-uploader-window">
		<div class="uploader-window-content">
			<h3>Drop files to upload</h3>
		</div>
	</script>

	<script type="text/html" id="tmpl-uploader-inline">
		<# var messageClass = data.message ? 'has-upload-message' : 'no-upload-message'; #>
		<div class="uploader-inline-content {{ messageClass }}">
		<# if ( data.message ) { #>
			<h3 class="upload-message">{{ data.message }}</h3>
		<# } #>
					<div class="upload-ui">
				<h3 class="upload-instructions drop-instructions">Drop files anywhere to upload</h3>
				<a href="#" class="browser button button-hero">Select Files</a>
			</div>

			<div class="upload-inline-status"></div>

			<div class="post-upload-ui">
				
				<p class="max-upload-size">Maximum upload file size: 32MB.</p>

				
							</div>
				</div>
	</script>

	<script type="text/html" id="tmpl-uploader-status">
		<h3>Uploading</h3>
		<a class="upload-dismiss-errors" href="#">Dismiss Errors</a>

		<div class="media-progress-bar"><div></div></div>
		<div class="upload-details">
			<span class="upload-count">
				<span class="upload-index"></span> / <span class="upload-total"></span>
			</span>
			<span class="upload-detail-separator">&ndash;</span>
			<span class="upload-filename"></span>
		</div>
		<div class="upload-errors"></div>
	</script>

	<script type="text/html" id="tmpl-uploader-status-error">
		<span class="upload-error-label">Error</span>
		<span class="upload-error-filename">{{{ data.filename }}}</span>
		<span class="upload-error-message">{{ data.message }}</span>
	</script>

	<script type="text/html" id="tmpl-attachment">
		<div class="attachment-preview type-{{ data.type }} subtype-{{ data.subtype }} {{ data.orientation }}">
			<# if ( data.uploading ) { #>
				<div class="media-progress-bar"><div></div></div>
			<# } else if ( 'image' === data.type ) { #>
				<div class="thumbnail">
					<div class="centered">
						<img src="{{ data.size.url }}" draggable="false" />
					</div>
				</div>
			<# } else { #>
				<img src="{{ data.icon }}" class="icon" draggable="false" />
				<div class="filename">
					<div>{{ data.filename }}</div>
				</div>
			<# } #>

			<# if ( data.buttons.close ) { #>
				<a class="close media-modal-icon" href="#" title="Remove"></a>
			<# } #>

			<# if ( data.buttons.check ) { #>
				<a class="check" href="#" title="Deselect"><div class="media-modal-icon"></div></a>
			<# } #>
		</div>
		<#
		var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly';
		if ( data.describe ) { #>
			<# if ( 'image' === data.type ) { #>
				<input type="text" value="{{ data.caption }}" class="describe" data-setting="caption"
					placeholder="Caption this image&hellip;" {{ maybeReadOnly }} />
			<# } else { #>
				<input type="text" value="{{ data.title }}" class="describe" data-setting="title"
					<# if ( 'video' === data.type ) { #>
						placeholder="Describe this video&hellip;"
					<# } else if ( 'audio' === data.type ) { #>
						placeholder="Describe this audio file&hellip;"
					<# } else { #>
						placeholder="Describe this media file&hellip;"
					<# } #> {{ maybeReadOnly }} />
			<# } #>
		<# } #>
	</script>

	<script type="text/html" id="tmpl-attachment-details">
		<h3>
			Attachment Details
			<span class="settings-save-status">
				<span class="spinner"></span>
				<span class="saved">Saved.</span>
			</span>
		</h3>
		<div class="attachment-info">
			<div class="thumbnail">
				<# if ( data.uploading ) { #>
					<div class="media-progress-bar"><div></div></div>
				<# } else if ( 'image' === data.type ) { #>
					<img src="{{ data.size.url }}" draggable="false" />
				<# } else { #>
					<img src="{{ data.icon }}" class="icon" draggable="false" />
				<# } #>
			</div>
			<div class="details">
				<div class="filename">{{ data.filename }}</div>
				<div class="uploaded">{{ data.dateFormatted }}</div>

				<# if ( 'image' === data.type && ! data.uploading ) { #>
					<# if ( data.width && data.height ) { #>
						<div class="dimensions">{{ data.width }} &times; {{ data.height }}</div>
					<# } #>

					<# if ( data.can.save ) { #>
						<a class="edit-attachment" href="{{ data.editLink }}&amp;image-editor" target="_blank">Edit Image</a>
						<a class="refresh-attachment" href="#">Refresh</a>
					<# } #>
				<# } #>

				<# if ( data.fileLength ) { #>
					<div class="file-length">Length: {{ data.fileLength }}</div>
				<# } #>

				<# if ( ! data.uploading && data.can.remove ) { #>
					<a class="delete-attachment" href="#">Delete Permanently</a>
				<# } #>

				<div class="compat-meta">
					<# if ( data.compat && data.compat.meta ) { #>
						{{{ data.compat.meta }}}
					<# } #>
				</div>
			</div>
		</div>

		<# var maybeReadOnly = data.can.save || data.allowLocalEdits ? '' : 'readonly'; #>
			<label class="setting" data-setting="title">
				<span>Title</span>
				<input type="text" value="{{ data.title }}" {{ maybeReadOnly }} />
			</label>
			<label class="setting" data-setting="caption">
				<span>Caption</span>
				<textarea {{ maybeReadOnly }}>{{ data.caption }}</textarea>
			</label>
		<# if ( 'image' === data.type ) { #>
			<label class="setting" data-setting="alt">
				<span>Alt Text</span>
				<input type="text" value="{{ data.alt }}" {{ maybeReadOnly }} />
			</label>
		<# } #>
			<label class="setting" data-setting="description">
				<span>Description</span>
				<textarea {{ maybeReadOnly }}>{{ data.description }}</textarea>
			</label>
	</script>

	<script type="text/html" id="tmpl-media-selection">
		<div class="selection-info">
			<span class="count"></span>
			<# if ( data.editable ) { #>
				<a class="edit-selection" href="#">Edit</a>
			<# } #>
			<# if ( data.clearable ) { #>
				<a class="clear-selection" href="#">Clear</a>
			<# } #>
		</div>
		<div class="selection-view"></div>
	</script>

	<script type="text/html" id="tmpl-attachment-display-settings">
		<h3>Attachment Display Settings</h3>

		<# if ( 'image' === data.type ) { #>
			<label class="setting">
				<span>Alignment</span>
				<select class="alignment"
					data-setting="align"
					<# if ( data.userSettings ) { #>
						data-user-setting="align"
					<# } #>>

					<option value="left">
						Left					</option>
					<option value="center">
						Center					</option>
					<option value="right">
						Right					</option>
					<option value="none" selected>
						None					</option>
				</select>
			</label>
		<# } #>

		<div class="setting">
			<label>
				<# if ( data.model.canEmbed ) { #>
					<span>Embed or Link</span>
				<# } else { #>
					<span>Link To</span>
				<# } #>

				<select class="link-to"
					data-setting="link"
					<# if ( data.userSettings && ! data.model.canEmbed ) { #>
						data-user-setting="urlbutton"
					<# } #>>

				<# if ( data.model.canEmbed ) { #>
					<option value="embed" selected>
						Embed Media Player					</option>
					<option value="file">
				<# } else { #>
					<option value="file" selected>
				<# } #>
					<# if ( data.model.canEmbed ) { #>
						Link to Media File					<# } else { #>
						Media File					<# } #>
					</option>
					<option value="post">
					<# if ( data.model.canEmbed ) { #>
						Link to Attachment Page					<# } else { #>
						Attachment Page					<# } #>
					</option>
				<# if ( 'image' === data.type ) { #>
					<option value="custom">
						Custom URL					</option>
					<option value="none">
						None					</option>
				<# } #>
				</select>
			</label>
			<input type="text" class="link-to-custom" data-setting="linkUrl" />
		</div>

		<# if ( 'undefined' !== typeof data.sizes ) { #>
			<label class="setting">
				<span>Size</span>
				<select class="size" name="size"
					data-setting="size"
					<# if ( data.userSettings ) { #>
						data-user-setting="imgsize"
					<# } #>>
											<#
						var size = data.sizes['thumbnail'];
						if ( size ) { #>
							<option value="thumbnail" >
								Thumbnail &ndash; {{ size.width }} &times; {{ size.height }}
							</option>
						<# } #>
											<#
						var size = data.sizes['medium'];
						if ( size ) { #>
							<option value="medium" >
								Medium &ndash; {{ size.width }} &times; {{ size.height }}
							</option>
						<# } #>
											<#
						var size = data.sizes['large'];
						if ( size ) { #>
							<option value="large" >
								Large &ndash; {{ size.width }} &times; {{ size.height }}
							</option>
						<# } #>
											<#
						var size = data.sizes['full'];
						if ( size ) { #>
							<option value="full"  selected='selected'>
								Full Size &ndash; {{ size.width }} &times; {{ size.height }}
							</option>
						<# } #>
									</select>
			</label>
		<# } #>
	</script>

	<script type="text/html" id="tmpl-gallery-settings">
		<h3>Gallery Settings</h3>

		<label class="setting">
			<span>Link To</span>
			<select class="link-to"
				data-setting="link"
				<# if ( data.userSettings ) { #>
					data-user-setting="urlbutton"
				<# } #>>

				<option value="post" selected>
					Attachment Page				</option>
				<option value="file">
					Media File				</option>
				<option value="none">
					None				</option>
			</select>
		</label>

		<label class="setting">
			<span>Columns</span>
			<select class="columns" name="columns"
				data-setting="columns">
									<option value="1" >
						1					</option>
									<option value="2" >
						2					</option>
									<option value="3"  selected='selected'>
						3					</option>
									<option value="4" >
						4					</option>
									<option value="5" >
						5					</option>
									<option value="6" >
						6					</option>
									<option value="7" >
						7					</option>
									<option value="8" >
						8					</option>
									<option value="9" >
						9					</option>
							</select>
		</label>

		<label class="setting">
			<span>Random Order</span>
			<input type="checkbox" data-setting="_orderbyRandom" />
		</label>
	</script>

	<script type="text/html" id="tmpl-embed-link-settings">
		<label class="setting">
			<span>Title</span>
			<input type="text" class="alignment" data-setting="title" />
		</label>
	</script>

	<script type="text/html" id="tmpl-embed-image-settings">
		<div class="thumbnail">
			<img src="{{ data.model.url }}" draggable="false" />
		</div>

					<label class="setting caption">
				<span>Caption</span>
				<textarea data-setting="caption" />
			</label>
		
		<label class="setting alt-text">
			<span>Alt Text</span>
			<input type="text" data-setting="alt" />
		</label>

		<div class="setting align">
			<span>Align</span>
			<div class="button-group button-large" data-setting="align">
				<button class="button" value="left">
					Left				</button>
				<button class="button" value="center">
					Center				</button>
				<button class="button" value="right">
					Right				</button>
				<button class="button active" value="none">
					None				</button>
			</div>
		</div>

		<div class="setting link-to">
			<span>Link To</span>
			<div class="button-group button-large" data-setting="link">
				<button class="button" value="file">
					Image URL				</button>
				<button class="button" value="custom">
					Custom URL				</button>
				<button class="button active" value="none">
					None				</button>
			</div>
			<input type="text" class="link-to-custom" data-setting="linkUrl" />
		</div>
	</script>

	<script type="text/html" id="tmpl-attachments-css">
		<style type="text/css" id="{{ data.id }}-css">
			#{{ data.id }} {
				padding: 0 {{ data.gutter }}px;
			}

			#{{ data.id }} .attachment {
				margin: {{ data.gutter }}px;
				width: {{ data.edge }}px;
			}

			#{{ data.id }} .attachment-preview,
			#{{ data.id }} .attachment-preview .thumbnail {
				width: {{ data.edge }}px;
				height: {{ data.edge }}px;
			}

			#{{ data.id }} .portrait .thumbnail img {
				max-width: {{ data.edge }}px;
				height: auto;
			}

			#{{ data.id }} .landscape .thumbnail img {
				width: auto;
				max-height: {{ data.edge }}px;
			}
		</style>
	</script>
		<div id="local-storage-notice" class="hidden">
	<p class="local-restore">
		The backup of this post in your browser is different from the version below.		<a class="restore-backup" href="#">Restore the backup.</a>
	</p>
	<p class="undo-restore hidden">
		Post restored successfully.		<a class="undo-restore-backup" href="#">Undo.</a>
	</p>
	</div>
        <link rel='stylesheet' href='<?php echo $url_admin; ?>load-styles.php?c=1&amp;dir=ltr&amp;load=admin-bar,buttons,media-views,wp-admin&amp;ver=3.6.1' type='text/css' media='all' />
        
        <link rel='stylesheet' href='<?php echo $this->theurl;?>fontawesome/font-awesome.min.css'></link>
<link rel="stylesheet" href="<?php echo $this->theurl;?>colorpicker/farbtastic.css">
<script src="<?php echo $this->theurl;?>colorpicker/farbtastic.js"></script>
<link rel="stylesheet" href="<?php echo $this->theurl;?>dzstooltip/dzstooltip.css">
<link rel="stylesheet" href="<?php echo $this->theurl;?>dzstabsandaccordions/dzstabsandaccordions.css">
<script src="<?php echo $this->theurl;?>dzstabsandaccordions/dzstabsandaccordions.js"></script>
        </body>
</html><?php
    }

}
    
