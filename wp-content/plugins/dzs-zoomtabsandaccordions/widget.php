<?php
add_action('widgets_init', array('dzstaa_widget', 'register_this_widget'));

class dzstaa_widget extends WP_Widget {

    public $name = "ZoomTabs and Accordions";
    public $control_options = array();

    function __construct() {
        $wdesc = '';
        if (isset($this->widget_desc))
            $wdesc = $this->widget_desc;
        $widget_options = array(
            'classname' => __CLASS__,
            'description' => $wdesc,
        );
        parent::__construct(__CLASS__, $this->name, $widget_options, $this->control_options);
    }

    //!!! Static Functions
    static function register_this_widget() {
        register_widget(__CLASS__);
    }

    function form($instance) {

        $defaults = array('zoomtabs_content' => '', 'title' => 'Tabs and Accordions');
        $instance = wp_parse_args((array) $instance, $defaults);
        ?>
        <p>        
            <label for="<?php echo $this->get_field_id('title'); ?>">Title</label><br/>
            <input type="text" name="<?php echo $this->get_field_name('title') ?>" id="<?php echo $this->get_field_id('title') ?> " value="<?php echo $instance['title'] ?>" size="20"> </p>
        <label style="vertical-align: top" for="<?php echo $this->get_field_id('zoomtabs_content'); ?>">Content</label> - <span class="dzstaa-widget-generate">Generate</span><br/>
        <textarea name="<?php echo $this->get_field_name('zoomtabs_content') ?>" id="<?php echo $this->get_field_id('zoomtabs_content') ?> "  size="20"><?php echo $instance['zoomtabs_content'] ?></textarea> 
</p>
        <p>
            <?php
        }

        function widget($args, $instance) {
            global $dzstaa;
            extract($args);

            $title = $instance['title'];
            //$title = $instance['dzstaaid'];



            echo $before_widget;
            echo $before_title;
            echo $title;
            echo $after_title;


            //do_shortcode('[phoenixgallery]');

            echo do_shortcode(stripslashes($instance['zoomtabs_content']));


            echo $after_widget;
        }

    }
    