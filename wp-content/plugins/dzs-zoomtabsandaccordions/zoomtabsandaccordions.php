<?php
/*
  Plugin Name: DZS ZoomTabs and Accordions
  Plugin URI: http://digitalzoomstudio.net/
  Description: Easy to use Tabs and Accordions plugin.
  Version: 1.40
  Author: Digital Zoom Studio
  Author URI: http://digitalzoomstudio.net/
 */
include_once(dirname(__FILE__).'/dzs_functions.php');
if(!class_exists('DZSZoomTabsandAccordinos')){
    include_once(dirname(__FILE__).'/class-dzstaa.php');
}

define("DZSTAA_VERSION", "1.40");


$dzstaa = new DZSZoomTabsandAccordinos();


if(isset($_GET['dzstaa_show_generator']) && $_GET['dzstaa_show_generator']=='on'){
    echo $dzstaa->show_generator();
    die();
}

include_once(dirname(__FILE__) . '/widget.php');