<?php 

// Create Slider
 
    function realhost_bxslider_template() {
 
        // Query Arguments
        $args = array(
            'post_type' => 'bxslider',
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'posts_per_page' => -1,
            'ignore_sticky_posts' => 1,
        );  
 
        // The Query
        $the_query = new WP_Query( $args );
 
        // Check if the Query returns any posts
        if ( $the_query->have_posts() ) {
 
            // Start the Slider ?>
            <div id="slider">
 
                <?php
                while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                    <div class="slide">

                        <?php // Check if there's a BxSlider h1 given and if so post it
                        if ( get_post_meta( get_the_id(), 'bxslide_metabox_h1', true) != '' ) { ?>
                            <h1><?php echo get_post_meta( get_the_id(), 'bxslide_metabox_h1', true); ?></h1>
                        <?php } ?>

                        <?php // Check if there's a BxSlider h6 given and if so post it
                        if ( get_post_meta( get_the_id(), 'bxslide_metabox_h6', true) != '' ) { ?>
                            <h6><?php echo get_post_meta( get_the_id(), 'bxslide_metabox_h6', true); ?></h6>
                        <?php }

                        // Check if there's a BxSlider button link or button name given and if so post it
                        if ( get_post_meta( get_the_id(), 'bxslide_metabox_link', true) != '' || get_post_meta( get_the_id(), 'bxslide_metabox_name', true) != '' ) { ?>
                            <a href="<?php echo esc_url( get_post_meta( get_the_id(), 'bxslide_metabox_link', true) ); ?>" class="button"><?php echo get_post_meta( get_the_id(), 'bxslide_metabox_name', true); ?></a>
                        <?php } ?>

                    </div>
                <?php endwhile; } ?>

            </div>
                
            <div class="clear"></div>
 
            <?php 
 
        // Reset Post Data
        wp_reset_postdata();
    }

?>