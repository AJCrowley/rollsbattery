<?php
 
// Create BxSlider Custom Post Type
     
function register_bxslider_posttype() {
    $labels = array(
        'name'              => _x( 'BxSlider', 'post type general name', 'realhost' ),
        'singular_name'     => _x( 'BxSlide', 'post type singular name', 'realhost' ),
        'add_new'           => __( 'Add New BxSlide', 'realhost' ),
        'add_new_item'      => __( 'Add New BxSlide', 'realhost' ),
        'edit_item'         => __( 'Edit BxSlide', 'realhost' ),
        'new_item'          => __( 'New BxSlide', 'realhost' ),
        'view_item'         => __( 'View BxSlide', 'realhost' ),
        'search_items'      => __( 'Search BxSlides', 'realhost' ),
        'not_found'         => __( 'BxSlide', 'realhost' ),
        'not_found_in_trash'=> __( 'BxSlide', 'realhost' ),
        'parent_item_colon' => __( 'BxSlide', 'realhost' ),
        'menu_name'         => __( 'BxSlides', 'realhost' )
    );

    $taxonomies = array();

    $supports = array('title','page-attributes');

    $post_type_args = array(
        'labels'            => $labels,
        'singular_label'    => __('BxSlide', 'realhost'),
        'public'            => true,
        'show_ui'           => true,
        'orderby'           => 'ID',
        'order'             => 'ASC',
        'publicly_queryable'=> true,
        'query_var'         => true,
        'capability_type'   => 'post',
        'exclude_from_search' => true,
        'has_archive'       => false,
        'hierarchical'      => false,
        'rewrite'           => array( 'slug' => 'bxslider', 'with_front' => false ),
        'supports'          => $supports,
        'taxonomies'        => $taxonomies
    );
    register_post_type('bxslider',$post_type_args);
}
add_action('init', 'register_bxslider_posttype');


// Add metabox

$bxslide_metabox = array( 
    'id' => 'bxslide_metabox',
    'title' => 'BxSlide',
    'page' => array('bxslider'),
    'context' => 'normal',
    'priority' => 'default',
    'fields' => array(   
        array(
            'name'          => 'BxSlide Heading H1',
            'desc'          => '',
            'id'            => 'bxslide_metabox_h1',
            'class'         => 'bxslide_metabox_h1',
            'type'          => 'text',
            'std'           => '',
            'rich_editor'   => 0,            
            'max'           => 0             
        ),
        array(
            'name'          => 'BxSlide Heading H6',
            'desc'          => '',
            'id'            => 'bxslide_metabox_h6',
            'class'         => 'bxslide_metabox_h6',
            'type'          => 'text',
            'std'           => '',
            'rich_editor'   => 0,            
            'max'           => 0             
        ),
        array(
            'name'          => 'BxSlide Button Name',
            'desc'          => '',
            'id'            => 'bxslide_metabox_name',
            'class'         => 'bxslide_metabox_name',
            'type'          => 'text',
            'std'           => '',
            'rich_editor'   => 0,            
            'max'           => 0             
        ),
        array(
            'name'          => 'BxSlider Button Link',
            'desc'          => '',
            'id'            => 'bxslide_metabox_link',
            'class'         => 'bxslide_metabox_link',
            'type'          => 'text',
            'std'           => '',
            'rich_editor'   => 0,            
            'max'           => 0             
        ),
    )
);          
         
add_action('admin_menu', 'add_bxslide_metabox');
function add_bxslide_metabox() {
 
    global $bxslide_metabox;        
 
    foreach($bxslide_metabox['page'] as $page) {
        add_meta_box($bxslide_metabox['id'], $bxslide_metabox['title'], 'show_bxslide_metabox', $page, 'normal', 'default', $bxslide_metabox);
    }
}
 
// Function to show meta boxes
function show_bxslide_metabox()  {
    global $post;
    global $bxslide_metabox;
    global $realhost_prefix;
    global $wp_version;
     
    // Use nonce for verification
    echo '<input type="hidden" name="bxslide_metabox_nonce" value="', wp_create_nonce(basename(__FILE__)), '" />';
     
    echo '<table class="form-table">';
 
    foreach ($bxslide_metabox['fields'] as $field) {
        // Get current post meta data
 
        $meta = get_post_meta($post->ID, $field['id'], true);
         
        echo '<tr>',
                '<th style="width:20%"><label for="', $field['id'], '">', stripslashes($field['name']), '</label></th>',
                '<td class="wptuts_field_type_' . str_replace(' ', '_', $field['type']) . '">';
        switch ($field['type']) {
            case 'text':
                echo '<input type="text" name="', $field['id'], '" id="', $field['id'], '" value="', $meta ? $meta : $field['std'], '" size="50" style="width:97%" /><br/>', '', stripslashes($field['desc']);
                break;
        }
        echo    '<td>',
            '</tr>';
    }
     
    echo '</table>';
}   
 
// Save data from meta box
add_action('save_post', 'bxslide_metabox_save');
function bxslide_metabox_save($post_id) {
    global $post;
    global $bxslide_metabox;
     
    // Verify nonce
    if (!isset($_POST['bxslide_metabox_nonce']) || !wp_verify_nonce($_POST['bxslide_metabox_nonce'], basename(__FILE__))) {
        return $post_id;
    }
 
    // Check autosave
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return $post_id;
    }
 
    // Check permissions
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id)) {
            return $post_id;
        }
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
     
    foreach ($bxslide_metabox['fields'] as $field) {
     
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
         
        if ($new && $new != $old) {
            if($field['type'] == 'date') {
                $new = realhost_format_date($new);
                update_post_meta($post_id, $field['id'], $new);
            } else {
                if(is_string($new)) {
                    $new = $new;
                } 
                update_post_meta($post_id, $field['id'], $new);
                 
                 
            }
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
?>