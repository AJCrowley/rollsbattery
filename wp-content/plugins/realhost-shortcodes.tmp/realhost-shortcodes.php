<?php
/*
Plugin Name: RealHost Shortcodes
Description: Shortcodes for the RealHost theme.
Plugin URI: http://danthemes.com
Author: DanThemes
Author URI: http://danthemes.com
Version: 1.0
*/

define( 'TEMPLATE_URL', get_template_directory_uri() );



/*******************************************************************************************************/
/* BxSlider */
/*******************************************************************************************************/
 
require( 'bxslider_post_type.php' );
require( 'bxslider.php' );




/*******************************************************************************************************/
/* Include the style.css file */
/*******************************************************************************************************/
add_action( 'wp_enqueue_scripts', 'realhost_shortcodes_style' );
function realhost_shortcodes_style() {
    // Respects SSL, style.css is relative to the current file
    wp_register_style( 'realhost_shortcodes_main_style', plugins_url('style.css', __FILE__) );
    wp_enqueue_style( 'realhost_shortcodes_main_style' );
}




/*******************************************************************************************************/
/* Shortcode for the list */
/*******************************************************************************************************/
add_shortcode('list', 'list_function');
function list_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<ul class="menu">'.do_shortcode($content).'</ul>';
}
add_shortcode('list_item', 'list_item_function');
function list_item_function($atts) {
	extract(shortcode_atts(array(
		'icon' => 'icon-ok',
		'text' => ''
	), $atts));
	return '<li><i class="'.sanitize_html_class($icon).'"></i>'.esc_html($text).'</li>';
}




/*******************************************************************************************************/
/* Shortcode for the google maps */
/*******************************************************************************************************/
add_shortcode("googlemap", "googlemap_function");
function googlemap_function($atts) {
    extract(shortcode_atts(array(
      "width" => '100%',
      "height" => '350',
      "latitude" => '44.431512',
      "longitude" => '26.103923',
      "zoom" => '15'
    ), $atts));
    return '<iframe id="google_map" width="'.esc_attr($width).'" height="'.esc_attr($height).'" src="https://maps.google.com/?ie=UTF8&amp;ll='.esc_attr($latitude).','.esc_attr($longitude).'&amp;spn=0.016947,0.042272&amp;t=m&amp;z='.esc_attr($zoom).'&amp;output=embed" ></iframe>';
}



/*******************************************************************************************************/
/* Shortcode for stats element */
/*******************************************************************************************************/
add_shortcode("stats", "stats_function");
function stats_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '</div><div class="stats_wrapper"><div class="wrap">'.do_shortcode($content).'</div></div><div class="wrap">';
}
add_shortcode("stats_item", "stats_item_function");
function stats_item_function($atts) {
    extract(shortcode_atts(array(
      'icon' => 'icon-heart',
      'number' => '999',
      'text' => 'text'
    ), $atts));
    return '<div class="stats"><i class="'.sanitize_html_class($icon).'"></i><h3>'.esc_html($number).'</h3><h6>'.esc_html($text).'</h6></div>';
}



/*******************************************************************************************************/
/* Shortcode for partners (logos) slider */
/*******************************************************************************************************/
add_shortcode("partners", "partners_function");
function partners_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '<ul id="partners">'.do_shortcode($content).'</ul>';
}
add_shortcode("partner", "partner_function");
function partner_function($atts) {
    extract(shortcode_atts(array(
      'image_url' => '',
      'link' => ''
    ), $atts));
    return '<li><a href="'.esc_url($link).'"><img src="' . TEMPLATE_URL . '/'.esc_attr($image_url).'" alt="" /></a></li>';
}



/*******************************************************************************************************/
/* Shortcode for table */
/*******************************************************************************************************/
add_shortcode("table", "table_function");
function table_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '<table class="demotable" border="0" cellspacing="0" cellpadding="0">' . do_shortcode($content) . '</table>';
}
add_shortcode("table_row", "table_row_function");
function table_row_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '<tr>' . do_shortcode($content) . '</tr>';
}
add_shortcode("table_head", "table_head_function");
function table_head_function($atts, $content = null) {
    extract(shortcode_atts(array(
    	'icon' => ''
    ), $atts));
    return '<th><i class="'.sanitize_html_class($icon).'"></i>' . do_shortcode($content) . '</th>';
}
add_shortcode("table_data", "table_data_function");
function table_data_function($atts, $content = null) {
    extract(shortcode_atts(array(
    	'icon' => ''
    ), $atts));
    return '<td><i class="'.sanitize_html_class($icon).'"></i>' . do_shortcode($content) . '</td>';
}





/*******************************************************************************************************/
/* Shortcode for highlight */
/*******************************************************************************************************/
add_shortcode("highlight", "highlight_function");
function highlight_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '<span class="highlight">' . esc_html($content) . '</span>';
}




/*******************************************************************************************************/
/* Shortcode for icon */
/*******************************************************************************************************/
add_shortcode("icon", "icon_function");
function icon_function($atts) {
    extract(shortcode_atts(array(
    	'icon' => '',
    	'text' => ''
    ), $atts));
    return '<span class="icon_list"><i class="'.sanitize_html_class($icon).' icon"></i>'.esc_html($text).'</span>';
}




/*******************************************************************************************************/
/* Shortcodes for adding some space */
/*******************************************************************************************************/
add_shortcode('space', 'space_function');
function space_function($atts) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<div class="clear"></div><div class="space"></div>';
}
add_shortcode('small_space', 'small_space_function');
function small_space_function($atts) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<div class="clear"></div><div class="space2"></div>';
}




/*******************************************************************************************************/
/* Shortcode for the message box */
/*******************************************************************************************************/
add_shortcode('message', 'message_function');
function message_function($atts) {
	extract(shortcode_atts(
		array(
			'title' => 'Title',
			'text' => 'text',
			'button_name' => 'Live Chat',
			'button_link' => '#'
		), $atts
	));
	$return = '</div><div class="message"><div class="wrap"><h3>'. esc_html($title) .'</h3><h5>'. esc_html($text) .'</h5>';
	$return .= '<a href="'. esc_url($button_link) .'" class="button red">'. esc_html($button_name) .'</a></div></div><div class="wrap">';
	return $return;
}




/*******************************************************************************************************/
/* Shortcode for the feature element */
/*******************************************************************************************************/
add_shortcode("feature", "feature_function");
function feature_function($atts) {
    extract(shortcode_atts(array(
    	'icon' => 'icon-ok',
    	'title' => '',
    	'text' => ''
    ), $atts));
    return '<div class="feature"><i class="'.sanitize_html_class($icon).'"></i><h6>'.esc_html($title).'</h6><p>'.esc_html($text).'</p></div>';
}




/*******************************************************************************************************/
/* Shortcode for the feature_2 element */
/*******************************************************************************************************/
add_shortcode("feature_2_left", "feature_2_left_function");
function feature_2_left_function($atts) {
    extract(shortcode_atts(array(
    	'icon' => 'icon-ok',
    	'title' => '',
    	'text' => ''
    ), $atts));
    return '<div class="one_half"><div class="feature_2 feature_2_left"><span><i class="'.sanitize_html_class($icon).'"></i></span><h6>'.esc_html($title).'</h6><p>'.esc_html($text).'</p></div></div>';
}
add_shortcode("feature_2_right", "feature_2_right_function");
function feature_2_right_function($atts) {
    extract(shortcode_atts(array(
    	'icon' => 'icon-ok',
    	'title' => '',
    	'text' => ''
    ), $atts));
    return '<div class="one_half last"><div class="feature_2 feature_2_right"><span><i class="'.sanitize_html_class($icon).'"></i></span><h6>'.esc_html($title).'</h6><p>'.esc_html($text).'</p></div></div>';
}




/*******************************************************************************************************/
/* Shortcode for the bold element */
/*******************************************************************************************************/
add_shortcode("bold", "bold_function");
function bold_function($atts, $content = null) {
    extract(shortcode_atts(array(), $atts));
    return '<strong>'.esc_attr($content).'</strong>';
}




/*******************************************************************************************************/
/* Shortcode for a testimonials */
/*******************************************************************************************************/
add_shortcode('testimonials', 'testimonials_function');
function testimonials_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));

	return '<div class="testimonials">' . do_shortcode($content) . '</div>';
}

add_shortcode('testimonial', 'testimonial_function');
function testimonial_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'picture_url' => 'images/user.png',
			'name' => 'John Doe',
			'role' => 'Executive Manager',
			'website_url' => 'danthemes.com'
		), $atts
	));
	$return = '<blockquote><div class="testimonials_img"><img src="' . TEMPLATE_URL . '/'.esc_attr($picture_url).'" alt="" /></div>';
	$return .= '<div class="testimonials_text"><p><i class="icon-quote-left"></i>' . do_shortcode($content) . '<i class="icon-quote-right"></i>';
	$return .= '<cite><strong>'.esc_html($name).'</strong> - '.esc_html($role).' - '.esc_html($website_url).' </cite></p></div></blockquote>';

	return $return;
}




/*******************************************************************************************************/
/* Shortcode for tabs */
/*******************************************************************************************************/
add_shortcode( 'tabs', 'tabs_function' );
	function tabs_function( $atts, $content ){
	extract(shortcode_atts(
		array(), $atts
	));
	$GLOBALS['tab_count'] = 0;
	$GLOBALS['tab_count_2'] = 0;

	do_shortcode( $content );
	 
	if( is_array( $GLOBALS['tabs'] ) ){
		foreach( $GLOBALS['tabs'] as $tab ){
			$tabs[] = '<li><a href="#tab'.$GLOBALS['tab_count_2'].'">';
			$tabs[] .= '<span><i class="'.$tab['icon'].'"></i></span>'.$tab['title'].'</a></li>';
			$tab_body[] = '<div id="tab'.$GLOBALS['tab_count_2'].'"><h6>'.$tab['title'].'</h6>'.$tab['content'].'</div>';

			$GLOBALS['tab_count_2']++;
		}
		$return = "\n".'<div id="tabs"><ul>'.implode( "\n", $tabs ).'</ul><div class="tabs_panel">'."\n".implode( "\n", $tab_body ).'</div></div>'."\n";
	}
	return $return;
}
 
add_shortcode( 'tab', 'tab_function' );
	function tab_function( $atts, $content ){
	extract(shortcode_atts(array(
		'icon' => 'icon-ok',
		'title' => 'Tab-%d'
	), $atts));

	$x = $GLOBALS['tab_count'];
	$GLOBALS['tabs'][$x] = array( 'title' => sprintf( $title, $GLOBALS['tab_count'] ), 'icon' => sprintf( $icon, $GLOBALS['tab_count'] ), 'content' =>  do_shortcode($content) ); 

	$GLOBALS['tab_count']++;
}




/*******************************************************************************************************/
/* Shortcode for accordion */
/*******************************************************************************************************/
add_shortcode('accordion', 'accordion_function');
function accordion_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<div class="accordion">'. do_shortcode($content) .'</div>';
}
add_shortcode('accordion_item', 'accordion_item_function');
function accordion_item_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'icon' => '',
			'title' => ''
		), $atts
	));
	$return = '';
	if($icon == true) {
		$return .= '<h6><i class="'.sanitize_html_class($icon).'"></i>'.esc_html($title).'</h6><div>'.do_shortcode($content).'</div>';
	} else {
		$return .= '<h6>'.esc_html($title).'</h6><div>'.do_shortcode($content).'</div>';
	}

	return $return;
}




/*******************************************************************************************************/
/* Shortcode for the buttons */
/*******************************************************************************************************/
add_shortcode('button', 'button_function');
function button_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'size' => 'big',
			'color' => '',
			'link' => ''
		), $atts
	));
	return '<a href="'.esc_url($link).'" class="button '.esc_attr($size).' '.esc_attr($color).'">'.esc_html($content).'</a>';
}





/*******************************************************************************************************/
/* Shortcode for the headline */
/*******************************************************************************************************/
add_shortcode('headline', 'headline_function');
function headline_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h1 class="heading"><span>'.do_shortcode($content).'</span></h1>';
}




/*******************************************************************************************************/
/* Shortcode for the h1, h2, h3, h4, h5, h6 */
/*******************************************************************************************************/
add_shortcode('h1', 'h1_function');
function h1_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h1>'.do_shortcode($content).'</h1>';
}
add_shortcode('h2', 'h2_function');
function h2_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h2>'.do_shortcode($content).'</h2>';
}
add_shortcode('h3', 'h3_function');
function h3_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h3>'.do_shortcode($content).'</h3>';
}
add_shortcode('h4', 'h4_function');
function h4_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h4>'.do_shortcode($content).'</h4>';
}
add_shortcode('h5', 'h5_function');
function h5_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h5>'.do_shortcode($content).'</h5>';
}
add_shortcode('h6', 'h6_function');
function h6_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<h6>'.do_shortcode($content).'</h6>';
}




/*******************************************************************************************************/
/* Shortcode for the columns */
/*******************************************************************************************************/

/* one_half column */
add_shortcode('one_half', 'one_half_function');
function one_half_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_half">' . do_shortcode($content) . '</div>';
}

/* one_half last column */
add_shortcode('one_half_last', 'one_half_last_function');
function one_half_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_half last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* one_third column */
add_shortcode('one_third', 'one_third_function');
function one_third_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_third">' . do_shortcode($content) . '</div>';
}

/* one_third last column */
add_shortcode('one_third_last', 'one_third_last_function');
function one_third_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_third last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* two_thirds column */
add_shortcode('two_thirds', 'two_thirds_function');
function two_thirds_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="two_thirds">' . do_shortcode($content) . '</div>';
}

/* two_thirds last column */
add_shortcode('two_thirds_last', 'two_thirds_last_function');
function two_thirds_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="two_thirds last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* one_fourth column */
add_shortcode('one_fourth', 'one_fourth_function');
function one_fourth_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fourth">' . do_shortcode($content) . '</div>';
}

/* one_fourth last column */
add_shortcode('one_fourth_last', 'one_fourth_last_function');
function one_fourth_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fourth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* one_fifth column */
add_shortcode('one_fifth', 'one_fifth_function');
function one_fifth_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fifth">' . do_shortcode($content) . '</div>';
}

/* one_fifth last column */
add_shortcode('one_fifth_last', 'one_fifth_last_function');
function one_fifth_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}


/*******************************************************************************************************/
/* Shortcodes for the inside columns */
/*******************************************************************************************************/

/* one_half inside column */
add_shortcode('one_half_inside', 'one_half_inside_function');
function one_half_inside_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_half">' . do_shortcode($content) . '</div>';
}

/* one_half inside last column */
add_shortcode('one_half_inside_last', 'one_half_inside_last_function');
function one_half_inside_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_half last">' . do_shortcode($content) . '</div>';
}

/* one_third inside column */
add_shortcode('one_third_inside', 'one_third_inside_function');
function one_third_inside_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_third">' . do_shortcode($content) . '</div>';
}

/* one_third inside last column */
add_shortcode('one_third_inside_last', 'one_third_inside_last_function');
function one_third_inside_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_third last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* two_thirds inside column */
add_shortcode('two_thirds_inside', 'two_thirds_inside_function');
function two_thirds_inside_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="two_thirds">' . do_shortcode($content) . '</div>';
}

/* two_thirds inside last column */
add_shortcode('two_thirds_inside_last', 'two_thirds_inside_last_function');
function two_thirds_inside_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="two_thirds last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* one_fourth inside column */
add_shortcode('one_fourth_inside', 'one_fourth_inside_function');
function one_fourth_inside_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fourth">' . do_shortcode($content) . '</div>';
}

/* one_fourth inside last column */
add_shortcode('one_fourth_inside_last', 'one_fourth_inside_last_function');
function one_fourth_inside_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fourth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}

/* one_fifth inside column */
add_shortcode('one_fifth_inside', 'one_fifth_inside_function');
function one_fifth_inside_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fifth">' . do_shortcode($content) . '</div>';
}

/* one_fifth inside last column */
add_shortcode('one_fifth_inside_last', 'one_fifth_inside_last_function');
function one_fifth_inside_last_function($atts, $content = null) {
	extract(shortcode_atts(array(), $atts));
	return '<div class="one_fifth last">' . do_shortcode($content) . '</div><div class="clear"></div>';
}




/*******************************************************************************************************/
/* Shortcode for the pricing tables */
/*******************************************************************************************************/

add_shortcode('pricing_table', 'pricing_table_function');
function pricing_table_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'columns' => ''
		), $atts
	));
	global $pricing_table_columns;
	$pricing_table_columns = $columns;
	if ($pricing_table_columns == '1') { $pricing_table_columns = ''; }
	elseif ($pricing_table_columns == '2') { $pricing_table_columns = 'one_half'; }
	elseif ($pricing_table_columns == '3') { $pricing_table_columns = 'one_third'; }
	elseif ($pricing_table_columns == '4') { $pricing_table_columns = 'one_fourth'; }
	elseif ($pricing_table_columns == '5') { $pricing_table_columns = 'one_fifth'; }
	return '<div class="pricing_table">' . do_shortcode($content) . '</div>';
}
add_shortcode('pricing_column', 'pricing_column_function');
function pricing_column_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'name' => 'Plan Name',
			'price' => '',
			'button_name' => 'Order Now',
			'button_link' => ''
		), $atts
	));
	global $pricing_table_columns;
	$return = '';
	$return .= '<div class="'.esc_attr($pricing_table_columns).' pricing">';
	$return .= '<div class="pricing_top"><h6>'.esc_html($name).'</h6><p><sup>$</sup>'.esc_html($price).'</p></div>';
	$return .= '<div class="pricing_middle"><ul>' . do_shortcode($content) . '</ul></div>';
	$return .= '<div class="pricing_bottom"><a href="'.esc_url($button_link).'">'.esc_html($button_name).'</a></div></div>';
	return $return;
}
add_shortcode('pricing_text', 'pricing_text_function');
function pricing_text_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'icon' => 'icon-ok'
		), $atts
	));
	return '<li><i class="'.sanitize_html_class($icon).'"></i>' . do_shortcode($content) . '</li>';
}




/*******************************************************************************************************/
/* Shortcode for the dedicated servers table */
/*******************************************************************************************************/

add_shortcode('dedicated_servers', 'dedicated_servers_function');
function dedicated_servers_function($atts, $content = null) {
	extract(shortcode_atts(
		array(), $atts
	));
	return '<div id="dedicated_servers">
				<div class="ds_heading">
					<div class="ds_processor">Processor</div>
					<div class="ds_ram">RAM</div>
					<div class="ds_cores">No. of Cores</div>
					<div class="ds_diskspace">Disk Space</div>
					<div class="ds_bandwidth">Bandwidth</div>
					<div class="ds_price">Price / month</div>
					<div class="ds_order"></div>
					<div class="clear"></div>
				</div>'. do_shortcode($content) .'</div>';
}
add_shortcode('dedicated_server', 'dedicated_server_function');
function dedicated_server_function($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'processor' => '',
			'ram' => '',
			'cores' => '',
			'diskspace' => '',
			'bandwidth' => '',
			'price' => '',
			'button_name' => 'Order Now',
			'button_url' => ''
		), $atts
	));

	$return = '<div class="ds"><div class="ds_processor">'.esc_html($processor).'</div>';
	$return .= '<div class="ds_ram">'.esc_html($ram).'</div><div class="ds_cores">'.esc_html($cores).'</div><div class="ds_diskspace">'.esc_html($diskspace).'</div>';
	$return .= '<div class="ds_bandwidth">'.esc_html($bandwidth).'</div><div class="ds_price">$'.esc_html($price).'</div>';
	$return .= '<div class="ds_order"><a href="'.esc_url($button_url).'" class="button">'.esc_html($button_name).'</a></div><div class="clear"></div></div>';

	return $return;
}



?>