<?php
/**
 * CSV To MySQL
 *
 * Choose a table from database.
 * Line exceeds warning where long HTML tag has.
 *
 * PHP version 5.4.3
 *
 * @category  Import
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

require_once '../config.php';
require_once '../Classes/PHPExcel.php';


tokenChecker('index.php');

tokenGenerator();


if ((!isset($_FILES['filename']) || !isset($_POST['delimiter'])
    || !isset($_POST['url']) || !isset($_POST['file_type'])) && !isset($_SESSION['filename'])
) {
    gotoheader('./index.php');
}



$mimetypes = array('text/csv');

// make tmp directory
$directory = '../tmp/';
if (!is_dir($directory)) {
    mkdir($directory, 0755, true);
}


// save file to server
if (isset($_FILES['filename']) && !empty($_FILES['filename']['name'])) {

    $_SESSION['has_header'] = (isset($_POST['has_header']));

    if (empty($_POST['delimiter'])) {
        gotoheader('./index.php?msg=1');
    } else {
        $_SESSION['delimiter'] = $_POST['delimiter'];

        if ($_FILES['filename']['error'] > 0) {
            gotoheader('./index.php?msg=2');
        } else {
            $filename = $directory . $_FILES['filename']['name'];
            $ext = explode('.', $_FILES['filename']['name']);
            $ext = strtolower(end($ext));

            // check the file is a csv
            if ($ext === 'csv') {

                $movefile = move_uploaded_file(
                    $_FILES['filename']['tmp_name'],
                    $filename
                );
                if ($movefile) {

                    try {
                        $inputFileType = PHPExcel_IOFactory::identify($filename);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        if (file_exists($filename) && filesize($filename) > 0
                            && $objReader->canRead($filename)
                        ) {
                            $_SESSION['filename'] = $filename;
                        } else {
                            gotoheader('./index.php?msg=4');
                        }
                    }
                    catch(Exception $e) {
                        gotoheader('./index.php?msg=4');
                    }

                } else {
                    gotoheader('./index.php?msg=3');
                }

            } else {
                gotoheader('./index.php?msg=4');
            }
        }
    }

} else if (!empty($_POST['url'])) {
// save file to server from url

    if (empty($_POST['delimiter'])) {
        gotoheader('./index.php?msg=1');
    } else {

        $url = str_replace(
            array(' ', '&'),
            array('%20', '%26'),
            $_POST['url']
        );

        if (VALIDATE_URL($url)) {

            $header = get_headers($url, 1);

            if (isset($header['Content-Type'])
                && in_array($header['Content-Type'], $mimetypes)
            ) {

                $filename = $url;
                $filename = str_replace(
                    array('%20', '%26'),
                    array('_', '_'),
                    $filename
                );
                $filename = explode('/', $filename);
                $filename = mb_strtolower(end($filename));
                $ext = explode('.', $filename);
                $ext = strtolower(end($ext));

                // check the file is a csv
                if ($ext === 'csv') {

                    $filename = $directory . time() . '_' . $filename;

                    file_put_contents($filename, file_get_contents($url));

                    if (file_exists($filename)) {

                        $inputFileType = PHPExcel_IOFactory::identify($filename);
                        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                        if (file_exists($filename) && filesize($filename) > 0
                            && $objReader->canRead($filename)
                        ) {
                            $_SESSION['filename'] = $filename;
                        } else {
                            gotoheader('./index.php?msg=4');
                        }

                    } else {
                        gotoheader('./index.php?msg=6');
                    }

                } else {
                    gotoheader('./index.php?msg=4');
                }

            } else {
                gotoheader('./index.php?msg=4');
            }

        } else {
            gotoheader('./index.php?msg=5');
        }
    }

} else {

    gotoheader('./index.php?msg=4');

}
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[0]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

<!--
        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>

        <h2 class="subtitle"><?php echo $_TEXT[0]; ?></h2>
-->

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-3 done">
                    <span>1. <?php echo $_TEXT[1]; ?></span>
                </h3>
                <h3 class="col-3 current">
                    <span>2. <?php echo $_TEXT[2]; ?></span>
                </h3>
                <h3 class="col-3">
                    <span>3. <?php echo $_TEXT[3]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

                <div class="msg error">
<?php
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
    case '1':       echo $_TEXT[23];
        break;
    case 'token':   echo $_TEXT[54];
        break;
    }
}
?>
                </div>

                <form method="POST" action="import.php">
                    <div class="zebra">
<?php
$tablessql = "SHOW TABLES FROM `" . $_CONFIG['db_name'] . "`";
$tablessql = mysql_query($tablessql);

if ($tablessql != false && mysql_num_rows($tablessql) > 0) {
    $checked = ' checked="checked"';
    while ($table = mysql_fetch_row($tablessql)) {
        echo '
        <div class="form-row clear">
            <input type="radio" name="table" id="table_' . $table[0]
            . '" value="' . $table[0] . '"' . $checked . ' />
            <label for="table_' . $table[0] . '">' . $table[0] . '</label>
        </div>';
        $checked = '';
    }

    echo '
    <div class="form-row clear">
        <input type="hidden" name="token" value="' . $_SESSION['token'] . '" />
        <input type="hidden" name="tables" value="save" />
        <input value="' . $_TEXT[22] . '" type="submit" />
    </div>';
} else {
    echo '<div class="msg error">' . $_TEXT[24] . '</div>';
}
?>
                    </div>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->
<!--
        <aside id="copy">
            <?php echo $_TEXT[30]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>
-->

    </div> <!-- .page-inner -->

</body>
</html>
<?php
mysql_close();
?>
