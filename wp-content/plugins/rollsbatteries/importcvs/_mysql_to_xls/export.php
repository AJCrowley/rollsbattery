<?php
/**
 * MySQL to CSV
 *
 * Pair columns end export to CSV file.
 * Line exceeds warning where long HTML tag has.
 *
 * PHP version 5.4.3
 *
 * @category  Export
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


require_once '../config.php';


tokenChecker('index.php');

tokenGenerator();


if ((!isset($_POST['tables']))
    && (!isset($_SESSION['exptables']))
) {
    gotoheader('./index.php?msg=1');
}

if (!isset($_SESSION['exptables'])
    || (isset($_POST['tablessave'])
    && $_POST['tablessave'] == 'save')
) {
    if (is_array($_POST['tables']) && count($_POST['tables']) > 0) {
        $_SESSION['exptables'] = $_POST['tables'];
    } else {
        gotoheader('./index.php?msg=1');
    }
}

if (isset($_POST['export']) && $_POST['export'] == 'save') {
    if (isset($_POST['filename']) && is_array($_POST['filename'])) {

        if (isset($_POST['file_type']) && $_POST['file_type'] == 'xls') {
            $fiel_save_type = 'Excel5';
            $file_type = 'xls';
        } else {
            $fiel_save_type = 'Excel2007';
            $file_type = 'xlsx';
        }

        $files = '';
        // tables name
        foreach ($_POST['filename'] as $key => $value) {

            $filename = trim($value);
            if ($filename == '') {
                $filename = $key;
            }

            // make directory
            $directory = '../data/';
            if (!is_dir($directory)) {
                mkdir($directory, 0755, true);
            }

            // columns of table
            $table_columns = array();
            $table_columns['table'] = $key;
            foreach ($_POST['table'][$key] as $colname => $colalpha) {
                if ($colalpha == '') {
                    continue;
                }
                $table_columns['data'][strtoupper($colalpha)] = $colname;
            }

            if (isset($table_columns['data'])
                && count($table_columns['data']) > 0
            ) {

                $result = array();
                $result[1] = $table_columns['data'];
                $sql = "SELECT `"
                . implode('`,`', array_values($table_columns['data']))
                . "` FROM `" . $table_columns['table'] . "`";
                $sql = mysql_query($sql);
                $i = 2;
                if (mysql_num_rows($sql) > 0) {
                    while ($sqla = mysql_fetch_assoc($sql)) {
                        foreach ($table_columns['data']
                            as $colaplha => $colname
                        ) {
                            $result[$i][$colaplha]
                                = $sqla[str_replace('`', '', $colname)];
                        }
                        ++$i;
                    }
                }


                include_once '../Classes/PHPExcel/IOFactory.php';

                $objPHPExcel = new PHPExcel();

                $objPHPExcel->getProperties()->setTitle($filename);
                $objPHPExcel->getProperties()->setSubject($filename);
                $objPHPExcel->getProperties()->setDescription($filename);

                $objPHPExcel->setActiveSheetIndex(0);

                foreach ($result as $result_key => $result_value) {
                    foreach ($result_value as $column => $data) {
                        $objPHPExcel
                            ->getActiveSheet()
                            ->setCellValue(
                                $column . $result_key,
                                $data
                            );
                    }
                }
                $objPHPExcel->getActiveSheet()->setTitle($filename);


                $objWriter = PHPExcel_IOFactory::createWriter(
                    $objPHPExcel,
                    $fiel_save_type
                );
                $objWriter->save($directory . $filename . '.' . $file_type);

                unset($objWriter);
                unset($objPHPExcel);

                $files .= '<a href="'
                . $directory . $filename . '.' . $file_type . '" target="_blank">
                    ' . $filename . '.' . $file_type . '
                </a><br />';

            }

        }

        if (!empty($files)) {
            $success = true;
            $msg = '
            <p>
                ' . $_TEXT[42] . ':<br />
                ' . $files . '
            </p>';
        }

    }
}

require_once '../Classes/PHPExcel.php';
require_once '../getNameFromNumber.php';
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[41]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>

        <h2 class="subtitle"><?php echo $_TEXT[41]; ?></h2>

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-2 done">
                    <span>1. <?php echo $_TEXT[34]; ?></span>
                </h3>
                <h3 class="col-2 current">
                    <span>2. <?php echo $_TEXT[35]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

            <div class="msg">
<?php
if (isset($success) && $success == true) {
    echo '
    <div class="box-body" style="font-size: 24px; text-align: center;">
        ' . $msg . '<br />
        ' . $_TEXT[52] . '
    </div>';
}
?>
            </div>


            <form method="POST" action="export.php">
                <div class="zebra">
<?php
// database table columns
foreach ($_SESSION['exptables'] as $key => $value) {
    echo '<div class="form-row clear highlight">
        ' . $_TEXT[44] . ': `' . $value . '`
    </div>';
    echo '<div class="form-row clear">' . $_TEXT[45] . ':
        <input type="text" name="filename[' . $value . ']" value="" />
        <sup>1</sup>
    </div>';
    $columnssql = "DESCRIBE `" . $value . "`";
    $columnssql = mysql_query($columnssql);
    if ($columnssql != false && mysql_num_rows($columnssql) > 0) {
        while ($column = mysql_fetch_assoc($columnssql)) {
            echo '
                <div class="form-row clear">
                    <input type="text" name="table['
                    . $value . '][' . $column['Field']
                    . ']"  id="table_' . $value . '_' . $column['Field'] . '" maxlength="2" value="" />
                    <label for="table_' . $value . '_' . $column['Field'] . '">
                        ' . $column['Field'] . '
                    </label> <sup>2</sup>
                </div>
            ';
        }
    }

}

echo '
<div class="form-row clear highlight">
    ' . $_TEXT[55] . ':
    <select name="file_type">
        <option value="xlsx">XLSX</option>
        <option value="xls">XLS</option>
    </select> <sup>3</sup>
</div>
<div class="plain-text clear">
    <p>
        <sup>1</sup> ' . $_TEXT[47] . '
    </p>
    <p>
        <sup>2</sup> ' . $_TEXT[51] . '
    </p>
    <p>
        <sup>3</sup> ' . $_TEXT[56] . '
    </p>
</div>

<div class="form-row clear">
    <input type="hidden" name="token" value="' . $_SESSION['token'] . '" />
    <input type="hidden" name="export" value="save" />
    <input value="' . $_TEXT[50] . '" type="submit" />
</div>
';
?>
                    </div>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->

        <aside id="copy">
            <?php echo $_TEXT[40]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>

    </div> <!-- .page-inner -->

</body>
</html>
<?php
mysql_close();
?>