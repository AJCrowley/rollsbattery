<?php
    /**
     * MySQL to XLS
     *
     * Choose one or more table from database.
     * Line exceeds warning where long HTML tag has.
     *
     * PHP version 5.4.3
     *
     * @category  Export
     * @package   CSV_XLS_XML_MySQL
     * @author    Erdsoft <daniel.erdudac@erdsoft.com>
     * @copyright 2013 Erdsoft
     * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
     * @link      http://erdsoft.com/
     */

    require_once '../config.php';

    tokenGenerator();
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[41]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>

        <h2 class="subtitle"><?php echo $_TEXT[41]; ?></h2>

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-2 current">
                    <span>1. <?php echo $_TEXT[34]; ?></span>
                </h3>
                <h3 class="col-2">
                    <span>2. <?php echo $_TEXT[35]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

                <div class="msg error">
<?php
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
    case '1':       echo $_TEXT[36];
        break;
    case 'token':   echo $_TEXT[54];
        break;
    }
}
?>
                </div>

                <form method="POST" action="export.php">
                    <div class="zebra">
<?php
$tablessql = "SHOW TABLES FROM `" . $_CONFIG['db_name'] . "`";
$tablessql = mysql_query($tablessql);

if ($tablessql != false && mysql_num_rows($tablessql) > 0) {
    while ($table = mysql_fetch_row($tablessql)) {
        echo '
        <div class="form-row clear">
            <input type="checkbox" name="tables[]" id="table_' . $table[0]
            . '" value="' . $table[0] . '" />
            <label for="table_' . $table[0] . '">' . $table[0] . '</label>
        </div>';
    }

    echo '
    <div class="plain-text clear">
        <p>
            ' . $_TEXT[37] . '
        </p>
    </div>

    <div class="form-row clear">
        <input type="hidden" name="token" value="' . $_SESSION['token'] . '" />
        <input type="hidden" name="tablessave" value="save" />
        <input value="' . $_TEXT[38] . '" type="submit" />
    </div>';
} else {
    echo '<div class="msg error">' . $_TEXT[39] . '</div>';
}
?>
                    </div>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->

        <aside id="copy">
            <?php echo $_TEXT[40]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>

    </div> <!-- .page-inner -->

</body>
</html>
<?php
mysql_close();
?>