<?php
/**
 * MySQL to XML
 *
 * Pair columns end export to XML file.
 * Line exceeds warning where long HTML tag has.
 *
 * PHP version 5.4.3
 *
 * @category  Export
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

require_once '../config.php';


tokenChecker('index.php');

tokenGenerator();


if ((!isset($_POST['tables']))
    && (!isset($_SESSION['exptables']))
) {
    gotoheader('./index.php?msg=1');
}

if (!isset($_SESSION['exptables'])
    || (isset($_POST['tablessave'])
    && $_POST['tablessave'] == 'save')
) {
    if ($_POST['tables'] != '') {
        $_SESSION['exptables'] = $_POST['tables'];
    } else {
        gotoheader('./index.php?msg=1');
    }
}

$files = '';
if (isset($_POST['export']) && $_POST['export'] == 'save') {

    $directory = '../data/';
    $file_type = 'xml';

    include_once '../xml_generator.php';

    $filename = trim($_POST['filename']);
    if ($filename == '') {
        $filename = $_SESSION['exptables'];
    }

    $names = $_POST['name'];
    $level = $_POST['level'];
    $fields = $_POST['field'];

    $arr = createSubArray(array($names, $level, $fields));

    $data = getData($arr);

    $xml = '<?xml version="1.0" encoding="UTF-8"?>';
    $xml .= getXML($data);
    file_put_contents($directory . $filename . '.xml', $xml);

    $files .= '
    <a href="' . $directory . $filename . '.' . $file_type . '" target="_blank">
        ' . $filename . '.' . $file_type . '
    </a><br />';

}

if (!empty($files)) {
    $success = true;
    $msg = '
    <p>
        ' . $_TEXT[42] . ':<br />
        ' . $files . '
    </p>';
}

require_once '../Classes/PHPExcel.php';
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[61]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>

        <h2 class="subtitle"><?php echo $_TEXT[61]; ?></h2>

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-2 done">
                    <span>1. <?php echo $_TEXT[34]; ?></span>
                </h3>
                <h3 class="col-2 current">
                    <span>2. <?php echo $_TEXT[35]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

                <div class="msg">
<?php
if (isset($success) && $success == true) {
    echo '
    <div class="box-body" style="font-size: 24px; text-align: center;">
        ' . $msg . '<br />
        ' . $_TEXT[52] . '
    </div>';
}
?>
                </div>

                <div class="msg error">
<?php
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
    case '1':       echo $_TEXT[43];
        break;
    }
}
?>
                </div>

                <form method="POST" action="export.php">
                    <div class="zebra">

                        <div class="form-row clear"><?php echo $_TEXT[45]; ?>:
                            <input type="text" name="filename" value="" />
                            <sup>1</sup>
                        </div>
<?php
$fields = '<option value=""></option>';
$fields .= '<option value="" disabled="disabled">'
    . addslashes($_SESSION['exptables'])
    . '</option>';

$columnssql = "DESCRIBE `" . $_SESSION['exptables'] . "`";
$columnssql = mysql_query($columnssql);
if ($columnssql == false || mysql_num_rows($columnssql) <= 0) {
    $fields .= '<option value="">' . addslashes($_TEXT[63]) . '</option>';
    continue;
}

while ($column = mysql_fetch_assoc($columnssql)) {
    $fields .= '<option value="'
        . addslashes($_SESSION['exptables'] . '.' . $column['Field'])
        . '">' . addslashes(' &raquo; ' . $column['Field']) . '</option>';
}
$fields .= '<option value="" disabled="disabled"></option>';

echo '
<div class="zebra" id="option-tables">
</div>

<div class="plain-text clear">
    <p>
        <sup>1</sup> ' . $_TEXT[47] . '
    </p>
</div>

<div class="form-row clear">
    <input type="hidden" name="token" value="' . $_SESSION['token'] . '" />
    <input type="hidden" name="export" value="save" />
    <input value="' . $_TEXT[50] . '" type="submit" />
</div>
';
?>
                    </div>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->

        <aside id="copy">
            <?php echo $_TEXT[40]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>

    </div> <!-- .page-inner -->

<script src="../jquery-1.11.0.min.js"></script>
<script type="text/javascript">
    jQuery(function($) {
        var table_select = '<?php echo $fields; ?>';

        String.prototype.repeat = function(num) {
            return new Array(num + 1).join(this);
        }

        function field_generator(_this, level) {
            var div = $('<div class="form-row clear" data-level="' + level + '"><input type="hidden" name="level[]" value="' + level + '" /><input type="text" class="name" name="name[]" value="" /><select name="field[]">' + table_select + '</select><a href="#" class="add-to">Add sub</a></div>');
            div.appendTo($(_this));
        }

        $(document).on('keydown', 'input[type="text"].name', function() {
            if ($(this).parent('.form-row').data('level') == '1') {
                var parent = $(this).parent('.form-row');
                var level = ~~parseInt(parent.data('level'));
                level++;
                var create_new = true;
                var new_parent = parent.find('[data-level="' + level + '"] > .name');
                new_parent.each(function() {
                    if ($(this).val().length == 0) {
                        create_new = false;
                    }
                });

                if (create_new) {
                    field_generator(parent, level);
                }
            }
            else if ($(this).val().length > 0) {
                var parent = $(this).parent('.form-row');
                var level = ~~parseInt(parent.data('level'));
                var create_new = true;
                var new_parent = parent.parent('.form-row').find('[data-level="' + level + '"] > .name');
                new_parent.each(function() {
                    if ($(this).val().length == 0) {
                        create_new = false;
                    }
                });

                if (create_new) {
                    field_generator(parent.parent(), level);
                }
            }
        });

        field_generator($('#option-tables'), 1);

        $(document).on('click', '.add-to', function(e) {
            e.preventDefault();

            var parent = $(this).parent('.form-row');
            var level = ~~parseInt(parent.data('level'));
            level++;

            if (parent.children('.name').val().length == 0) {
                return true;
            }

            var create_new = true;
            parent.find('[data-level="' + level + '"]').each(function() {
                if ($(this).children('.name').val().length == 0) {
                    create_new = false;
                    return true;
                }
            });
            if (create_new === false) {
                return true;
            }


            field_generator(parent, level);
        });

    });
</script>

</body>
</html>

<?php
mysql_close();
?>