<?php
/**
 * XLS To MySQL
 *
 * Pair columns and import columns to database
 * Line exceeds warning where long HTML tag has.
 *
 * PHP version 5.4.3
 *
 * @category  Import
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

require_once '../config.php';


tokenChecker('choose_table.php');

tokenGenerator();



if ((!isset($_POST['table']))
    && (!isset($_SESSION['filename'])
        || !file_exists($_SESSION['filename'])
    )
) {
    gotoheader('./choose_table.php');
}

if (!file_exists($_SESSION['filename'])) {
    gotoheader('./index.php?msg=4');
}

require_once '../Classes/PHPExcel.php';
require_once '../getNameFromNumber.php';


$msg = '';
// save table name to session
if (!isset($_SESSION['table'])
    || (isset($_POST['table'])
    && $_POST['tables'] == 'save')
) {
    if (strlen($_POST['table']) > 0) {
        $_SESSION['table'] = $_POST['table'];
    } else {
        gotoheader('./choose_table.php?msg=1');
    }
}


$inputFileName = $_SESSION['filename'];
$inputFileType = PHPExcel_IOFactory::identify($inputFileName);


// save file to table
if (isset($_POST['import']) && $_POST['import'] == 'save') {
    if (isset($_SESSION['filename'])
        && isset($_SESSION['table'])
        && isset($_POST['column'])
    ) {

        $data = array();
        try {
            $arr = array();
            foreach ($_POST['column'] as $key => $value) {
                // load file
                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
                $worksheet = $objPHPExcel->getSheet(0);
                $lastRow = $worksheet->getHighestRow();
                $lastColumn = PHPExcel_Cell::columnIndexFromString(
                    $worksheet->getHighestColumn()
                );

                // create columns list
                $columns = array();
                foreach ($_POST['column'] as $key => $value) {
                    if ($value != '') {
                        $columns[$key] = $value;
                    }
                }
            }

            // create data list
            if (count($columns) > 0) {
                // skip first row if exist header row
                $r = ($_SESSION['has_header'] ? 2 : 1);
                for ( ; $r <= $lastRow; $r++) {
                    $data[$r] = array();
                    foreach ($columns as $key => $value) {
                        if (isset($_POST['compare'][$key])) {
                            $data[$r]['__compare__'][$key] =
                                $worksheet
                                    ->getCell($value . $r)
                                    ->getCalculatedValue();
                        } else {
                            $data[$r][$key] =
                                $worksheet
                                    ->getCell($value . $r)
                                    ->getCalculatedValue();
                        }
                    }
                }
            } else {
                $msg = $_TEXT[31];
            }
        }
        catch(Exception $e) {
            $msg = $_TEXT[15];
        }

        foreach ($data as $value) {
            if (count($value) == 0
                || (isset($value['__compare__'])
                && count($value) < 2)
            ) {
                $msg = $_TEXT[53];
                $data = array();
                break;
            }
        }

        if ($msg == '') {
            include_once '../saveToDB.php';
            if (saveToDB($data, $_SESSION['table']) == true) {
                gotoheader('../success.php');
            } else {
                $msg = $_TEXT[14];
            }
        }
    }
}
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[25]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>


        <h2 class="subtitle"><?php echo $_TEXT[25]; ?></h2>

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-3 done">
                    <span>1. <?php echo $_TEXT[26]; ?></span>
                </h3>
                <h3 class="col-3 done">
                    <span>2. <?php echo $_TEXT[2]; ?></span>
                </h3>
                <h3 class="col-3 current">
                    <span>3. <?php echo $_TEXT[3]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

                <div class="msg error"><? echo $msg; ?></div>

                <form method="POST" action="import.php">
<?php
// database table columns
$columns = array();
$columnssql = "DESCRIBE `" . $_SESSION['table'] . "`";
$columnssql = mysql_query($columnssql);
if ($columnssql != false && mysql_num_rows($columnssql) > 0) {
    while ($column = mysql_fetch_assoc($columnssql)) {
        $columns[] = $column['Field'];
    }
}

// excel file's columns
$excel_columns = '<option value=""></option>';
try {
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
    $worksheet = $objPHPExcel->getSheet(0);
    $lastColumn = PHPExcel_Cell::columnIndexFromString(
        $worksheet->getHighestColumn()
    );
    for ($i = 0; $i < $lastColumn; $i++) {
        $alpha = getNameFromNumber($i);
        $excel_columns .= '
            <option value="' . $alpha . '">' .
            ($_SESSION['has_header'] ?
                $worksheet->getCellByColumnAndRow($i, 1)->getValue() :
                $alpha)
            . '</option>
        ';
    }
}
catch (Exception $e) {
    echo '<div class="msg error">' . $_TEXT[15] . '</div>';
}

// draw
echo '<table cellpadding="0" cellspacing="0" border="0">';
if (count($columns) > 0 && !empty($excel_columns)) {
    echo '
    <thead>
        <tr>
            <th>' . $_TEXT[16] . '</th>
            <th>' . $_TEXT[32] . '</th>
        </tr>
    </thead>
    </body>
    ';
    foreach ($columns as $value) {
        echo '
        <tr>
            <td><label for="column_' . $value . '">' . $value . '</label></td>
            <td>
                <select name="column[' . $value . ']" id="column_' . $value . '">
                ' . $excel_columns . '
                </select>
                <input type="checkbox" name="compare[' . $value . ']" value="on" id="compare[' . $value . ']" />
                <label for="compare[' . $value . ']">'
                . $_TEXT[18] .
                '</label>
            </td>
        </tr>';
    }
    echo '
    </tbody>
    </table>

    <div class="plain-text clear">
        <p>
            <sup>1</sup> ' . $_TEXT[19] . '
        </p>
    </div>

    <div class="form-row clear">
        <input type="hidden" name="token" value="' . $_SESSION['token'] . '" />
        <input type="hidden" name="import" value="save" />
        <input value="' . $_TEXT[21] . '" type="submit" />
    </div>';
} else {
    echo '<div class="msg error">' . $_TEXT[20] . '</div>';
}

?>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->

        <aside id="copy">
            <?php echo $_TEXT[30]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>

    </div> <!-- .page-inner -->

</body>
</html>
<?php
mysql_close();
?>