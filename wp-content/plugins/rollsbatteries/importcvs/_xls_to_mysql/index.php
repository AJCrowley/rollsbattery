<?php
    /**
     * XLS To MySQL
     *
     * Index file of XLS To MySQL application. Can choose XLS files and
     * delimiter
     * Line exceeds warning where long HTML tag has.
     *
     * PHP version 5.4.3
     *
     * @category  Import
     * @package   CSV_XLS_XML_MySQL
     * @author    Erdsoft <daniel.erdudac@erdsoft.com>
     * @copyright 2013 Erdsoft
     * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
     * @link      http://erdsoft.com/
     */

    require_once '../config.php';

    tokenGenerator();
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title><?php echo $_TEXT[25]; ?></title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="../images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="../css/main.css" />
    <link type="text/css" rel="stylesheet" href="../css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="../" id="logo">CSV, XLS, XML to MSQL</a>
        <h2 class="subtitle"><?php echo $_TEXT[25]; ?></h2>

        <section class="box clear">
            <header class="box-header clear">

                <h3 class="col-3 current">
                    <span>1. <?php echo $_TEXT[26]; ?></span>
                </h3>
                <h3 class="col-3">
                    <span>2. <?php echo $_TEXT[2]; ?></span>
                </h3>
                <h3 class="col-3">
                    <span>3. <?php echo $_TEXT[3]; ?></span>
                </h3>

            </header> <!-- .box-header -->

            <article class="box-body clear">

                <div class="msg error">

<?php
if (isset($_GET['msg'])) {
    switch ($_GET['msg']) {
    case '1':       echo $_TEXT[4];
        break;
    case '2':       echo $_TEXT[27];
        break;
    case '3':       echo $_TEXT[6];
        break;
    case '4':       echo $_TEXT[28];
        break;
    case '5':       echo $_TEXT[58];
        break;
    case '6':       echo $_TEXT[59];
        break;
    case 'token':   echo $_TEXT[54];
        break;
    }
}
?>
                </div>

                <form method="POST" enctype="multipart/form-data" action="choose_table.php">
                    <div class="plain-text clear">
                        <p>
                            <sup>**</sup> <?php echo $_TEXT[60]; ?>
                        </p>
                    </div>
                    <div class="form-row clear">
                        <label for="filename"><?php echo $_TEXT[26]; ?></label>
                        <input type="file" id="filename" name="filename" value="" />
                     </div>
                    <div class="form-row clear">
                        <label>- OR -</label>
                    </div>
                    <div class="form-row clear">
                        <label for="url">
                            <?php echo $_TEXT[57]; ?>
                        </label>
                        <input type="text" id="url" name="url" value="" />
                    </div>
                     <div class="form-row clear">
                        <label for="has_header">
                            <?php echo $_TEXT[10]; ?> <sup>1</sup>
                        </label>
                        <input type="checkbox" id="has_header" name="has_header" value="on" />
                    </div>
                    <div class="plain-text clear">
                        <p>
                            <sup>1</sup> <?php echo $_TEXT[12]; ?>
                        </p>
                    </div>
                     <div class="form-row clear">
                        <input type="hidden" name="token" value="<?php echo $_SESSION['token']; ?>" />
                        <input type="hidden" name="file_type" value="xls" />
                        <input value="<?php echo $_TEXT[22]; ?>" type="submit" />
                    </div>
                </form>
            </article> <!-- .box-body -->
        </section> <!-- .box -->

        <aside id="copy">
            <?php echo $_TEXT[30]; ?>
            <img src="../images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>

    </div> <!-- .page-inner -->

</body>
</html>
<?php
mysql_close();
?>