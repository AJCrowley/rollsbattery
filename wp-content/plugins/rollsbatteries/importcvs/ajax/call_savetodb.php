<?php
include_once '../config.php';

if (isset($_POST['data']) && $_POST['data'] != ''
    && isset($_POST['table']) && $_POST['table'] != ''
) {

    include_once '../saveToDB.php';

    $data = json_decode($_POST['data']);

    if (saveToDB($data, $_POST['table']) === true) {
        echo 'ok';
    } else {
        echo 'not ok';
    }

    mysql_close();

}

?>