<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Config file.
 *
 * PHP version 5.4.3
 *
 * @category  Config
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

// starting session
session_start();

error_reporting(E_ALL ^ E_DEPRECATED ^ E_NOTICE);
ini_set('display_errors', 1);

ini_set('max_execution_time', 12000);

setlocale(LC_ALL, 'en_EN.UTF-8');

// CONFIG
$_CONFIG = array();
$_CONFIG['db_host'] = 'localhost';
$_CONFIG['db_name'] = 'rolls152_rollswp2015';
$_CONFIG['db_user'] = 'rolls152_root1';
$_CONFIG['db_pass'] = '0u73kTe}[Ogs';

$_CONFIG['tokenTimer'] = 10;

$connection = mysql_connect("$_CONFIG[db_host]", "$_CONFIG[db_user]", "$_CONFIG[db_pass]") or die(mysql_error());
$ide = mysql_select_db("$_CONFIG[db_name]", $connection) or die(mysql_error());

// set charset
$sql = "SET NAMES 'utf8'";
mysql_query($sql);


// set timezone
date_default_timezone_set('Europe/Belgrade');

$sql = "SET time_zone = '+01:00'";
mysql_query($sql);


require_once '../en.php';

require_once '../validate.php';

require_once '../gotoheader.php';

require_once '../tokens.php';


?>
