<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Language file
 *
 * PHP version 5.4.3
 *
 * @category  Language
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

$_TEXT = array();


$_TEXT[0]   = "CSV to MySQL";
$_TEXT[1]   = "Upload CSV";
$_TEXT[2]   = "Select table";
$_TEXT[3]   = "Pair columns";
$_TEXT[4]   = "Delimiter can't be empty!";
$_TEXT[5]   = "Please, choose a CSV file!";
$_TEXT[6]   = "Permission problem!";
$_TEXT[7]   = "Please, choose a CSV file!";
$_TEXT[8]   = "CSV File";
$_TEXT[9]   = "Delimiter";
$_TEXT[10]  = "Has header";
$_TEXT[11]  = 'The delimiter separates your data in CSV file. You can check '
    . 'your default delimiter in "Control Panel" > "Clock, Language, and '
    . 'Region" > "Region" > "Additional settings". Look for "List separator" '
    . 'field.';
$_TEXT[12]  = "Check if the first row in the table is the header. If checked, "
    . "the application will skip the first row.";
$_TEXT[13]  = "Choose a CSV column!";
$_TEXT[14]  = "Incorrect data!";
$_TEXT[15]  = "Error loading file!";
$_TEXT[16]  = "Database columns";
$_TEXT[17]  = "CSV columns";
$_TEXT[18]  = "Key";
$_TEXT[19]  = "If this is checked, the application will use this for "
    . "comparison (not save). The existing data will be updated.";
$_TEXT[20]  = "Please, choose another file or table!";
$_TEXT[21]  = "Import";
$_TEXT[22]  = "Next";
$_TEXT[23]  = "Please, select a table!";
$_TEXT[24]  = "Please, choose a not empty database!";
$_TEXT[25]  = "XLS(X) to MySQL";
$_TEXT[26]  = "Upload XLS(X)";
$_TEXT[27]  = "Please, choose an XLS(X) file!";
$_TEXT[28]  = "Please, choose an XLS(X) file!";
$_TEXT[29]  = "Excel File";
$_TEXT[30]  = "Code by ";
$_TEXT[31]  = "Choose an XLS(X) column!";
$_TEXT[32]  = "XLS(X) columns";

$_TEXT[33]  = "MySQL to CSV";
$_TEXT[34]  = "Select table";
$_TEXT[35]  = "Export tables";
$_TEXT[36]  = "Please, select a table!";
$_TEXT[37]  = "Here you can choose tables you would like to export to a file. "
    . "You can choose more than one. For each table a separate file will be "
    . "created.";
$_TEXT[38]  = "Next";
$_TEXT[39]  = "Please, choose a not empty database!";
$_TEXT[40]  = "Code by ";
$_TEXT[41]  = "MySQL to XLS(X)";
$_TEXT[42]  = "Download";
$_TEXT[43]  = "Delimiter can't be empty!";
$_TEXT[44]  = "Table name";
$_TEXT[45]  = "File name (without extension)";
$_TEXT[46]  = "Delimiter";
$_TEXT[47]  = "Desired file name. If you leave empty, the table‘s name will be "
    . "used.";
$_TEXT[48]  = "Add CSV file's column name. Example: A, B, C, AA, CB, DG";
$_TEXT[49]  = 'Delimiter separates your data in CSV file. You can check your '
    . 'default delimiter in "Control Panel" > "Clock, Language, and Region" > '
    . '"Region" > "Additional settings". Look for "List separator" field.';
$_TEXT[50]  = "Export";
$_TEXT[51]  = "Add XLS(X) file's column name. Example: A, B, C, AA, CB, DG";
$_TEXT[52]  = "Process successfully completed!";
$_TEXT[53]  = "Select data for save!";
$_TEXT[54]  = "Invalid token!";
$_TEXT[55]  = "File type";
$_TEXT[56]  = "Choose file type.";
$_TEXT[57]  = "External source";
$_TEXT[58]  = "Invalid URL!";
$_TEXT[59]  = "Couldn't upload file from URL!";
$_TEXT[60]  = "The server is restricted, so 10.000 row can be parset at a time. This is a server settings parameter.";

$_TEXT[61]  = "MySQL to XML";
$_TEXT[62]  = "XML to MySQL";
$_TEXT[63]  = "Empty table!";
$_TEXT[64]  = "Upload XML";
$_TEXT[65]  = "Please, choose an XML file!";
$_TEXT[66]  = "Choose an XML column!";
$_TEXT[67]  = "XML columns";

?>
