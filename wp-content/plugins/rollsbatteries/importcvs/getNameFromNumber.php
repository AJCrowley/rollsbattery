<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Convert numbers to Excel's columns.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Convert numbers to Excel's column names
 *
 * @param int $num columns number
 *
 * @return bool  return a string
 */
function getNameFromNumber($num)
{
    $index = 0;
    $index = abs($index * 1);
    $numeric = ($num - $index) % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval(($num - $index) / 26);
    if ($num2 > 0) {
        return getNameFromNumber(
            $num2 - 1 + $index
        ) . $letter;
    } else {
        return $letter;
    }
}

?>