<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Header rerouting.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Redirect to URL.
 *
 * @param string $file link
 *
 * @return null
 */

function gotoheader($file)
{
    header("Location: $file");
    exit;
}

?>