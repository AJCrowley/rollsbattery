<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Multi converter</title>
    <meta name="description" content="" />
    <meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="./css/main.css" />
    <link type="text/css" rel="stylesheet" href="./css/developer.css" />



</head>

<body class="no-js">

    <div class="page-inner clear">

        <a href="./" id="logo">CSV, XLS, XML to MSQL</a>


        <section class="option-list">

            <ul>
                <li>
                    <a href="./_csv_to_mysql/" target="_self">CSV <em>to</em> MYSQL</a>
                </li>
                <li>
                    <a href="./_mysql_to_csv/" target="_self">MYSQL <em>to</em> CSV</a>
                </li>
                <li>
                    <a href="./_xls_to_mysql/" target="_self">XLS(X) <em>to</em> MYSQL</a>
                </li>
                <li>
                    <a href="./_mysql_to_xls/" target="_self">MYSQL <em>to</em> XLS(X)</a>
                </li>
                <li>
                    <a href="./_xml_to_mysql/" target="_self">XML <em>to</em> MYSQL</a>
                </li>
                <li>
                    <a href="./_mysql_to_xml/" target="_self">MYSQL <em>to</em> XML</a>
                </li>
            </ul>

        </section>

        <aside id="copy">Code by <img src="./images/logo_erdsoft_32_27.png" alt="Erdsoft" /></aside>

    </div> <!-- .page-inner -->

</body>
</html>
