<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * SQL Injection, XSS, CSRF
 *
 * Strip dangerous characters.
 *
 * PHP version 5.4.3
 *
 * @category  Security
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Replace special characters.
 *
 * @param string $value an array with data from file
 *
 * @return string return a boolean variable, true if save is success
 */
function replaceData($value = '')
{

    return (
        mysql_real_escape_string(
        	$value
	    )
    );

}

?>