<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Save data array values to table.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Saveing data to table.
 *
 * @param array  $data  an array with data from file
 * @param string $table choosed database table name
 *
 * @return bool  return a boolean variable, true if save is success
 */
function saveToDB($data = array(), $table = null)
{
    if (count($data) == 0 || $table == null) {
        return false;
    }
    $table = mysql_real_escape_string($table);

    include_once '../replace_data.php';

    $insert = array();
    foreach ($data as $key => $value) {

        $values = $filter = array();
        foreach ($value as $col => $row) {
            $save_col = $save_row = '';

            if ($col == '__compare__') {
                foreach ($row as $cmp_col => $cmp_row) {
                    $save_col = replaceData($cmp_col);
                    $save_row = replaceData($cmp_row);

                    if (empty($save_col)) {
                        continue;
                    }
                    $filter[] = "`$save_col` = '$save_row'";
                }
                continue;
            }


            $save_col = replaceData($col);
            $save_row = replaceData($row);

            if (empty($save_col)) {
                continue;
            }
            $values[] = "`$save_col` = '$save_row'";
        }


        $temp_filter = $filter;
        if (count($filter) > 0) {
            $filter = " WHERE " . implode(' AND ', $filter);
        } else {
            $filter = '';
        }


        if (!empty($filter)) {
            $rows_count = mysql_num_rows(
                mysql_query(
                    "SELECT TRUE FROM `{$table}` {$filter} LIMIT 1"
                )
            );
            if ($rows_count > 0) {

                // update
                $sql = mysql_query(
                    "UPDATE `{$table}`
                    SET " . implode(',', $values) . "
                    " . $filter . "
                    LIMIT 1"
                );
                if (! $sql) {
                    return false;
                }

                continue;

            }
        }

        $insert[] = implode(',', array_merge($temp_filter, $values));

    }

    // insert
    if (count($insert) > 0) {
        foreach ($insert as $value) {
            if ( ! mysql_query("INSERT INTO `{$table}` SET " . $value . "")) {
                return false;
            }
        }
    }
    return true;
}

?>