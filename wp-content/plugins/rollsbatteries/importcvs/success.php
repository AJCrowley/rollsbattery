<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Success message page.
 *
 * PHP version 5.4.3
 *
 * @category  General
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */

require_once './en.php';
?>

<!DOCTYPE HTML>
<!--[if lt IE 7 ]> <html lang="en" class="ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- force latest IE rendering engine & Chrome Frame -->
 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Multi converter</title>
    <meta name="description" content="" />
  	<meta name="author" content="" />

    <!-- Mobile viewport optimized: j.mp/bplateviewport -->
  	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- favicon -->
    <link rel="icon" type="image/png" href="images/favicon.png" />

    <link type="text/css" rel="stylesheet" href="./css/main.css" />
    <link type="text/css" rel="stylesheet" href="./css/developer.css" />



</head>

<body class="no-js">

	<div class="page-inner clear">
<!--
    	<a href="./" id="logo">CSV, XLS to MSQL</a>
-->
        <article class="box clear">

        	<div class="box-body" style="font-size: 24px; text-align: center;">
            <?php echo $_TEXT[52]; ?>
            </div>

        </article> <!-- .box -->
<!--
        <aside id="copy">
            Code by
            <img src="./images/logo_erdsoft_32_27.png" alt="Erdsoft" />
        </aside>
-->

    </div> <!-- .page-inner -->

</body>
</html>

