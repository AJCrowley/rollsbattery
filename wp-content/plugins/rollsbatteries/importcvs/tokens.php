<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Check the token.
 *
 * PHP version 5.4.3
 *
 * @category  Security
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Tocken checker and validator.
 * Check token time.
 *
 * @param string $url url for rerouting
 *
 * @return null
 */
function tokenChecker($url = '')
{
    // token check
    if (!isset($_SESSION['token']) || !isset($_SESSION['token_time'])) {
        gotoheader('./' . $url . '?msg=token');
    }
    if (!isset($_POST['token']) || $_POST['token'] != $_SESSION['token']) {
        gotoheader('./' . $url . '?msg=token');
    }

    global $_CONFIG;
    $token_age = time() - $_SESSION['token_time'];
    // ten minute
    if ($token_age > ($_CONFIG['tokenTimer'] * 60)) {
        gotoheader('./' . $url . '?msg=token');
    }
}


/**
 * Token generator
 *
 * @return null
 */
function tokenGenerator()
{
    $token = md5(uniqid(rand(), true));
    $_SESSION['token'] = $token;
    $_SESSION['token_time'] = time();
}


?>