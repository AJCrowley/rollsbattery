<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Header rerouting.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */


/**
 * Check if value is a valid url
 *
 * @param string $value
 *
 * @return boolean
 */

function VALIDATE_URL($value = '')
{
    return preg_match(
        '/(http|https|ftp|ftps)\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/',
        $value
    ) ? TRUE : FALSE;
}

?>