<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Working with XML files.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */



/**
 * Get XML file string. This create new XML file.
 *
 * @param array  $arr        data
 * @param string $parent_key previous element key
 *
 * @return string return XML data
 */
function getXML($arr = array(), $parent_key = '') {

    $xml = '';
    foreach ($arr as $key => $val) {
        if (is_array($val)) {
            $xml_data = getXML($val, $key);
            if (is_numeric($key)) {
                $xml .= "<" . $parent_key . ">"
                    . $xml_data
                    . "</" . $parent_key . ">\n";
            }
            else {
                $filter = array_filter(array_keys($val), 'is_numeric');
                if ((count($filter) - count(array_keys($val))) == 0) {
                    $xml .= $xml_data . "\n";
                }
                else {
                    $xml .= "<" . $key . ">" . $xml_data . "</" . $key . ">\n";
                }
            }
        }
        else {
            if (!is_numeric($val) || $val == '') {
                $val = "<![CDATA[" . $val . "]]>";
            }
            $xml .= "<" . $key . ">" . $val . "</" . $key . ">\n";
        }
    }

    return $xml;

}



/**
 * Load data from tables.
 *
 * @param  array $arr table list
 *
 * @return array data from table
 */
function getData($arr = array()) {

    $data_arr = array();
    foreach ($arr as $key => $val) {
        if ($val == '') {
            continue;
        }

        if (is_array($val)) {
            $data_arr[$key] = getData($val);
        }
        else {
            $val = explode('.', $val);
            $table_name = $val[0];
            $column_name = $val[1];

            $sql = "
                SELECT `" . mysql_real_escape_string($column_name) . "`
                FROM `" . mysql_real_escape_string($table_name) . "`
            ";
            $sql = mysql_query($sql);
            $i = 0;
            while ($value = mysql_fetch_assoc($sql)) {
                $data_arr[$i][$key] = $value[$column_name];
                $i++;
            }
            unset($sql);
        }
    }

    return $data_arr;

}



/**
 * Create file list with fields.
 *
 * @param  array $arr field list
 * @param  int   $level level of field
 *
 * @return array return fields array
 */
function createSubArray($arr = array(), $level = 1) {

    $tempate_arr = array();
    foreach ($arr[1] as $key => $val) {

        unset($arr[1][$key]);

        if ($arr[0][$key] == '') {
            continue;
        }

        if ($level != $val) {
            if ($level > $val) {
                break;
            }
            continue;
        }

        $tempate_arr[$arr[0][$key]] = $arr[2][$key];
        $get_arr = createSubArray($arr, $level + 1);

        if (!empty($get_arr)) {
            $tempate_arr[$arr[0][$key]] = $get_arr;
        }

    }

    return $tempate_arr;

}

?>