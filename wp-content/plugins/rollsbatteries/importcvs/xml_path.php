<?php
/**
 * CSV, XLS, XML To MySQL
 *
 * Get XML path and data.
 *
 * PHP version 5.4.3
 *
 * @category  Database
 * @package   CSV_XLS_XML_MySQL
 * @author    Erdsoft <daniel.erdudac@erdsoft.com>
 * @copyright 2013 Erdsoft
 * @license   http://www.php.net/license/3_01.txt  PHP License 3.01
 * @link      http://erdsoft.com/
 */



/**
 * Get XML paths from array.
 *
 * @param array  $xml  XML data
 * @param string $path previous path
 *
 * @return array return an array
 */
function XML_PATH($xml = array(), $path = '') {
    $data = array();

    foreach ($xml as $key => $val) {
        $path_temp = $path . $key;
        if ((is_object($val) && count($val) > 0)) {
            $data = array_merge($data, XML_PATH($val, $path_temp . ' / '));
            continue;
        }

        if (isset($data[$path_temp])) {
            continue;
        }

        $data[$path_temp] = $path_temp;
    }

    return $data;
}



/**
 * Get XML data from array.
 *
 * @param array  $xml     XML data
 * @param array  $columns enabled column list
 * @param string $path    previous path
 *
 * @return bool  return a string
 */
function XML_DATA($xml = array(), $columns = array(), $path = '') {
    $data = array();

    $xml = json_decode(json_encode($xml), true);

    foreach ($xml as $key => $val) {

        if (!is_numeric($key) && !in_array($key, $columns)) {
            continue;
        }

        $path_temp = $path . $key;
        if ((is_array($val) && count($val) > 0)) {
            $data = array_merge($data, XML_DATA($val, $columns, $path_temp . ' / '));
            continue;
        }

        if (count($val) == 0) {
            $val = '';
        }

        $data[$path][$key] = $val;
    }

    return $data;
}

?>