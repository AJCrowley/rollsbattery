jQuery(document).ready(function(){

// Range Slider
$desiredamphr = jQuery("#DesiredAmpHrCapacity");
$voltageselect = jQuery("#VoltageSelect");
$stringselect = jQuery("#StringSelect");
//$chargeroutput = jQuery("#spinEdit");
$chargeroutput = jQuery("#spinner");


$desiredamphr.ionRangeSlider({
                type: "double",
                //grid: true,
                min: 0,
                max: 10000,
                step: 50,
                from: 500,
                to: 3500,

});

$desiredamphr.on("change", function() {
        selectorfilter();
});

$voltageselect.change(function() {
        selectorfilter();
});

$stringselect.change(function() {
        selectorfilter();
});


jQuery("#spinner").spinner({
	min: 5,
	step: 5
}).val(5);

jQuery("#spinner").after('<div style="display:inline-block;position:absolute;left:50px;top:10px;"><strong>A</strong></div> ');

jQuery('.ui-spinner-button').click(function() {
	jQuery(this).siblings('input').keyup();
});

$chargeroutput.keyup(function(){
        selectorfilter('chargeroutput');
});



function selectorfilter(action){

        var thisstring = $stringselect.val();
	var batterytype = jQuery('#selectbatterytype').val();
	if (batterytype == "")
		batterytype= 'marine';

        //Desired Amp Hr
        minandmax = $desiredamphr.prop("value").split(";");
    /*    minAmpHr = minandmax[0] / thisstring;
        maxAmpHr = minandmax[1] / thisstring; */
        minAmpHr = minandmax[0];
        maxAmpHr = minandmax[1];

	var $totalcapacity_html = jQuery('div#total_ah_capacity');
	var rate_20_hr = jQuery('#selected-battery').data('20_hr_rate');
	var totalcapacity = rate_20_hr * thisstring;

	totalcapacity_st = '<strong>TOTAL AH CAPACITY: ' + totalcapacity + ' AH</strong>';
	$totalcapacity_html.html(totalcapacity_st);

	var thischargeroutput = $chargeroutput.val();
	var aborptioncharge = (totalcapacity / thischargeroutput * 0.42).toFixed(2);
	chargeroutput_st = '<strong>' + aborptioncharge + ' Hours</strong>';
	jQuery("#charge_time").html(chargeroutput_st);


	// Charger Output

	var totalcapacity = rate_20_hr * thisstring;

	var this_5_charge = Math.round(totalcapacity * .05);
	var this_10_charge = Math.round(totalcapacity * .1);

	var numberofamps = this_5_charge / 5;
	var reminderofamps = this_5_charge % 5;

	if (reminderofamps > 0) {
		numberofamps = Math.floor(numberofamps);
		numberofamps = numberofamps + 1;
	}

	numberofamps = Math.floor(numberofamps);
	numberofamps = numberofamps * 5 ;
	if (action != 'chargeroutput') {
		$chargeroutput.val(numberofamps);
	}

	var minimum_chargeroutput = Math.round(totalcapacity *  0.05);
	var recommended_chargeroutput = Math.round(totalcapacity * 0.1);
	jQuery('#minimum_chargeroutput').html(minimum_chargeroutput);
	jQuery('#recommended_chargeroutput').html(recommended_chargeroutput);

        //Voltage

	var system_1_string_v12 = jQuery('#selected-battery').data('system_1_string_12_v');
	var system_2_string_v12 = jQuery('#selected-battery').data('system_2_string_12_v');
	var system_3_string_v12 = jQuery('#selected-battery').data('system_3_string_12_v');
	var system_1_string_v24 = jQuery('#selected-battery').data('system_1_string_24_v');
	var system_2_string_v24 = jQuery('#selected-battery').data('system_2_string_24_v');
	var system_3_string_v24 = jQuery('#selected-battery').data('system_3_string_24_v');
	var system_1_string_v48 = jQuery('#selected-battery').data('system_1_string_48_v');
	var system_2_string_v48 = jQuery('#selected-battery').data('system_2_string_48_v');
	var system_3_string_v48 = jQuery('#selected-battery').data('system_3_string_48_v');

        var thisvoltage = $voltageselect.val();

        switch (thisvoltage) {
        	case "12V":
			switch (thisstring) {
				case '1':
					jQuery("#Quantity").val(system_1_string_v12);
					break;
				case '2':
					jQuery("#Quantity").val(system_2_string_v12);
					break;
				case '3':
					jQuery("#Quantity").val(system_3_string_v12);
					break;
			}
			break;
        	case "24V":
			switch (thisstring) {
				case '1':
					jQuery("#Quantity").val(system_1_string_v24);
					break;
				case '2':
					jQuery("#Quantity").val(system_2_string_v24);
					break;
				case '3':
					jQuery("#Quantity").val(system_3_string_v24);
					break;
			}
			break;
        	case "48V":
			switch (thisstring) {
				case '1':
					jQuery("#Quantity").val(system_1_string_v48);
					break;
				case '2':
					jQuery("#Quantity").val(system_2_string_v48);
					break;
				case '3':
					jQuery("#Quantity").val(system_3_string_v48);
					break;
			}
			break;
      	}

        voltage = $voltageselect.val();
        var value = [];
        jQuery('.batterydata').filter(function(index){
                var $this = jQuery(this);
                var matcharrbatteryname = $this.data('battery_name');

                var matcharr20hr = $this.data('20_hr_rate');

                var hr5match = false;
                var hr8match = false;
                var hr20match = false;
                var hr100match = false;

                var matcharrvolt_12v = $this.data('system_voltage_12V');
                var matcharrvolt_24v = $this.data('system_voltage_24V');
                var matcharrvolt_48v = $this.data('system_voltage_48V');
                var matcharrvolt_32v = $this.data('system_voltage_32V');

                //console.log(matcharrvolt_48v);
                //console.log(voltage);
                var usethisvoltage = '';
                var thisvoltage = '';

                switch (voltage) {
                        case '12V':
                                if (!matcharrvolt_12v) {
                                        usethisvoltage = '.12V';
                                }
				thisvoltage = 12;
                                break;
                        case '24V':
                                if (!matcharrvolt_24v) {
                                        usethisvoltage = '.24V';
                                }
				thisvoltage = 24;
                                break;
                        case '48V':
                                if (!matcharrvolt_48v) {
                                        usethisvoltage = '.48V';
                                }
				thisvoltage = 48;
                                break;
                        case '32V':
                                if (!matcharrvolt_32v) {
                                        usethisvoltage = '.32V';
                                }
				thisvoltage = 32;
                                break;
                }

                var thisbatteryvoltage = $this.data('voltage');

                if (matcharr20hr) {
                        var amphr20 = parseInt(matcharr20hr);
                        var amphr20x2 = amphr20 * 2;
                        var amphr20x3 = amphr20 * 3;
                        if ((amphr20 >= minAmpHr) && (amphr20 <= maxAmpHr)) {
				if (thisvoltage % thisbatteryvoltage == 0) {
                                	hr20match = true;
				}
                        }
                        if ((amphr20x2 >= minAmpHr) && (amphr20x2 <= maxAmpHr)) {
				if (thisvoltage % thisbatteryvoltage == 0) {
                                	hr20match = true;
				}
                        }
                        if ((amphr20x3 >= minAmpHr) && (amphr20x3 <= maxAmpHr)) {
				if (thisvoltage % thisbatteryvoltage == 0) {
                                	hr20match = true;
				}
                        }
                }

                if (hr20match == true ) {
                        value.push('.' + batterytype + '.' + matcharrbatteryname + usethisvoltage);
                        //value.push('.' + batterytype + '.' + matcharrbatteryname);
                } else {
                }
        });

        var batteries = value.join(",");
        $batteries_container.isotope({ filter: (batteries == null || batteries.length == 0) ? 'default' : batteries});

}


//Selector section
jQuery('#selector_box').on('click', function(){

        selector_change_all('selector_box');

	var edge_selectbox = jQuery('#thisselector_box');
	var this4thstep = jQuery('#step4selector');
	var offset = edge_selectbox.offset();
	var top = offset.top;
	var bottom = top + edge_selectbox.outerHeight() + 'px';

	//this4thistep.css('top', bottom );



});

jQuery('#selectmarine').on('click', function(){

	jQuery('#selectbatterytype').val('marine');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>48V</option> ');
        selectorfilter();



});

jQuery('#selectagm').on('click', function(){

	jQuery('#selectbatterytype').val('agm');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>48V</option> ');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/ACP.pdf" class="fancybox">AGM Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/ACP.pdf" class="fancybox">AGM Charging Parameters</a>');
        selectorfilter();

});

jQuery('#selectmotivepower').on('click', function(){

	jQuery('#selectbatterytype').val('motive_power');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>48V</option> ');
        selectorfilter();

});

jQuery('#selectre').on('click', function(){

	jQuery('#selectbatterytype').val('renewable_energy');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>48V</option> ');
        selectorfilter();
});
jQuery('#selectrailroad').on('click', function(){

	jQuery('#selectbatterytype').val('railroad');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging Parameters</a>');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>32V</option> <option>48V</option> ');
        selectorfilter();

});

jQuery('#selectgel').on('click', function(){

	jQuery('#selectbatterytype').val('gel');
	jQuery('#parameter_pdf').html('<a href="/uploads/pdfs/GCP.pdf" class="fancybox">Gel Charging Parameters</a>');
	jQuery('#parameter_pdf2').html('<a href="/uploads/pdfs/GCP.pdf" class="fancybox">Gel Charging Parameters</a>');
	$voltageselect.empty().append(' <option>12V</option> <option>24V</option> <option>48V</option> ');
        selectorfilter();

});

var searchMatches = [];

function updateSearchResults() {
	term = jQuery('#searchBox').val().toLowerCase();
    var choices = searchData;
    var matches = [];
    for (i=0; i<choices.length; i++)
        if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i].replace(/ /g,""));
	var selector;
    if(matches.length > 0) {
    	selector = '.' + matches.join(',.');
    }
    $series_container.isotope({ filter: selector });
    $series_marine.isotope({ filter: selector });
    $series_renewable_energy.isotope({ filter: selector });
    $series_agm.isotope({ filter: selector });
    $series_railroad.isotope({ filter: selector });
    $series_motive_power.isotope({ filter: selector });
    $batteries_container.isotope({ filter: selector });
}

jQuery('#searchBox').autoComplete({
	minChars: 1,
    source: function(term, suggest){
        term = term.toLowerCase();
        var choices = searchData;
        var matches = [];
        for (i=0; i<choices.length; i++)
            if (~choices[i].toLowerCase().indexOf(term)) matches.push(choices[i]);
        suggest(matches);
        updateSearchResults();
    },
    onSelect: function() {
        updateSearchResults();
    }
}).on('change keyup paste', updateSearchResults);

function selector_change_all(type) {

        selector_change('#selector_box1');
        selector_change('#step4selector');
        selector_change('#selector_box2');
        selector_change('#selector_box3');
        selector_change('#marine_box1');
        selector_change('#agm_box1');
        selector_change('#renewable_energy_box1');
        selector_change('#railroad_box1');
        selector_change('#motive_power_box1');
        selector_change('#gel_box1');
        selector_change('#series_container');
        selector_change('#series_renewable_energy');
        selector_change('#series_marine');
        selector_change('#series_agm');
        selector_change('#series_railroad');
        selector_change('#series_motive_power');
	if (type == 'selector_box') {
        	selector_change('.series_mobile');
	}

}

function selector_change(thisone) {
        if (jQuery(thisone).is(':visible')){
                jQuery(thisone).hide(400);
        } else {
                jQuery(thisone).show(400);
        }
}

var $series_container = jQuery('#series_container').isotope({
        isOriginLeft: false,
        sortBy: 'name',
        sortAscending: false,
        layoutMode: 'horizontal',
});

var $series_marine = jQuery('#series_marine').isotope({
        sortBy: 'name',
        sortAscending: false,
        //layoutMode: 'horizontal',
	layoutMode: 'vertical',
});

var $series_renewable_energy = jQuery('#series_renewable_energy').isotope({
        sortBy: 'name',
        sortAscending: false,
        //layoutMode: 'horizontal',
	layoutMode: 'vertical',
});

var $series_agm = jQuery('#series_agm ').isotope({
        sortBy: 'name',
        sortAscending: false,
        //layoutMode: 'horizontal',
	layoutMode: 'vertical',
});

var $series_railroad = jQuery('#series_railroad ').isotope({
        sortBy: 'name',
        sortAscending: false,
        //layoutMode: 'horizontal',
	layoutMode: 'vertical',
});

var $series_motive_power = jQuery('#series_motive_power').isotope({
        sortBy: 'name',
        sortAscending: false,
        //layoutMode: 'horizontal',
	layoutMode: 'vertical',
});

jQuery('.filters a').click(function(event){
        var selector = jQuery(this).attr('data-filter');
        console.log(selector);
        $series_container.isotope({ filter: selector });
        $series_marine.isotope({ filter: selector });
        $series_renewable_energy.isotope({ filter: selector });
        $series_agm.isotope({ filter: selector });
        $series_railroad.isotope({ filter: selector });
        $series_motive_power.isotope({ filter: selector });
        $batteries_container.isotope({ filter: selector });
        event.preventDefault();
});


var $batteries_container = jQuery('#batteries_container').isotope({
        transformsEnabled: false
});

jQuery('.series_filters a').click(function(){
        var selector = jQuery(this).attr('data-filter');
        $batteries_container.isotope({ filter: selector });
        event.preventDefault();
});

jQuery('.series_filters_mobile a').click(function(){
        var selector = jQuery(this).attr('data-filter');
        $batteries_container.isotope({ filter: selector });
        event.preventDefault();
});

jQuery('.fancybox').fancybox({
	width: 600,
	height: 400,
	type: 'iframe'
});


jQuery(".sidebar h5").click(function (e) {
        e.preventDefault();

        jQuery(this).parent().find(".tgl_c").slideToggle(300);
        if (jQuery(this).hasClass("active")) {
            jQuery(this).removeClass('active');
        } else {
            jQuery(this).addClass('active');
        }

});


jQuery(".batterydata").click(function (e) {

        var $this = jQuery(this);

	// Battery Capacity

	var $divrate_5_hr = jQuery('div#battery_5_hr');
	var $divrate_8_hr = jQuery('div#battery_8_hr');
	var $divrate_20_hr = jQuery('div#battery_20_hr');
	var $divrate_100_hr = jQuery('div#battery_100_hr');

	var rate_5 = $this.data('5_hr_rate') + ' AH'
	var rate_8 = $this.data('8_hr_rate') + ' AH';
	var rate_20 = $this.data('20_hr_rate') + ' AH';
	var rate_100 = $this.data('100_hr_rate') + ' AH';

	var step5modelnumber = $this.data('modelnumber');

	$divrate_5_hr.html(rate_5);
	$divrate_8_hr.html(rate_8);
	$divrate_20_hr.html(rate_20);
	$divrate_100_hr.html(rate_100);

        var matcharr20hr = $this.data('20_hr_rate');

	// Dimensions

	var $divlength_cm = jQuery('div#length_cm');
	var $divlength_inches = jQuery('div#length_inches');
	var $divwidth_cm = jQuery('div#width_cm');
	var $divwidth_inches = jQuery('div#width_inches');
	var $divheight_cm = jQuery('div#height_cm');
	var $divheight_inches = jQuery('div#height_inches');

	var this_length_cm = $this.data('system_cm_length');
	var this_length_inches = $this.data('system_inches_length') + ' "';
	var this_width_cm = $this.data('system_cm_width');
	var this_width_inches = $this.data('system_inches_width') + ' "';
	var this_height_cm = $this.data('system_cm_height');
	var this_height_inches = $this.data('system_inches_height') + ' "';

	var res_st = this_length_inches.split(" ");
	if (!res_st[1] == '' && !res_st[1] == '"') {
		var fraction = res_st[1];
		fraction = fraction.split(" ");
		var fraction_a = fraction[0].split("/");
		this_length_inches = res_st[0] + ' ' + '<span class="fraction"><sup>' + fraction_a[0] + '</sup>/<sub>' + fraction_a[1] + '</sub></span>"';
	}

	res_st = this_width_inches.split(" ");
	if (!res_st[1] == '' && !res_st[1] == '"') {
		var fraction = res_st[1];
		fraction = fraction.split(" ");
		var fraction_a = fraction[0].split("/");
		this_width_inches = res_st[0] + ' ' + '<span class="fraction"><sup>' + fraction_a[0] + '</sup>/<sub>' + fraction_a[1] + '</sub></span>"';
	}

	res_st = this_height_inches.split(" ");
	if (!res_st[1] == '' && !res_st[1] == '"') {
		var fraction = res_st[1];
		fraction = fraction.split(" ");
		var fraction_a = fraction[0].split("/");
		this_height_inches = res_st[0] + ' ' + '<span class="fraction"><sup>' + fraction_a[0] + '</sup>/<sub>' + fraction_a[1] + '</sub></span>"';
	}

	$divlength_cm.html(this_length_cm);
	$divlength_inches.html(this_length_inches);
	$divwidth_cm.html(this_width_cm);
	$divwidth_inches.html(this_width_inches);
	$divheight_cm.html(this_height_cm);
	$divheight_inches.html(this_height_inches);

	// Weight

	var $divdry_lbs = jQuery('div#weight_dry_lbs');
	var $divdry_kg = jQuery('div#weight_dry_kg');
	var $divwet_lbs = jQuery('div#weight_wet_lbs');
	var $divwet_kg = jQuery('div#weight_wet_kg');

	var this_dry_lbs = $this.data('system_dry_lbs') + ' lbs';
	var this_dry_kg = $this.data('system_dry_kg') + ' kg';
	var this_wet_lbs = $this.data('system_wet_lbs') + ' lbs';
	var this_wet_kg = $this.data('system_wet_kg') + ' kg';

	$divdry_lbs.html(this_dry_lbs);
	$divdry_kg.html(this_dry_kg);
	$divwet_lbs.html(this_wet_lbs);
	$divwet_kg.html(this_wet_kg);

	jQuery('div#selector_steps').hide();
	jQuery('div#product_description').show();

        var image_filename = '/uploads/batteries/' + $this.data('battery_filename');
	jQuery('img#sel_batteryimage').attr('src', image_filename);

	var system_1_string_v12 = $this.data('system_1_string_12_v');
	var system_2_string_v12 = $this.data('system_2_string_12_v');
	var system_3_string_v12 = $this.data('system_3_string_12_v');
	var system_1_string_v24 = $this.data('system_1_string_24_v');
	var system_2_string_v24 = $this.data('system_2_string_24_v');
	var system_3_string_v24 = $this.data('system_3_string_24_v');
	var system_1_string_v48 = $this.data('system_1_string_48_v');
	var system_2_string_v48 = $this.data('system_2_string_48_v');
	var system_3_string_v48 = $this.data('system_3_string_48_v');
	var rate_20_hr = $this.data('20_hr_rate');


	jQuery("#selected-battery").attr("data-system_1_string_12_v", system_1_string_v12);
	jQuery("#selected-battery").data("system_1_string_12_v", system_1_string_v12);
	jQuery("#selected-battery").attr("data-system_2_string_12_v", system_2_string_v12);
	jQuery("#selected-battery").data("system_2_string_12_v", system_2_string_v12);
	jQuery("#selected-battery").attr("data-system_3_string_12_v", system_3_string_v12);
	jQuery("#selected-battery").data("system_3_string_12_v", system_3_string_v12);
	jQuery("#selected-battery").attr("data-system_1_string_24_v", system_1_string_v24);
	jQuery("#selected-battery").data("system_1_string_24_v", system_1_string_v24);
	jQuery("#selected-battery").attr("data-system_2_string_24_v", system_2_string_v24);
	jQuery("#selected-battery").data("system_2_string_24_v", system_2_string_v24);
	jQuery("#selected-battery").attr("data-system_3_string_24_v", system_3_string_v24);
	jQuery("#selected-battery").data("system_3_string_24_v", system_3_string_v24);
	jQuery("#selected-battery").attr("data-system_1_string_48_v", system_1_string_v48);
	jQuery("#selected-battery").data("system_1_string_48_v", system_1_string_v48);
	jQuery("#selected-battery").attr("data-system_2_string_48_v", system_2_string_v48);
	jQuery("#selected-battery").data("system_2_string_48_v", system_2_string_v48);
	jQuery("#selected-battery").attr("data-system_3_string_48_v", system_3_string_v48);
	jQuery("#selected-battery").data("system_3_string_48_v", system_3_string_v48);
	jQuery("#selected-battery").attr("data-20_hr_rate", rate_20_hr);
	jQuery("#selected-battery").data("20_hr_rate", rate_20_hr);

        //Desired Amp Hr
        minandmax = $desiredamphr.prop("value").split(";");
        minAmpHr = minandmax[0];
        maxAmpHr = minandmax[1];

	// Strings

        var thisbatteryvoltage = $this.data('voltage');
	var string1 = false;
	var string2 = false;
	var string3 = false;
	var selected = true;

        var thisstring = $stringselect.val();

	var string_st = '';
        if (matcharr20hr) {
	        var amphr20 = parseInt(matcharr20hr);
                var amphr20x2 = amphr20 * 2;
                var amphr20x3 = amphr20 * 3;
                if ((amphr20 >= minAmpHr) && (amphr20 <= maxAmpHr)) {
				if (selected) {
					selected_st = "selected";
					selected = false;
					thisstring = 1;
				}
                       		string1 = true;
				string_st = '<option ' + selected_st + ' >1</option>';
                }
                if ((amphr20x2 >= minAmpHr) && (amphr20x2 <= maxAmpHr)) {
				selected_st = "";
				if (selected) {
					selected_st = "selected";
					selected = false;
					thisstring = 2;
				}
                       		string2 = true;
				string_st = string_st + '<option ' + selected + ' >2</option>';
                }
                if ((amphr20x3 >= minAmpHr) && (amphr20x3 <= maxAmpHr)) {
				selected_st = "";
				if (selected) {
					selected_st = "selected";
					selected = false;
					thisstring = 3;
				}
               			string3 = true;
				string_st = string_st + '<option ' + selected + '>3</option>';
                }
	}

	if ((string1) || (string2) || (string3)) {
		$stringselect.empty().append(string_st);
	}


	// Charger Output

	var totalcapacity = rate_20_hr * thisstring;

        var matcharr20hr = $this.data('20_hr_rate');
	var this_5_charge = Math.round(totalcapacity * .05);
	var this_10_charge = Math.round(totalcapacity * .1);

	var numberofamps = this_5_charge / 5;
	var reminderofamps = this_5_charge % 5;

	if (reminderofamps > 0) {
		numberofamps = Math.floor(numberofamps);
		numberofamps = numberofamps + 1;
	}

	numberofamps = Math.floor(numberofamps);
	numberofamps = numberofamps * 5 ;
	$chargeroutput.val(numberofamps);

	var aborptioncharge = (totalcapacity / numberofamps * 0.42).toFixed(2);
	chargeroutput_st = '<strong>' + aborptioncharge + ' Hours</strong>';
	jQuery("#charge_time").html(chargeroutput_st);

        var thisvoltage = $voltageselect.val();

        switch (thisvoltage) {
        	case '12V':
			switch (thisstring) {
				case 1:
					jQuery("#Quantity").val(system_1_string_v12);
					break;
				case 2:
					jQuery("#Quantity").val(system_2_string_v12);
					break;
				case 3:
					jQuery("#Quantity").val(system_3_string_v12);
					break;
			}
			break;
        	case '24V':
			switch (thisstring) {
				case 1:
					jQuery("#Quantity").val(system_1_string_v24);
					break;
				case 2:
					jQuery("#Quantity").val(system_2_string_v24);
					break;
				case 3:
					jQuery("#Quantity").val(system_3_string_v24);
					break;
			}
			break;
        	case '48V':
			switch (thisstring) {
				case 1:
					jQuery("#Quantity").val(system_1_string_v48);
					break;
				case 2:
					jQuery("#Quantity").val(system_2_string_v48);
					break;
				case 3:
					jQuery("#Quantity").val(system_3_string_v48);
					break;
			}
			break;
      	}

	var totalcapacity = thisstring * matcharr20hr;

/*
	jQuery("#selected-battery").attr("data-charger", thischargeroutput);
	jQuery("#selected-battery").attr("data-strings", thisstring);

	var thischargeroutput = $chargeroutput.val();
	var aborptioncharge = Math.round(totalcapacity / thischargeroutput * 0.42);
	chargeroutput_st = '<strong>' + aborptioncharge + ' Hours</strong>';
	jQuery("#charge_time").html(chargeroutput_st);
*/


	var model = '<strong> SELECTED MODEL:  <span style="margin-left:30px;font-size:120%;">' + $this.data('battery_name') + '</span></strong>';
	model = model.replace(/_/g,' ');
	var voltage = $this.data('voltage') + 'V';

	var $divmodel = jQuery('div#model');
	var $divvoltage = jQuery('div#voltage');

	$divvoltage.html(voltage);
	$divmodel.html(model);

	var total_ah_capacity = '<strong>TOTAL AH CAPACITY: ' + totalcapacity + ' AH';
	var $total_ah_capacity_div = jQuery('div#total_ah_capacity');
	$total_ah_capacity_div.html(total_ah_capacity);

	var minimum_chargeroutput = Math.round(totalcapacity * 0.05);
	var recommended_chargeroutput = Math.round(totalcapacity * 0.1);
	jQuery('#minimum_chargeroutput').html(minimum_chargeroutput);
	jQuery('#recommended_chargeroutput').html(recommended_chargeroutput);

	//var this_5_max_10_charge_statement = '*Charge output: minimum = 5% (' + this_5_charge + ' Amps), max 10% (' + this_10_charge + ' Amps)<br/>Total AH Capacity ' + totalcapacity + ' AH is 20 Hr AH rating of battery x number of strings.<br/>All calculations assume connection in series. 50% DOD<br/>TOTAL AH CAPACITY = ' + totalcapacity + ' AH';

	//var this_5_max_10_charge_statement = '<section class="sectiontable"><div style="width:100%;text-align:center;"> <strong>Charge Output</strong> </div> </section> <section class="sectiontable"> <div class="hrcolumn"> Minimum 5%</div> <div class="hrcolumn">' + this_5_charge + ' Amps</div> <div class="hrcolumn"> Maximum 10%</div> <div class="hrcolumn">' + this_10_charge + ' Amps</div> </section> <section class="sectiontable"> <div style="width:50%;text-align:center;"><strong>Aborption Charge Time</strong></div>	<div style="width:50%;text-align:center;"><strong>Charging Parameters</strong></div></section><section class="sectiontable"> <div style="width:50%;text-align:center;" id="chargeroutput_st"><span style="font-size:60%;">[(20hr rate of battery x strings / charger output) x.42]</span><br/>' + aborptioncharge + ' Hours</div> <div style="width:50%;text-align:center;"><a href="/uploads/pdfs/ACP.pdf" class="fancybox">AGM Charging</a><br/><a href="/uploads/pdfs/FCP.pdf" class="fancybox">Flooded Charging</a></div> </section><section class="sectiontable"> <div id="totalcapacity_st" style="width:100%;text-align:center;font-size:120%;"><strong>TOTAL AH CAPACITY: ' + totalcapacity + ' AH</strong><br/><span style="font-size:60%;">All calculations assume connection in series. 50% DOD</span></div></section> ';

	var tag = jQuery('#content');

	var windowsize = jQuery(window).width();
	if ( windowsize < 640 ) {

	// make sure last one is to be displayed
	jQuery(".batterymodeldata").show();
	jQuery(".batterymodeldatafirstpart").show();
	jQuery(".batterymodeldata2ndpart").hide();
	jQuery(".selectorbox_absorptioncharge").hide();

	jQuery("#step5thandquantitybox").show();
	var thismodelbox = "#step5thandqualityboxmodelbox-" + step5modelnumber;
	jQuery("#step5thandquantitybox").appendTo(jQuery(thismodelbox));

	var thisbatterydata = "#batterymodeldata-" + step5modelnumber;
	var thisbatterymodeldatafirstpart = "#batterymodeldatafirstpart-" + step5modelnumber;
	var thisbatterymodeldata2ndpart = "#batterymodeldata2ndpart-" + step5modelnumber;

	var selectorbox_absorptioncharge = "#selector_box3";
	var thisselectorbox_absorptioncharge = "#selectorbox_absorptioncharge";
	var thisselectorboxpart2_absorptioncharge = "#selectorboxpart2_absorptioncharge-" + step5modelnumber;

//selectorbox_absorptioncharge
		jQuery("#selected_box").show();
		jQuery("#selected_box").appendTo(jQuery(thisbatterymodeldata2ndpart));

		jQuery(thisbatterymodeldata2ndpart).show();
		jQuery(thisbatterymodeldatafirstpart).hide();

		jQuery(thisselectorboxpart2_absorptioncharge).show();
		jQuery(thisselectorbox_absorptioncharge).appendTo(jQuery(thisselectorboxpart2_absorptioncharge));
		jQuery(thisselectorboxpart2_absorptioncharge).show();
		jQuery(thisselectorbox_absorptioncharge).show();
		jQuery(selectorbox_absorptioncharge).hide();
	}

//alert(thisselectorboxpart2_absorptioncharge + ' ' + selectorbox_absorptioncharge + ' ' + thisselectorbox_absorptioncharge);

// #selectorboxpart2_absorptioncharge-1 #selector_box3 #selectorbox_absorptioncharge

        $batteries_container.isotope('layout');

	return false;

});

});
