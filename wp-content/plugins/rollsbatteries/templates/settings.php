<div class="wrap">
    <h2>Rolls Batteries</h2>
    <form method="post" action="options.php"> 
        <?php @settings_fields('rolls_batteries-group'); ?>
        <?php @do_settings_fields('rolls_batteries-group'); ?>

        <?php do_settings_sections('rolls_batteries'); ?>

        <?php @submit_button(); ?>
    </form>
</div>
