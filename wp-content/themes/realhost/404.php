<?php
/*
404.php
*/

get_header(); ?>



<div id="breadcrumb_wrapper">
	<div class="wrap">
			
		<h3><?php wp_title(''); ?></h3>

	</div>
</div>

<div class="clear"></div>

<!-- START content -->
<div class="content">
	<div class="wrap search_form_404">
			
		<div id="page_404">
			<p><?php echo _e('The page you are looking for has not been found. Please go back or use the search form below.', 'realhost') ?></p>
			<?php get_search_form(); ?>
		</div>

		<div class="clear"></div>
		
	</div>
</div>
<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>
