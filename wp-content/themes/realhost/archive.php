<?php
/*
index.php
*/

get_header(); ?>
	
	<div id="breadcrumb_wrapper">
		<div class="wrap">
				
			<h3>
			<?php
				if ( is_category() ) :
					single_cat_title();

				elseif ( is_tag() ) :
					single_tag_title();

				elseif ( is_author() ) :
					/* Queue the first post, that way we know
					 * what author we're dealing with (if that is the case).
					*/
					the_post();
					printf( __( 'Author: %s', 'realhost' ), '<span class="vcard">' . get_the_author() . '</span>' );
					/* Since we called the_post() above, we need to
					 * rewind the loop back to the beginning that way
					 * we can run the loop properly, in full.
					 */
					rewind_posts();

				elseif ( is_day() ) :
					printf( __( 'Day: %s', 'realhost' ), '<span>' . get_the_date() . '</span>' );

				elseif ( is_month() ) :
					printf( __( 'Month: %s', 'realhost' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

				elseif ( is_year() ) :
					printf( __( 'Year: %s', 'realhost' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

				elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
					_e( 'Asides', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
					_e( 'Images', 'realhost');

				elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
					_e( 'Videos', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
					_e( 'Quotes', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
					_e( 'Statuses', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
					_e( 'Galleries', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
					_e( 'Chat', 'realhost' );

				elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
					_e( 'Links', 'realhost' );

				else :
					_e(wp_title(), 'realhost');

				endif;
			?>
			</h3>

		</div>
	</div>

	<!-- START content -->
	<div class="content">
		<div class="wrap">
		
			<!-- sidebar -->
			<?php get_sidebar('left'); ?>

			<div class="content_right">

				<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

					<?php get_template_part( 'content', get_post_format() ); ?>
					
				<?php endwhile; ?>
				
				<?php else : ?>
					<h3 class="center"><?php _e('Not Found', 'realhost') ?></h3>
					<p class="center">
						<?php _e("Sorry, no posts were found.", 'realhost'); ?>
					</p>
				
				<?php endif; ?>
				<?php
					if (function_exists('wp_pagenavi')) {
					    wp_pagenavi();
					}
					else
					{
					    echo '<div id="pagination">';
					        echo previous_posts_link('&laquo; Previous') . next_posts_link('Next &raquo;');
					        echo '<div class="clear"></div>';
					    echo '</div>';
					}
				?>
				
					<div class="clear"></div>

			</div>
				
			<div class="clear"></div>
			
		</div>
	</div>
	<!-- END content -->


<!-- START footer -->
<?php get_footer(); ?>
