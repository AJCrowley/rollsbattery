<?php
/*
comments.php
*/
?>


<?php 

// Prevent the direct loading of this file.
if (!empty($_SERVER['SCRIPT-FILENAME']) && basename($_SERVER['SCRIPT-FILENAME']) == 'comments.php' ) {
	die(__('You cannot access this file directly.', 'realhost'));
}

// Check if the post is password protected.
if (post_password_required()) : ?>
	<p>
		<?php _e('This post is password protected. Enter the password to view the comments.', 'realhost'); ?>
		<?php return; ?>
	</p>

	<?php endif;

if (have_comments()) : ?>

	<section class="comments" id="comments">
	<h3><?php comments_number(__('No Comments', 'realhost'), __('1 Comment', 'realhost'), __('% Comments', 'realhost')); ?></h3>
		
		<div class="clear"></div>
		<div class="space"></div>

		<ol class="commentslist">
			<?php wp_list_comments('callback=realhost_comments'); ?>
		</ol>

		<div class="clear"></div>

		<?php if(get_comment_pages_count() >= 1 && get_option('page_comments')) : ?>
			<div class="comments-nav">
				<p class="alignleft"><?php previous_comments_link(__('&larr; Older Comments', 'realhost')); ?></p>
				<p class="alignright"><?php next_comments_link(__('Newer Comments &rarr;', 'realhost')); ?></p>
			</div>
		<?php endif; ?>

		<div class="clear"></div>

	</section>


<?php else : ?>

	<p><?php _e('There are no comments.' , 'realhost'); ?></p>

<?php endif;

	// Display the comment form
	comment_form();

?>