<?php
/*
content-aside.php
*/
?>

<article class="blog_post">
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	} ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_content(); ?>
	</div>
	
	<ul class="blog_post_details">
		<li><i class="icon-time"></i><?php the_time( get_option( "date_format" ) ); ?></li>
	</ul>
</article>

	<div class="clear"></div>
	<div class="space"></div>