<?php
/*
content-gallery.php
*/
?>

<article class="blog_post">
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	} ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><h4><?php the_title(); ?></h4></a>
	
		<ul class="blog_post_details">
			<li><i class="icon-time"></i><?php the_time( get_option( "date_format" ) ); ?></li>
		</ul>
		
		<?php if(!is_single()) the_excerpt(); else the_content(); ?>
	</div>
</article>

	<div class="clear"></div>
	<div class="space"></div>