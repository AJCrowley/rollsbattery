<?php
/*
content.php
*/
?>

<article class="blog_post">
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	} ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><h4><?php the_title(); ?></h4></a>

		<ul class="blog_post_details">
			<li><i class="icon-folder-close"></i>in <?php the_category(', ') ?></li>
			<li><i class="icon-user"></i>by <?php the_author_posts_link(); ?></li>
			<li><i class="icon-time"></i><?php the_time( get_option( "date_format" ) ); ?></li>
			<li><i class="icon-comments"></i><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></li>
			<?php if(has_tag()) : ?>
				<li><i class="icon-tag"></i><?php the_tags(); ?></li>
			<?php endif; ?>
		</ul>
		
		<div class="blog_post_content">
			<?php if(!is_single()) the_excerpt(); else the_content(); ?>
		</div>
	</div>
</article>

	<div class="clear"></div>
	<div class="space"></div>