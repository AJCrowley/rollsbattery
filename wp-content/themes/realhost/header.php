<!DOCTYPE html>
<html <?php language_attributes(); ?> class="noIE">

<head>
        
    <title><?php wp_title(''); ?></title>

	<meta name="author" content="DanThemes" />
	<meta http-equiv="content-type" content="<?php bloginfo('html_type'); ?>;charset=<?php bloginfo('charset'); ?>" />
	<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<link rel="icon" type="image/x-icon" href="http://rollsbattery.com/favicon.ico" />
	<?php 
		$get_template_directory_uri = get_template_directory_uri();
		if(ot_get_option( 'select_favicon' )) { 
			echo '<link rel="shortcut icon" href="' . esc_url(ot_get_option( 'select_favicon' )) . '" />';
		}
	?>
	<link rel='alternate' type='application/rss+xml' title='RSS 2.0' href="<?php bloginfo('rss2_url'); ?>" />
	<link rel='pingback' href="<?php bloginfo('pingback_url'); ?>" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

	<div id="top_links">
		<div class="wrap">
			<ul>
				<?php
					$login = ot_get_option('login');
					$live_chat = ot_get_option('live_chat');
					$phone = ot_get_option('phone');
					$ticket = ot_get_option('ticket');
					$email = ot_get_option('email');

					if ($login == true) { echo '<li><i class="icon-unlock"></i><a href="' . esc_url($login) . '">Login</a></li>'; }
					if ($live_chat == true) { echo '<li><i class="icon-headphones"></i><a href="' . esc_url($live_chat) . '">Live Chat</a></li>'; }
					if ($phone == true) { echo '<li><i class="icon-phone"></i>' . sanitize_text_field($phone) . '</li>'; }
					if ($ticket == true) { echo '<li><i class="icon-comments "></i><a href="' . esc_url($ticket) . '">Send Tickets</a></li>'; }
					if ($email == true) { echo '<li><i class="icon-envelope"></i><a href="mailto:' . is_email($email) . '">' . is_email($email) . '</a></li>'; }
				?>
			</ul>
		</div>
		<div class="clear"></div>
	</div>

	<div id="top">
		<div class="wrap">
	
			<!-- logo -->
			<div id="logo">
				<?php
					$logo_image = ot_get_option('logo_image');
					if ($logo_image == true) {
				?>
					<img src="<?php echo get_template_directory_uri(); ?>/images/main_logo.png" alt="main" />
					<!--<img src="<?php echo esc_url(ot_get_option('logo_image')); ?>" alt="" />-->
				<?php } else { ?>
					<a href="<?php echo home_url(); ?>"><i class="icon-hdd"></i><h1><?php echo esc_html(ot_get_option('logo')); ?></h1></a>
				<?php } ?>
			</div>
			
			<!-- responsive menu toggle -->
			<a href="#nav" id="toggle"><i class="icon-reorder"></i></a>

			<!-- navigation menu -->
			<nav class="nav-collapse" id="nav">
				<?php if (has_nav_menu('realhost_nav')) { ?>
					<?php wp_nav_menu( array( 'theme_location' => 'realhost_nav' ) ); ?>
				<?php } ?>
			</nav>
			<div class="clear"></div>

		</div>
	</div>


