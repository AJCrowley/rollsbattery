<?php
/*
search.php
*/
?>

<!-- START header -->
<?php get_header(); ?>

	<div id="breadcrumb_wrapper">
		<div class="wrap">
				
			<h3><?php wp_title(''); ?></h3>

		</div>
	</div>

	<!-- START content -->
	<div class="content">
		<div class="wrap">
		
			<!-- sidebar left -->
			<?php get_sidebar('left'); ?>

			<div class="content_right">

				<!-- search content -->
				<?php if(have_posts()) : ?>

					<?php get_template_part( 'content', 'search' ); ?>
				
				<?php else : ?>
					<h3 class="center">Not Found</h3>
					<p class="center">
						<?php _e("Sorry, no posts were found.", 'realhost'); ?>
					</p>
				
				<?php endif; ?>
				<?php
					if (function_exists('wp_pagenavi')) {
					    wp_pagenavi();
					}
					else
					{
					    echo '<div class="default-blog-nav">';
					        echo previous_posts_link('&laquo; Previous') . next_posts_link('Next &raquo;');
					        echo '<div class="clear"></div>';
					    echo '</div>';
					}
				?>
				
					<div class="clear"></div>

			</div>
				
			<div class="clear"></div>
			
		</div>
	</div>
	<!-- END content -->


<!-- START footer -->
<?php get_footer(); ?>