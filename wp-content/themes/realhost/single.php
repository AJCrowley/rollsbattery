<?php
/*
single.php
*/

get_header(); ?>
	
	<div id="breadcrumb_wrapper">
		<div class="wrap">
				
			<h3><?php wp_title(''); ?></h3>

		</div>
	</div>
	
	<!-- START content -->
	<div class="content">
		<div class="wrap">
		
			<!-- sidebar left -->
			<?php get_sidebar('left'); ?>

			<div class="content_right">

				<?php if(have_posts()) : the_post(); ?>

					<!-- blog post -->
					<?php get_template_part( 'content', get_post_format()); ?>

					<div class="clear"></div>

					<?php $args = array(
						'before'           => '<div class="post_navigation"><span class="pages">' . __('Pages:', 'realhost') . '</span><span class="post_navigation_links">',
						'after'            => '</span></div>',
						'link_before'      => '<p>',
						'link_after'       => '</p>',
						'next_or_number'   => 'number',
						'pagelink'         => '%',
						'echo'             => 1
					); ?>
					<?php wp_link_pages( $args ); ?>
					<?php posts_nav_link(); ?>

					<?php endif; ?>
				
					<div class="clear"></div>

				<div class="comments">

					<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() && !post_password_required() ) {
						comments_template('', true);
					}

					elseif (!comments_open()) { ?>

						<p><?php _e("Comments are closed.", 'realhost'); ?></p>

					<?php } ?>

				</div>
			
			</div>
				
			<div class="clear"></div>
			
		</div>
	</div>
	<!-- END content -->

	
	<!-- START footer -->
	<?php get_footer(); ?>