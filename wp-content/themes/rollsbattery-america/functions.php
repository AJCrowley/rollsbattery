<?php
// remove redirect to fix multisite issue
remove_action('template_redirect','redirect_canonical');

// prevent plugin updates to modified plugins
function disable_plugin_updates( $value ) {
  if(isset($value) && is_object($value)) {
    if(isset($value->response['wp-google-maps/wpGoogleMaps.php'])) {
      unset( $value->response['wp-google-maps/wpGoogleMaps.php']);
    }
    if (isset($value->response['wp-google-maps-gold/wp-google-maps-gold.php'])) {
      unset( $value->response['wp-google-maps-gold/wp-google-maps-gold.php']);
    }
    if (isset($value->response['wp-google-maps-pro/wp-google-maps-pro.php'])) {
      unset( $value->response['wp-google-maps-pro/wp-google-maps-pro.php']);
    }
  }
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );

function theme_enqueue_styles() {
    $parent_style = 'realhost';
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
    );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );

function rolls_body_classes( $classes ) {
	$classes[] = 'rolls-america';
	return $classes;
}
add_filter( 'body_class', 'rolls_body_classes' );
?>
