<?php
/*
content-search.php
*/
?>

<?php while (have_posts()) : the_post(); ?>
<article class="blog_post">
	<?php if ( has_post_thumbnail() ) {
		the_post_thumbnail();
	} ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><h4><?php the_title(); ?></h4></a>
		
		<?php the_excerpt(); ?>
	</div>
	
	<ul class="blog_post_details">
		<li><i class="icon-tag"></i>in <?php the_category(', ') ?></li>
		<li><i class="icon-user"></i>by <?php the_author_posts_link(); ?></li>
		<li><i class="icon-time"></i><?php the_time( get_option( "date_format" ) ); ?></li>
		<li><i class="icon-comments"></i><?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?></li>
		<?php if(has_tag()) : ?>
			<li><i class="icon-tag"></i><?php the_tags(); ?></li>
		<?php endif; ?>
	</ul>
</article>

	<div class="clear"></div>
	<div class="space"></div>
	
<?php endwhile; ?>