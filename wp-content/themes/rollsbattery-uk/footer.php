
<footer> 
	<div class="wrap">

		<div class="footer_nav">
			<div class="two_thirds">
<!--
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('RealHost - Footer Left') ) : ?><?php endif; ?>
-->
		<div id="twitter"  >
                	<div id="twitter_logo_blue"  ></div> 
                	<div id="twitter_box" >
                        	<?php 
                                	echo do_shortcode("[AIGetTwitterFeeds ai_username='RollsBatteryUK' ai_numberoftweets='1' ai_tweet_title='Latest Tweets']");
                        	?>
				<?php //dynamic_sidebar('realhost_sidebar_tweet'); ?>
                	</div>  
        	</div>  

		<!--	 	<img src="<?php echo get_bloginfo('template_url'); ?>/images/logo_bottom.png"> -->
			</div>

			<div class="one_fourth">
<!--
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('RealHost - Footer Middle Left') ) : ?><?php endif; ?>
-->
			</div>

			<div class="one_fourth">
<!--
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('RealHost - Footer Middle Right') ) : ?><?php endif; ?>
-->
			</div>

			<div class="one_third last" style="float:right;">
<!--
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('RealHost - Footer Right') ) : ?><?php endif; ?>
-->
				<div style="float:right;top:-10px;position:relative;margin-left:30px;"><a href="http://facebook.com/RollsBattery"><img src="<?php echo get_bloginfo('template_url'); ?>/images/facebook_bottom.png" /></a></div>
				<div style="float:right;margin-left:30px;"><a href="http://www.youtube.com/user/RollsBattery/videos"><img src="<?php echo get_bloginfo('template_url'); ?>/images/youtube_bottom.png" /></a></div>
				<div style="float:right;top:-6px;position:relative;"><a href="https://twitter.com/RollsBatteryUK"><img src="<?php echo get_bloginfo('template_url'); ?>/images/twitter_bottom.png" /></a></div>
			</div>
			<div class="clear"></div>
		</div>

		<div class="clear"></div>

	</div>

	<div id="footer_bottom">

		<div class="wrap">

			<!-- copyright text -->
			<p id="copyrightbottom">Copyright &copy; <?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>. All Rights Reserved.</p>

			<!-- social icons -->
			<ul>
				<?php
					$facebook = ot_get_option('facebook');
					$tumblr = ot_get_option('tumblr');
					$twitter = 'http://twitter.com/RollsBatteryUK'; //ot_get_option('twitter');
					$youtube = ot_get_option('youtube');
					$flickr = ot_get_option('flickr');
					$foursquare = ot_get_option('foursquare');
					$github = ot_get_option('github');
					$google_plus = ot_get_option('google_plus');
					$instagram = ot_get_option('instagram');
					$linkedin = ot_get_option('linkedin');
					$bitbucket = ot_get_option('bitbucket');
					$pinterest = ot_get_option('pinterest');
					$skype = ot_get_option('skype');
					$dribbble = ot_get_option('dribbble');
					$dropbox = ot_get_option('dropbox');
					$android = ot_get_option('android');

					if ($facebook == true) { echo '<li><a href="' . esc_url($facebook) . '"><i class="icon-facebook"></i></a></li>'; }
					if ($tumblr == true) { echo '<li><a href="' . esc_url($tumblr) . '"><i class="icon-tumblr"></i></a></li>'; }
					if ($twitter == true) { echo '<li><a href="' . esc_url($twitter) . '"><i class="icon-twitter"></i></a></li>'; }
					if ($youtube == true) { echo '<li><a href="' . esc_url($youtube) . '"><i class="icon-youtube"></i></a></li>'; }
					if ($flickr == true) { echo '<li><a href="' . esc_url($flickr) . '"><i class="icon-flickr"></i></a></li>'; }
					if ($foursquare == true) { echo '<li><a href="' . esc_url($foursquare) . '"><i class="icon-foursquare"></i></a></li>'; }
					if ($github == true) { echo '<li><a href="' . esc_url($github) . '"><i class="icon-github"></i></a></li>'; }
					if ($google_plus == true) { echo '<li><a href="' . esc_url($google_plus) . '"><i class="icon-google-plus"></i></a></li>'; }
					if ($instagram == true) { echo '<li><a href="' . esc_url($instagram) . '"><i class="icon-instagram"></i></a></li>'; }
					if ($linkedin == true) { echo '<li><a href="' . esc_url($linkedin) . '"><i class="icon-linkedin"></i></a></li>'; }
					if ($bitbucket == true) { echo '<li><a href="' . esc_url($bitbucket) . '"><i class="icon-bitbucket"></i></a></li>'; }
					if ($pinterest == true) { echo '<li><a href="' . esc_url($pinterest) . '"><i class="icon-pinterest"></i></a></li>'; }
					if ($skype == true) { echo '<li><a href="' . esc_url($skype) . '"><i class="icon-skype"></i></a></li>'; }
					if ($dribbble == true) { echo '<li><a href="' . esc_url($dribbble) . '"><i class="icon-dribbble"></i></a></li>'; }
					if ($dropbox == true) { echo '<li><a href="' . esc_url($dropbox) . '"><i class="icon-dropbox"></i></a></li>'; }
					if ($android == true) { echo '<li><a href="' . esc_url($android) . '"><i class="icon-android"></i></a></li>'; }
				?>
			</ul>
			<p id="footerlinks"><a href="http://rollsbattery.com/contact/">CONTACT</a> |  <a href="http://rollsbattery.com/press/">PRESS</a>  </p>
			<div class="clear"></div>
		</div>
	</div>

	<div class="clear"></div>

</footer>


<?php wp_footer(); ?>
<script type="text/javascript" src="http://assets.freshdesk.com/widget/freshwidget.js"></script>
<script type="text/javascript">
	FreshWidget.init("", {"queryString": "&widgetType=popup", "utf8": "✓", "widgetType": "popup", "buttonType": "text", "buttonText": "Support", "buttonColor": "white", "buttonBg": "#c71222", "alignment": "4", "offset": "335px", "formHeight": "500px", "url": "http://support.rollsbattery.com"} );
</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54042156-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

</html>
