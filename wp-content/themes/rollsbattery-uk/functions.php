<?php
/*
functions.php
*/
define( 'HOME_URI', home_url() );
define( 'TEMPLATE_URI', get_template_directory_uri() );
if ( ! isset( $content_width ) ) $content_width = 1100;

// remove redirect to fix multisite issue
remove_action('template_redirect','redirect_canonical');

/*******************************************************************************************************/
/* enqueue scripts files */
/*******************************************************************************************************/

function realhost_theme_scripts() {
  wp_enqueue_style( 'fontawesome', get_template_directory_uri() . '/css/font-awesome.css');
  wp_enqueue_style( 'main-style', get_stylesheet_directory_uri() . '/style.css');
  wp_enqueue_style( 'lightslider-style', get_template_directory_uri() . '/css/lightSlider.css');
  wp_enqueue_style( 'jquery-ui-style', get_template_directory_uri() . '/css/jquery-ui.css');

  $responsive = ot_get_option( 'responsive' );
  if($responsive == 'yes') {
      wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css');
  }

  wp_enqueue_script( 'jquery' );

  wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.js', array('jquery'), '', true );
  wp_enqueue_script( 'easytabs', get_template_directory_uri() . '/js/jquery.easytabs.js', array('jquery'), '', true );
  wp_enqueue_script( 'bxslider', get_template_directory_uri() . '/js/jquery.bxSlider.min.js', array('jquery'), '', true );
  wp_enqueue_script( 'nav', get_template_directory_uri() . '/js/nav.js', array('jquery'), '', true );
  wp_enqueue_script( 'responsive-nav', get_template_directory_uri() . '/js/responsive-nav.js', array('jquery'), '', true );
  wp_enqueue_script( 'cycle-all', get_template_directory_uri() . '/js/jquery.cycle.all.js', array('jquery'), '', true );
  wp_enqueue_script( 'lightslider', get_template_directory_uri() . '/js/jquery.lightSlider.min.js', array('jquery'), '', true );
  wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js', array('jquery'), '', true );

/*
  if(is_page_template('page-vpsslider2.php')) {
      wp_enqueue_script( 'vps_slider_2', get_template_directory_uri() . '/js/vps_slider_2.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider3.php')) {
      wp_enqueue_script( 'vps_slider_3', get_template_directory_uri() . '/js/vps_slider_3.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider4.php')) {
      wp_enqueue_script( 'vps_slider_4', get_template_directory_uri() . '/js/vps_slider_4.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider5.php')) {
      wp_enqueue_script( 'vps_slider_5', get_template_directory_uri() . '/js/vps_slider_5.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider6.php')) {
      wp_enqueue_script( 'vps_slider_6', get_template_directory_uri() . '/js/vps_slider_6.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider7.php')) {
      wp_enqueue_script( 'vps_slider_7', get_template_directory_uri() . '/js/vps_slider_7.js', array('jquery'), '', true );
  }
  if(is_page_template('page-vpsslider8.php')) {
      wp_enqueue_script( 'vps_slider_8', get_template_directory_uri() . '/js/vps_slider_8.js', array('jquery'), '', true );
  }
*/

}
add_action( 'wp_enqueue_scripts', 'realhost_theme_scripts' );


function realhost_google_fonts() {
    $protocol = is_ssl() ? 'https' : 'http';
    wp_enqueue_style( 'realhost-opensans', "$protocol://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" );
    wp_enqueue_style( 'realhost-lato', "$protocol://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" );}
add_action( 'wp_enqueue_scripts', 'realhost_google_fonts' );


function realhost_custom_css() {
  echo '<style type="text/css">' . ot_get_option('custom_css') . '</style>';
}
add_action( 'wp_head', 'realhost_custom_css' );


function realhost_tracking_code() {
  echo '<script>' . ot_get_option('tracking_code') . '</script>';
}
add_action( 'wp_footer', 'realhost_tracking_code' );

function realhost_change_color_style() {
  $change_color_style = ot_get_option('change_color_style');
  if ($change_color_style == true) {
  echo '<style type="text/css">.highlight { background: ' . ot_get_option('change_color_style') . '; }'
  . 'input[type="submit"], input[type="reset"] { border: 2px solid ' . ot_get_option('change_color_style') . '; color: ' . ot_get_option('change_color_style') . '; }'
  . 'input[type="submit"]:hover, input[type="reset"]:hover { background: ' . ot_get_option('change_color_style') . ' !important; border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . 'i { color: ' . ot_get_option('change_color_style') . '; }'
  . '::selection, ::-moz-selection { background: ' . ot_get_option('change_color_style') . '; }'
  . 'a h1:hover, a h2:hover, a h3:hover, a h4:hover, a h5:hover, a h6:hover, a { color: ' . ot_get_option('change_color_style') . '; }'
  . '#top_links ul li a:hover, #toggle i, #nav li a:hover { color: ' . ot_get_option('change_color_style') . '; }'
  . '#nav > div > ul > li.current-menu-item a { border-top:3px solid ' . ot_get_option('change_color_style') . '; }'
  . '#nav ul.sub-menu > li a { border-top:0 !important; color: #aaa; }'
  . '#nav > div > ul > li.current-menu-item > a { color: ' . ot_get_option('change_color_style') . '; }'
  . '#nav ul ul li.current-menu-item a, #nav ul ul ul li.current-menu-item a { color: ' . ot_get_option('change_color_style') . '; }'
  . '#nav div > ul > li:hover > ul { border-top:3px solid ' . ot_get_option('change_color_style') . '; }'
  . '#nav ul ul li a:hover { background: ' . ot_get_option('change_color_style') . '; color: #fff; }'
  . '@media (max-width: 1060px){ #nav ul ul li a:hover { color: ' . ot_get_option('change_color_style') . '; }'
  . '#nav ul ul ul li a:hover { color: ' . ot_get_option('change_color_style') . '; } }'
  . '#nav div > ul > li > ul::after { border-bottom-color: ' . ot_get_option('change_color_style') . '; }'
  . '#nav ul ul ul li:first-child { border-top: 3px solid ' . ot_get_option('change_color_style') . '; }'
  . '#slider_wrapper h1 span { color: ' . ot_get_option('change_color_style') . '; }'
  . '.bx-pager.bx-default-pager a:hover, .bx-pager.bx-default-pager a.active { background: ' . ot_get_option('change_color_style') . '; }'
  . '.sidebar_left ul li a:hover, .sidebar_right ul li a:hover { color: ' . ot_get_option('change_color_style') . '; }'
  . '.button { border: 2px solid ' . ot_get_option('change_color_style') . '; color: ' . ot_get_option('change_color_style') . '; }'
  . '.button:hover { background: ' . ot_get_option('change_color_style') . '; border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '.black { border: 2px solid #000; color: #000; }'
  . '.black:hover { border: 2px solid #000; background: #000; color: #fff; }'
  . '.pricing_top h6, .pricing_middle ul li:hover i, .pricing_bottom a, .feature i { color: ' . ot_get_option('change_color_style') . '; }'
  . '.feature_2 span { background: ' . ot_get_option('change_color_style') . '; border:1px solid ' . ot_get_option('change_color_style') . '; }'
  . '#tabs > ul li.selected-tab a i, .ui-accordion i, .vps_highlight i, .latest-tweets li a:hover, .ds .ds_processor:before { color: ' . ot_get_option('change_color_style') . '; }'
  . '.post_navigation a p, .post_navigation a, .post_navigation span > p, .wp-pagenavi a { color: ' . ot_get_option('change_color_style') . '; }'
  . '.post_navigation span.post_navigation_links > p { background: ' . ot_get_option('change_color_style') . '; }'
  . '.post_navigation span.pages, .wp-pagenavi span { color: ' . ot_get_option('change_color_style') . '; }'
  . '.post_navigation a:hover, .wp-pagenavi a:hover, .wp-pagenavi span.current { background: ' . ot_get_option('change_color_style') . '; }'
  . '.wp-pagenavi span.current { background: ' . ot_get_option('change_color_style') . '; }'
  . '#pagination li a.pagination_current, #pagination li a:hover { background: ' . ot_get_option('change_color_style') . '; border: 1px solid ' . ot_get_option('change_color_style') . '; }'
  . '#contact_form input[type="submit"]:hover, #contact_form input[type="reset"]:hover { background: ' . ot_get_option('change_color_style') . '; }'
  . '#contact_form .success, #contact_form .error { color: ' . ot_get_option('change_color_style') . '; }'
  . 'footer a:hover { color: ' . ot_get_option('change_color_style') . '; }'
  . 'footer ul li a:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . 'footer input[type="text"]:focus { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '#footer_contact li a:hover, #footer_bottom ul li a { color: ' . ot_get_option('change_color_style') . '; }'
  . '#footer_bottom ul li a:hover { background: ' . ot_get_option('change_color_style') . '; }'
  . '#recentcomments li:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '##nav ul ul li a.selected, #nav ul ul ul li a.selected, #nav ul ul li:hover > a, #nav ul ul ul li a:hover { color: ' . ot_get_option('change_color_style') . '; }'
  . '#recentcomments li:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '#recentcomments li:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '#recentcomments li:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '#recentcomments li:hover:before { border: 2px solid ' . ot_get_option('change_color_style') . '; }'
  . '.post.sticky h4:first-child { background: ' . ot_get_option('change_color_style') . '; } .post.sticky h4:first-child:hover { background: #1a1a1a; }'
  . '.ui-slider.ui-widget-content { background: ' . ot_get_option('change_color_style') . '; }'
  //. '.ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default { background: ' . ot_get_option('change_color_style') . ' url('. TEMPLATE_URI .'/images/vps_slider_arrows.png) center center no-repeat; }'
  . '.lSSlideOuter .lSPager.lSpg > li:hover a, .lSSlideOuter .lSPager.lSpg > li.active a { background: ' . ot_get_option('change_color_style') . '; }'
  . '</style>';
  }

}
add_action( 'wp_footer', 'realhost_change_color_style' );




/*******************************************************************************************************/
/* wp_title filter */
/*******************************************************************************************************/

function realhost_wp_title( $title, $sep ) {
  if ( is_feed() ) {
    return $title;
  }

  if ( is_home() ) {
    return $title;
  }

  global $page, $paged;

  $sep = "|";

  // Add the blog name

  $title .= "";

  // Add the blog description for the home/front page.
  $site_name = get_bloginfo( 'name', 'display' );
  $site_description = get_bloginfo( 'description', 'display' );
  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title = " $site_name $sep $site_description";
  }

  // Add a page number if necessary:
  if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
    $title .= " $sep " . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
  }

  return $title;
}
add_filter( 'wp_title', 'realhost_wp_title', 10, 2 );




/*******************************************************************************************************/
/* Removes <p> and <br /> tags from inside the shortcodes */
/*******************************************************************************************************/

add_filter("the_content", "the_content_filter");

function the_content_filter($content) {

  // array of custom shortcodes requiring the fix
  $block = join("|",array("divider","googlemap","bold","list","list_item","stats","stats_item","table","table_row","table_head","table_data",
    "highlight","space","small_space","headline","message","feature","feature_2_left","feature_2_right","accordion",
    "accordion_item","testimonials","testimonial","tabs","tab","accordion","accordion_item","button","one_half","one_half_last","one_third",
    "one_third_last","two_thirds","two_thirds_last","one_fourth","one_fourth_last","one_fifth","one_fifth_last","one_half_inside",
    "one_half_inside_last","one_third_inside","one_third_inside_last","one_fourth_inside","one_fourth_inside_last","one_fifth_inside",
    "one_fifth_inside_last","pricing_table","icon","pricing_column","pricing_text","h1","h2","h3","h4","h5","h6","dedicated_servers",
    "dedicated_server","partners","partner"));

  // opening tag
  $rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);

  // closing tag
  $rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);

  return $rep;

}



/*******************************************************************************************************/
/* Include the TGM_Plugin_Activation class */
/*******************************************************************************************************/

require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'realhost_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 *
 * In this example, we register two plugins - one included with the TGMPA library
 * and one from the .org repo.
 *
 * The variable passed to tgmpa_register_plugins() should be an array of plugin
 * arrays.
 *
 * This function is hooked into tgmpa_init, which is fired within the
 * TGM_Plugin_Activation class constructor.
 */
function realhost_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name, slug and required.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
      array(
          'name'      => 'WHMCS Bridge',
          'slug'      => 'whmcs-bridge',
          'required'  => false,
      ),
      array(
          'name'      => 'Latest Tweets Widget',
          'slug'      => 'latest-tweets-widget',
          'required'  => false,
      ),
      array(
          'name'      => 'Contact Form 7',
          'slug'      => 'contact-form-7',
          'required'  => false,
      ),
      array(
          'name'      => 'WP PageNavi',
          'slug'      => 'wp-pagenavi',
          'required'  => false,
      ),
      array(
          'name'                  => 'RealHost Shortcodes', // The plugin name
          'slug'                  => 'realhost-shortcodes', // The plugin slug (typically the folder name)
          'source'                => get_template_directory_uri() . '/plugins/realhost-shortcodes.zip', // The plugin source
          'required'              => true, // If false, the plugin is only 'recommended' instead of required
          'version'               => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
          'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
          'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
          'external_url'          => '', // If set, overrides default API URL and points to an external URL
      ),

    );

    // Change this to your theme text domain, used for internationalising strings
    $theme_text_domain = 'realhost';

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'domain'            => $theme_text_domain,           // Text domain - likely want to be the same as your theme.
        'default_path'      => '',                           // Default absolute path to pre-packaged plugins
        'parent_menu_slug'  => 'themes.php',         // Default parent menu slug
        'parent_url_slug'   => 'themes.php',         // Default parent URL slug
        'menu'              => 'install-required-plugins',   // Menu slug
        'has_notices'       => true,                         // Show admin notices or not
        'is_automatic'      => false,            // Automatically activate plugins after installation or not
        'message'           => '',               // Message to output right before the plugins table
        'strings'           => array(
            'page_title'                                => __( 'Install Required Plugins', $theme_text_domain ),
            'menu_title'                                => __( 'Install Plugins', $theme_text_domain ),
            'installing'                                => __( 'Installing Plugin: %s', $theme_text_domain ), // %1$s = plugin name
            'oops'                                      => __( 'Something went wrong with the plugin API.', $theme_text_domain ),
            'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
            'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
            'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
            'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
            'return'                                    => __( 'Return to Required Plugins Installer', $theme_text_domain ),
            'plugin_activated'                          => __( 'Plugin activated successfully.', $theme_text_domain ),
            'complete'                                  => __( 'All plugins installed and activated successfully. %s', $theme_text_domain ) // %1$s = dashboard link
        )
    );

    tgmpa( $plugins, $config );

}



/*******************************************************************************************************/
/* load optiontree */
/*******************************************************************************************************/

/**
 * Optional: set 'ot_show_pages' filter to false.
 * This will hide the settings & documentation pages.
 */
add_filter( 'ot_show_pages', '__return_false' );
/**
 * Optional: set 'ot_show_new_layout' filter to false.
 * This will hide the "New Layout" section on the Theme Options page.
 */
add_filter( 'ot_show_new_layout', '__return_false' );
/**
 * Required: set 'ot_theme_mode' filter to true.
 */
add_filter( 'ot_theme_mode', '__return_true' );
/**
 * Required: include OptionTree.
 */
load_template( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );
/**
 * Theme Options
 */
load_template( trailingslashit( get_template_directory() ) . 'includes/theme-options.php' );
require get_template_directory() .'/one-click-demo-install/init.php';



/*******************************************************************************************************/
/* Enabling Theme Support */
/*******************************************************************************************************/
if (function_exists('add_theme_support')) {
  add_theme_support( 'post-thumbnails' );
  add_theme_support('post-formats', array('link', 'quote', 'gallery', 'aside', 'status', 'chat', 'image', 'video', 'audio'));
  // add_theme_support( 'custom-header', $args );
  // add_theme_support( 'custom-background', $args );
  add_theme_support( 'automatic-feed-links' );
}



/*******************************************************************************************************/
/* Read more */
/*******************************************************************************************************/
function new_excerpt_more( $more ) {
  return ' <a class="read-more" href="'. get_permalink( get_the_ID() ) . '">Continue Reading</a>';
}
add_filter( 'excerpt_more', 'new_excerpt_more' );



/*******************************************************************************************************/
/* Excerpt length */
/*******************************************************************************************************/

function custom_excerpt_length( $length ) {
  return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );



/*******************************************************************************************************/
/* Navigation menu */
/*******************************************************************************************************/
function register_my_menu() {
  register_nav_menu('realhost_nav', __( 'RealHost Navigation Menu', 'realhost' ));
}
add_action( 'init', 'register_my_menu' );



/*******************************************************************************************************/
/* Remove pages from search results */
/*******************************************************************************************************/
function remove_pages_from_search() {
    global $wp_post_types;
    $wp_post_types['page']->exclude_from_search = true;
}
add_action('init', 'remove_pages_from_search');



/*******************************************************************************************************/
/* Display comments */
/*******************************************************************************************************/
function realhost_comments($comment, $args, $depth) {
  $template_uri = TEMPLATE_URI;
  $GLOBALS['comment'] = $comment;
  if (get_comment_type() == 'pingback' || get_comment_type() == 'trackback') : ?>

    <article comment_class('pingback clear')  id="comment-<?php comment_ID(); ?>" <?php comment_class('comment clear') ?>>
      <div class="comment_text">
          <p><?php _e('Pingback', 'realhost') ?></p>

          <div class="clear"></div>
        <p><?php comment_author_link(); ?></p>
      </div>
    </article>

    <div class="clear"></div>
    <div class="divider"></div>

  <?php elseif (get_comment_type() == 'comment') : ?>

    <article id="comment-<?php comment_ID(); ?>" <?php comment_class('comment clear') ?>>
      <div class="comment_avatar">
        <?php
        $avatar_size = 80;
          if($comment->comment_parent != 0) {
            $avatar_size = 64;
          }

          echo get_avatar($comment, $avatar_size);
        ?>
      </div>
      <div class="comment_text">
        <?php comment_text(); ?>

        <div class="clear"></div>

        <ul class="comment_details">
          <li><i class="icon-user"></i>by <?php comment_author_link(); ?></li>
          <li><i class="icon-time"></i><?php comment_time(); ?>, <?php comment_date(); ?></li>
          <li><?php if( comments_open() ) { echo '<i class="icon-pencil"></i>'; } comment_reply_link(array_merge($args, array(
          'depth' => $depth,
          'max-depth' => $args['max_depth']
          ))); ?></li>
        </ul>
      </div>
        <div class="clear"></div>

        <?php if($comment->comment_approved == '0') : ?>
          <p class="awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'realhost') ?></p>
        <?php endif; ?>
      </article>
    <div class="clear"></div>

  <?php endif;
}
function realhost_comments_reply() {
  if( get_option( 'thread_comments' ) )  {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'comment_form_before', 'realhost_comments_reply' );



function realhost_custom_comment_form($defaults) {
  $defaults['comment_notes_before'] = '';
  return $defaults;
}
add_filter('comment_form_defaults', 'realhost_custom_comment_form');



/*******************************************************************************************************/
/* Creating Widget Areas */
/*******************************************************************************************************/
if (function_exists('register_sidebar')) {
  register_sidebar(array(
    'name' => __('RealHost - Footer Left', 'realhost'),
    'id'   => 'realhost_footer_left',
    'description'   => 'The left area of the footer.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('RealHost - Footer Middle Left', 'realhost'),
    'id'   => 'realhost_footer_middle_left',
    'description'   => 'The middle left area of the footer.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('RealHost - Footer Middle Right', 'realhost'),
    'id'   => 'realhost_footer_middle_right',
    'description'   => 'The middle right area of the footer.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('RealHost - Footer Right', 'realhost'),
    'id'   => 'realhost_footer_right',
    'description'   => 'The right area of the footer.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('RealHost - Sidebar Left', 'realhost'),
    'id'   => 'realhost_sidebar_left',
    'description'   => 'The right sidebar area.',
    'before_widget' => '',
    'after_widget'  => '<div class="clear"></div><div class="space2"></div>',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('RealHost - Sidebar Right', 'realhost'),
    'id'   => 'realhost_sidebar_right',
    'description'   => 'The left sidebar area.',
    'before_widget' => '',
    'after_widget'  => '<div class="clear"></div><div class="space2"></div>',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));

  register_sidebar(array(
    'name' => __('Sidebar Footer - Tweet', 'realhost'),
    'id'   => 'realhost_sidebar_tweet',
    'description'   => 'The footer tweet area.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '<h6>',
    'after_title'   => '</h6>'
  ));
  register_sidebar(array(
    'name' => __('Frontpage Quote 1', 'realhost'),
    'id'   => 'realhost_frontpage_quote_1',
    'description'   => 'The frontpage quote 1.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => ''
  ));
  register_sidebar(array(
    'name' => __('Frontpage Quote Person', 'realhost'),
    'id'   => 'realhost_frontpage_quote_person',
    'description'   => 'The frontpage quote person.',
    'before_widget' => '',
    'after_widget'  => '',
    'before_title'  => '',
    'after_title'   => ''
  ));
}



/*******************************************************************************************************/
/* Footer Contact Us Widget */
/*******************************************************************************************************/
class realhost_get_in_touch_widget extends WP_Widget {
  function realhost_get_in_touch_widget() {
    parent::WP_Widget(
      false,
      $name = __('RealHost - Footer Contact Us Widget', 'realhost_get_in_touch'),
      array('description' => 'This widget displays the address, phone and email of your company in the footer.')
    );
  }
  function form($instance) {
    if ($instance) {
      $title = esc_attr($instance['title']);
      $address = esc_attr($instance['address']);
      $phone = esc_attr($instance['phone']);
      $email = esc_attr($instance['email']);
    } else {
      $title = '';
      $address = '';
      $phone = '';
      $email = '';
    }
    ?>

    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title', 'realhost_get_in_touch_widget'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Office Address', 'realhost_get_in_touch_widget'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" />
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('phone'); ?>"><?php _e('Phone Number', 'realhost_get_in_touch_widget'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('phone'); ?>" name="<?php echo $this->get_field_name('phone'); ?>" type="text" value="<?php echo $phone; ?>" />
    </p>

    <p>
      <label for="<?php echo $this->get_field_id('email'); ?>"><?php _e('Email Address', 'realhost_get_in_touch_widget'); ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('email'); ?>" name="<?php echo $this->get_field_name('email'); ?>" type="text" value="<?php echo $email; ?>" />
    </p>

    <?php
  }
  function update($new_instance, $old_instance) {
    $instance = $old_instance;

    $instance['title'] = strip_tags($new_instance['title']);
    $instance['address'] = strip_tags($new_instance['address']);
    $instance['phone'] = strip_tags($new_instance['phone']);
    $instance['email'] = strip_tags($new_instance['email']);

    return $instance;
  }
  function widget($args, $instance) {
    extract ($args);
    // these are the widget options
    $title = apply_filters('widget_title', $instance['title']);
    $address = $instance['address'];
    $phone = $instance['phone'];
    $email = $instance['email'];

    // Display the widget
    echo $before_widget;

    // Check if title is set
    if($title) {
      echo $before_title . $title . $after_title;
    }
    if($address) {
      echo '<ul id="footer_contact"><li id="footer_address">' . esc_html($address) . '</li>';
    }
    if($phone) {
      echo '<li id="footer_phone">' . sanitize_text_field($phone) . '</li>';
    }
    if($email) {
      echo '<li id="footer_email"><a href="mailto:' . is_email($email) . '">' . is_email($email) . '</a></li>';
    }

    echo '</ul>' . $after_widget;
  }
}
add_action('widgets_init', create_function('', 'return register_widget("realhost_get_in_touch_widget");'));

function rolls_body_classes( $classes ) {
  $classes[] = 'rolls-uk';
  return $classes;
}
add_filter( 'body_class', 'rolls_body_classes' );
?>
