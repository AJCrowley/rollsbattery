<?php
/**
 * Initialize the options before anything else. 
 */
add_action( 'admin_init', 'custom_theme_options', 1 );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {
  /**
   * Get a copy of the saved settings array. 
   */
  $saved_settings = get_option( 'option_tree_settings', array() );
  
  /**
   * Custom settings array that will eventually be 
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'content'       => array( 
        array(
          'id'        => 'general_help',
          'title'     => 'General',
          'content'   => '<p>Help content goes here!</p>'
        )
      ),
      'sidebar'       => '<p>Sidebar content goes here!</p>',
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'header_info',
        'title'       => 'Header Info'
      )
      ,array(
        'id'          => 'contact_page',
        'title'       => 'Contact Page'
      ),
      array(
        'id'          => 'social_icons',
        'title'       => 'Social Icons'
      ),
      array(
        'id'          => 'vps_plan_1',
        'title'       => 'VPS Slider - Plan 1'
      ),
      array(
        'id'          => 'vps_plan_2',
        'title'       => 'VPS Slider - Plan 2'
      ),
      array(
        'id'          => 'vps_plan_3',
        'title'       => 'VPS Slider - Plan 3'
      ),
      array(
        'id'          => 'vps_plan_4',
        'title'       => 'VPS Slider - Plan 4'
      ),
      array(
        'id'          => 'vps_plan_5',
        'title'       => 'VPS Slider - Plan 5'
      ),
      array(
        'id'          => 'vps_plan_6',
        'title'       => 'VPS Slider - Plan 6'
      ),
      array(
        'id'          => 'vps_plan_7',
        'title'       => 'VPS Slider - Plan 7'
      ),
      array(
        'id'          => 'vps_plan_8',
        'title'       => 'VPS Slider - Plan 8'
      )
    ),
     'settings'        => array(


     
     
     /*******************************************************************************************************/
     /* General section */
     /*******************************************************************************************************/
     
     	array(
        'label'       => 'Logo Text',
        'id'          => 'logo',
        'type'        => 'text',
        'desc'        => 'You can change the logo text here.',
        'std'         => 'RealHost',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Logo Image',
        'id'          => 'logo_image',
        'type'        => 'upload',
        'desc'        => 'You can upload an image to use as the logo. This option overrides the "Logo Text" option above..',
        'std'         => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'general'
      ),
      array(
        'label'       => 'Change Color Style',
        'id'          => 'change_color_style',
        'type'        => 'colorpicker',
        'desc'        => 'Choose your color style (default: #c65757).',
        'std'         => '#c65757',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'general'
      ),
     	array(
        'label'       => 'Change Favicon',
        'id'          => 'select_favicon',
        'type'        => 'upload',
        'desc'        => 'You can change the favicon here.',
        'std'         => '',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'general'
      ),
      
      array(
        'label'       => 'Do you want your website to be responsive?',
        'id'          => 'responsive',
        'type'        => 'select',
        'desc'        => 'Here you can choose to disable or enable your website responsiveness.',
        'choices'     => array(
          array(
            'label'       => 'Yes, I want it to be responsive.',
            'value'       => 'yes'
          ),
          array(
            'label'       => 'No, I don\'t want it to be responsive.',
            'value'       => 'no'
          )
        ),
        'std'         => 'yes',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'class'       => '',
        'section'     => 'general'
      ),
      array(
            'label'       => 'Custom CSS',
            'id'          => 'custom_css',
            'type'        => 'textarea-simple',
            'section'     => 'general',
            'desc'        => 'You can add here your custom CSS.',
            'std'         => '',
            'rows'        => '10',
            'post_type'   => '',
            'taxonomy'    => '',
            'class'       => ''
       ),
      array(
            'label'       => 'Add your tracking code to the footer',
            'id'          => 'tracking_code',
            'type'        => 'textarea-simple',
      	   	'section'     => 'general',
            'desc'        => 'If you would like to add your tracking code to this site, then please insert your code to this text area. It will be added automatically in the footer of every page.',
            'std'         => '',
            'rows'        => '10',
            'post_type'   => '',
            'taxonomy'    => '',
            'class'       => ''
       ),
      

      
      
      /*******************************************************************************************************/
      /* Header Info section */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Client Login',
        'id'          => 'login',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your client login page.',
        'section'     => 'header_info'
      ),
      array(
        'label'       => 'Live Chat',
        'id'          => 'live_chat',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Live Chat software.',
        'section'     => 'header_info'
      ),
      array(
        'label'       => 'Phone Number',
        'id'          => 'phone',
        'type'        => 'text',
        'desc'        => 'Enter the phone number for customer support.',
        'section'     => 'header_info'
      ),
      array(
        'label'       => 'Send a Ticket',
        'id'          => 'ticket',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Ticket System (e.g. WHMCS).',
        'section'     => 'header_info'
      ),
      array(
        'label'       => 'Email Address',
        'id'          => 'email',
        'type'        => 'text',
        'desc'        => 'Enter the email address for customer support.',
        'section'     => 'header_info'
      ),
      

      
      
      /*******************************************************************************************************/
      /* Contact Page section */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Headline',
        'id'          => 'contact_headline',
        'type'        => 'text',
        'desc'        => 'Enter a headline for the top paragraph.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Text Area',
        'id'          => 'contact_text',
        'type'        => 'textarea',
        'desc'        => 'Write a short paragraph.',
        'section'     => 'contact_page'
      ),

      array(
        'label'       => 'Department #1 - Name',
        'id'          => 'department_1_name',
        'type'        => 'text',
        'desc'        => 'Enter the name of department #1.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #1 - Email Address',
        'id'          => 'department_1_email',
        'type'        => 'text',
        'desc'        => 'Enter the email address of department #1.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #2 - Name',
        'id'          => 'department_2_name',
        'type'        => 'text',
        'desc'        => 'Enter the name of department #2.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #2 - Email Address',
        'id'          => 'department_2_email',
        'type'        => 'text',
        'desc'        => 'Enter the email address of department #2.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #3 - Name',
        'id'          => 'department_3_name',
        'type'        => 'text',
        'desc'        => 'Enter the name of department #3.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #3 - Email Address',
        'id'          => 'department_3_email',
        'type'        => 'text',
        'desc'        => 'Enter the email address of department #3.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #4 - Name',
        'id'          => 'department_4_name',
        'type'        => 'text',
        'desc'        => 'Enter the name of department #4.',
        'section'     => 'contact_page'
      ),
      array(
        'label'       => 'Department #4 - Email Address',
        'id'          => 'department_4_email',
        'type'        => 'text',
        'desc'        => 'Enter the email address of department #4.',
        'section'     => 'contact_page'
      ),

      
      
      /*******************************************************************************************************/
      /* Social Icons section */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Facebook',
        'id'          => 'facebook',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Facebook account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Tumblr',
        'id'          => 'tumblr',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Tumblr account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Twitter',
        'id'          => 'twitter',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Twitter account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Youtube',
        'id'          => 'youtube',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Youtube account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Flickr',
        'id'          => 'flickr',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Flickr account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Foursquare',
        'id'          => 'foursquare',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Foursquare account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Github',
        'id'          => 'github',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Github account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Google+',
        'id'          => 'google_plus',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Google+ account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Instagram',
        'id'          => 'instagram',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Instagram account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Linkedin',
        'id'          => 'linkedin',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Linkedin account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Bitbucket',
        'id'          => 'bitbucket',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Bitbucket account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Pinterest',
        'id'          => 'pinterest',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Pinterest account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Skype',
        'id'          => 'skype',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Skype account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Dribbble',
        'id'          => 'dribbble',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Dribbble account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Dropbox',
        'id'          => 'dropbox',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Dropbox account.',
        'section'     => 'social_icons'
      ),
      array(
        'label'       => 'Android',
        'id'          => 'android',
        'type'        => 'text',
        'desc'        => 'Enter the URL to your Android account.',
        'section'     => 'social_icons'
      ),


      /*******************************************************************************************************/
      /* VPS Slider - Plan 1 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_1_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_1_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_1_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 1.',
        'section'     => 'vps_plan_1'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_1_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_1_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_1_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_1_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_1_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_1_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_1_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_1_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_1_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 1.',
        'section'     => 'vps_plan_1'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 2 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_2_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_2_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_2_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 2.',
        'section'     => 'vps_plan_2'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_2_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_2_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_2_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_2_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_2_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_2_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_2_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_2_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_2_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 2.',
        'section'     => 'vps_plan_2'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 3 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_3_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_3_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_3_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 3.',
        'section'     => 'vps_plan_3'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_3_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_3_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_3_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_3_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_3_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_3_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_3_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_3_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_3_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 3.',
        'section'     => 'vps_plan_3'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 4 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_4_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_4_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_4_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 4.',
        'section'     => 'vps_plan_4'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_4_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_4_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_4_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_4_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_4_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_4_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_4_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_4_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_4_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 4.',
        'section'     => 'vps_plan_4'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 5 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_5_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_5_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_5_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 5.',
        'section'     => 'vps_plan_5'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_5_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_5_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_5_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_5_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_5_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_5_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_5_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_5_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_5_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 5.',
        'section'     => 'vps_plan_5'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 6 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_6_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_6_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_6_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 6.',
        'section'     => 'vps_plan_6'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_6_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_6_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_6_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_6_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_6_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_6_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_6_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_6_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_6_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 6.',
        'section'     => 'vps_plan_6'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 7 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_7_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_7_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_7_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 7.',
        'section'     => 'vps_plan_7'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_7_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_7_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_7_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_7_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_7_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_7_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_7_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_7_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_7_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 7.',
        'section'     => 'vps_plan_7'
      ),



      /*******************************************************************************************************/
      /* VPS Slider - Plan 8 */
      /*******************************************************************************************************/
      
      array(
        'label'       => 'Price',
        'id'          => 'vps_8_price',
        'type'        => 'text',
        'desc'        => 'Enter the price of the plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Order Button Name',
        'id'          => 'vps_8_button_name',
        'type'        => 'text',
        'desc'        => 'Enter the order button name of the plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Order Button URL',
        'id'          => 'vps_8_button_url',
        'type'        => 'text',
        'desc'        => 'Enter the order button URL of the plan 8.',
        'section'     => 'vps_plan_8'
      ),

      array(
        'label'       => 'Feature #1',
        'id'          => 'vps_8_feature_1',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #2',
        'id'          => 'vps_8_feature_2',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #3',
        'id'          => 'vps_8_feature_3',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #4',
        'id'          => 'vps_8_feature_4',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #5',
        'id'          => 'vps_8_feature_5',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #6',
        'id'          => 'vps_8_feature_6',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #7',
        'id'          => 'vps_8_feature_7',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #8',
        'id'          => 'vps_8_feature_8',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),
      array(
        'label'       => 'Feature #9',
        'id'          => 'vps_8_feature_9',
        'type'        => 'text',
        'desc'        => 'Enter the name of a feature for plan 8.',
        'section'     => 'vps_plan_8'
      ),






    )
  );
  
  /* if settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( 'option_tree_settings', $custom_settings ); 
  }
  
}
?>