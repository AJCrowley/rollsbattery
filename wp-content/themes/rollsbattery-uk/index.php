<?php
/*
index.php
*/

get_header(); ?>

	<div id="breadcrumb_wrapper">
		<div class="wrap">
				
			<h3><?php wp_title(''); ?></h3>

		</div>
	</div>

	<!-- START content -->
	<div class="content">
		<div class="wrap">
		
			<!-- sidebar -->
			<?php get_sidebar('left'); ?>

			<div class="content_right">

				<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

					<?php get_template_part( 'content', get_post_format() ); ?>
					
				<?php endwhile; ?>
				
				<?php else : ?>
					<h6 class="center"><?php _e('Not Found', 'realhost') ?></h6>
					<p class="center">
						<?php _e("Sorry, no posts were found.", 'realhost'); ?>
					</p>
				
				<?php endif; ?>
				<?php
					if (function_exists('wp_pagenavi')) {
					    wp_pagenavi();
					}
					else
					{
					    echo '<div id="pagination">';
					        echo previous_posts_link('&laquo; Previous') . next_posts_link('Next &raquo;');
					        echo '<div class="clear"></div>';
					    echo '</div>';
					}
				?>
				
					<div class="clear"></div>

			</div>
				
			<div class="clear"></div>
			
		</div>
	</div>
	<!-- END content -->


<!-- START footer -->
<?php get_footer(); ?>