(function($) {

	"use strict";
	
	var vpsplan;

	$('#vps-slider').slider({
	range: "max",
	animate: true,
	value: 1,
	min: 1,
	max: 8,
	step: 1,
	slide: function(event, ui) {
		$('.node').removeClass('vps_highlight');
		$('.node:lt(' + ui.value + ')').addClass("vps_highlight");
		vpsplan = ui.value;
		vpsPlanCheck();
	}
	});

	function moveSlider(e, num) {
	e.preventDefault();
	$('#vps-slider').slider(
		'value',
		[num]
	);
	vpsplan = num;
	vpsPlanCheck();
	}

	function vpsPlanCheck() {
		if (vpsplan < "2") { $('.vps1').css('display', 'block'); } else { $('.vps1').css('display', 'none'); }
		if (vpsplan > "1" && vpsplan < "3") { $('.vps2').css('display', 'block'); } else { $('.vps2').css('display', 'none'); }
		if (vpsplan > "2" && vpsplan < "4") { $('.vps3').css('display', 'block'); } else { $('.vps3').css('display', 'none');  }
		if (vpsplan > "3" && vpsplan < "5") { $('.vps4').css('display', 'block'); } else { $('.vps4').css('display', 'none');  }
		if (vpsplan > "4" && vpsplan < "6") { $('.vps5').css('display', 'block'); } else { $('.vps5').css('display', 'none');  }
		if (vpsplan > "5" && vpsplan < "7") { $('.vps6').css('display', 'block'); } else { $('.vps6').css('display', 'none');  }
		if (vpsplan > "6" && vpsplan < "8") { $('.vps7').css('display', 'block'); } else { $('.vps7').css('display', 'none');  }
		if (vpsplan > "7" && vpsplan < "9") { $('.vps8').css('display', 'block'); } else { $('.vps8').css('display', 'none');  }
	}

})(jQuery);