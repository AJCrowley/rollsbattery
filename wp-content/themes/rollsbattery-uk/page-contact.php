<?php
/*
Template Name: Contact Template
*/

get_header(); ?>


<div id="breadcrumb_wrapper">
	<div class="wrap">
			
		<h3><?php wp_title(''); ?></h3>

	</div>
</div>

<div class="clear"></div>

<!-- START content -->
<div class="content">
	<div class="wrap">

		<h4><?php echo esc_attr(ot_get_option('contact_headline')); ?></h4>

		<p><?php echo esc_attr(ot_get_option('contact_text')); ?></p>

			<div class="clear"></div>
			<div class="space2"></div>

		<!-- contact form -->
		<div class="one_half">
			
		<?php echo do_shortcode('[contact-form-7 id="1773" title="Contact form 1"]'); ?>

		</div>

		<!-- contact details -->
		<div class="one_half last">
			<div id="contact_info">
				<h4>Contact Details</h4>

				<div class="clear"></div>
				<div class="space2"></div>

				<div class="one_half">
					<h6><i class="icon-envelope"></i><?php echo esc_attr(ot_get_option('department_1_name')); ?></h6>
					<p><a href="mailto:<?php echo esc_attr(ot_get_option('department_1_email')); ?>"><?php echo esc_attr(ot_get_option('department_1_email')); ?></a></p>

						<div class="clear"></div>
						<div class="space2"></div>

					<h6><i class="icon-envelope"></i><?php echo esc_attr(ot_get_option('department_2_name')); ?></h6>
					<p><a href="mailto:<?php echo esc_attr(ot_get_option('department_2_email')); ?>"><?php echo esc_attr(ot_get_option('department_2_email')); ?></a></p>
				</div>

				<div class="one_half last">
					<h6><i class="icon-envelope"></i><?php echo esc_attr(ot_get_option('department_3_name')); ?></h6>
					<p><a href="mailto:<?php echo esc_attr(ot_get_option('department_3_email')); ?>"><?php echo esc_attr(ot_get_option('department_3_email')); ?></a></p>

						<div class="clear"></div>
						<div class="space2"></div>

					<h6><i class="icon-envelope"></i><?php echo esc_attr(ot_get_option('department_4_email')); ?></h6>
					<p><a href="mailto:<?php echo esc_attr(ot_get_option('department_4_email')); ?>"><?php echo esc_attr(ot_get_option('department_4_email')); ?></a></p>
				</div>
			</div>
		</div>

		<div class="clear"></div>
		
	</div>
</div>

			
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>
			
		<?php endwhile; ?>
		
		<?php endif; ?>

		<div class="clear"></div>

<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>