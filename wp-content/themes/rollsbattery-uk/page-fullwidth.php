<?php
/*
Template Name: Full Width Template
*/

get_header(); ?>


<div id="breadcrumb_wrapper">
	<div class="wrap">

		<h3><?php wp_title(''); ?></h3>

	</div>
</div>

<div class="clear"></div>

<!-- START content -->
<div class="content">
	<div class="wrap">

		<!-- page content -->
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>

		<?php endif; ?>

		<div class="clear"></div>

	</div>
</div>
<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>