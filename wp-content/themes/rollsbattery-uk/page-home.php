<?php
/*
Template Name: Home Template
*/

get_header(); ?>

<!-- START slider -->
<div id="slider_wrapper">
	<div class="wrap">

		<?php //echo realhost_bxslider_template(); ?>
		<?php layerslider(1) ?>

	</div>
</div>

<!-- START content -->
<div class="content">
	<div class="wrap">

		<!-- page content -->
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>

		<?php endif; ?>

		<div class="clear"></div>
	</div>
	<div id="our_product"  >

		<div id="our_products" > OUR BATTERIES </div>

		<div id="icons"  >
			<div id="sun"><a href="/renewable_energy/" id="sunlink"></a></div>
			<div id="ship"><a href="/marine/" id="shiplink"></a></div>
			<div id="train"><a href="/railroad/" id="trainlink"></a></div>
			<div id="battery"><a href="/motive_power/" id="batterylink"></a></div>
			<div id="lightning"><a href="/agm/" id="lightninglink"></a></div>
			<div id="gel"><a href="/gel/" id="gellink"></a></div>
		</div>
	</div>
	<div id="quote">
		<div id="quote_box">
		<div id="left_quote"><img src="<?php echo get_bloginfo('template_url');?>/images/left_quotes.png" /> </div>
		<div id="quotes">
			<div id="quote1" >
				<?php dynamic_sidebar('realhost_frontpage_quote_1'); ?>
			</div>
			<div id="quote2" >
				<?php dynamic_sidebar('realhost_frontpage_quote_person'); ?>
			</div>
		</div>
		<div id="right_quote"><img src="<?php echo get_bloginfo('template_url');?>/images/right_quotes.png" /> </div>
		</div>
	</div>
	<div id="rewards"  >
		<div id="rollsrewards"></div>
		<div id="rewards_right">
			<h5>Protect your investment.</h5>

			<p>1. Register your Rolls batteries and have a member of our Technical team verify all system setup and charging details, offering maintenance instructions and recommendations to prolong the life of your investment.</p>

<p>2. The battery bank is often the most costly component of your system. When it's time to replace your batteries, we want to make it easy. We offer Rolls Rewards members with vouchers and special services for being a long term customer.</p>
		<div id="more_info" >
			<a href="http://rollsbattery.com/rewards/">GET REWARDS >></a>
		</div>
		</div>
	</div>
</div>
<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>
