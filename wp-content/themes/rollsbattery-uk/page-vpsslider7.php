<?php
/*
Template Name: VPS - 7 Plans Template
*/

get_header(); ?>


<div id="breadcrumb_wrapper">
	<div class="wrap">
			
		<h3><?php wp_title(''); ?></h3>

	</div>
</div>

<div class="clear"></div>

<!-- START content -->
<div class="content">
	<div class="wrap">

		<!-- 7 plan vps slider -->
	    <div class="vps_slider vps_slider_7" id="vps-slider"></div><br>
	    <div class="vps_7" id="nodes">
	        <div class="node vps_highlight"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="node"><i class="icon-hdd"></i></div>
	        <div class="clear"></div>
	    </div>

	    <div class="vps vps1" style="display:block">
	    	<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_1_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_1_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_1_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_1_feature_9')); ?></h6></div>
	    </div>

		<div class="vps vps2">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_2_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_2_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_2_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_2_feature_9')); ?></h6></div>
	    </div>

	    <div class="vps vps3">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_3_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_3_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_3_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_3_feature_9')); ?></h6></div>
	    </div>

	    <div class="vps vps4">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_4_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_4_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_4_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_4_feature_9')); ?></h6></div>
	    </div>

	    <div class="vps vps5">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_5_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_5_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_5_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_5_feature_9')); ?></h6></div>
	    </div>

	    <div class="vps vps6">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_6_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_6_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_6_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_6_feature_9')); ?></h6></div>
	    </div>

	    <div class="vps vps7">
			<div class="one_half"><h4>Price: $<?php echo esc_html(ot_get_option('vps_7_price')); ?></h4></div>
			<div class="one_half last"><a href="<?php echo esc_url(ot_get_option('vps_7_button_url')); ?>" class="button"><?php echo esc_html(ot_get_option('vps_7_button_name')); ?></a></div>
				<div class="clear"></div>
				<div class="space2"></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_1')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_2')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_3')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_4')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_5')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_6')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_7')); ?></h6></div>
			<div class="one_third node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_8')); ?></h6></div>
			<div class="one_third last node-details"><h6><?php echo esc_html(ot_get_option('vps_7_feature_9')); ?></h6></div>
	    </div>
	    
		<div class="clear"></div>
		<div class="space"></div>
			
		<!-- page content -->
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>
			
		<?php endwhile; ?>
		
		<?php endif; ?>

		<div class="clear"></div>
		
	</div>
</div>
<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>