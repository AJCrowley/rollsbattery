<?php
/*
page.php
*/

get_header(); ?>


<div id="breadcrumb_wrapper">
	<div class="wrap">

		<h3><?php the_title(''); ?></h3>

	</div>
</div>

<div class="clear"></div>

<!-- START content -->
<div class="content" id="content">
	<div class="wrap">

		<!-- page content -->
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>

			<?php the_content(); ?>

		<?php endwhile; ?>

		<?php endif; ?>

		<div class="clear"></div>

	</div>
</div>
<!-- END content -->

<!-- START footer -->
<?php get_footer(); ?>
